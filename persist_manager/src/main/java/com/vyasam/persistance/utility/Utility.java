package com.vyasam.persistance.utility;


import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Utility {

	public static SessionFactory sessionFactory = null; 
	
	public static synchronized SessionFactory getSessionFactory()
	{
		if(sessionFactory == null)
		{
			Configuration config = new Configuration();
			sessionFactory = config.configure().buildSessionFactory();
		}
		return sessionFactory;
	}
	
	public static void closeSessionFactory()
	{
		sessionFactory.close();
	}
}
