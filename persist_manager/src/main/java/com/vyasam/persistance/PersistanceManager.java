package com.vyasam.persistance;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import javassist.bytecode.analysis.Type;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.vyasam.category.models.VClass;
import com.vyasam.jsoup.models.exceptions.VyasamModelsException;
import com.vyasam.models.ItemDetails;
import com.vyasam.models.ItemsSourceDetails;
import com.vyasam.models.interfaces.IVyasamModel;
import com.vyasam.persistance.utility.CommonConstants;
import com.vyasam.persistance.utility.Utility;
import com.vyasam.query.models.DataType;
import com.vyasam.query.models.Query;
import com.vyasam.testModels.Address;
import com.vyasam.testModels.Job;
import com.vyasam.testModels.User;
import com.vyasam.utility.db.ConnectionFactory;
import com.vyasam.utility.db.DBType;

public class PersistanceManager {

	/*----------------------------Save the objects ----------------------*/

	public Object save(Object obj) {
		SessionFactory factory = Utility.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();

		session.save(obj);

		session.getTransaction().commit();
		session.close();
		return obj;
	}

	public Object persist(Object obj) {
		SessionFactory factory = Utility.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();

		session.persist(obj);
		session.flush();
		session.getTransaction().commit();
		session.close();
		return obj;
	}

	public Object saveOrUpdate(Object obj) {
		try
		{
			SessionFactory factory = Utility.getSessionFactory();
			Session session = factory.openSession();
			session.beginTransaction();
	
			session.saveOrUpdate(obj);
	
			session.getTransaction().commit();
			session.close();
			return obj;
		} catch(Exception e)
		{
			System.out.println(e.getMessage());
			e.printStackTrace();
			System.out.println("\n=================================================\n\n\n");
		}
		return null;
	}	

	/*-------------------------------Delete-------------------------------*/

	public void Delete(Object obj) {
		SessionFactory factory = Utility.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();

		session.delete(obj);

		session.getTransaction().commit();
		session.close();
	}

	/*-------------------------------Fetch-------------------------------*/

	/*-------------------------------Using Hibernate-------------------------------*/
	public <T> List<T> fetch(String sql, String entityName) {
		SessionFactory factory = Utility.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();

		List<T> list = session.createSQLQuery(sql).addEntity(entityName).list();

		session.getTransaction().commit();
		session.close();
		return list;
	}

	public List fetchFromSql(String sql) {
		SessionFactory factory = Utility.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();

		List list = session.createSQLQuery(sql).list();

		session.getTransaction().commit();
		session.close();
		return list;
	}

	public List<?> fetch(String hql) {
		SessionFactory factory = Utility.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();

		List<?> list = session.createQuery(hql).list();

		session.getTransaction().commit();
		session.close();
		return list;
	}

	public List<?> fetch(String hql, Integer limit) {
		SessionFactory factory = Utility.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();

		List<?> list = session.createQuery(hql).setMaxResults(limit).list();

		session.getTransaction().commit();
		session.close();
		return list;
	}

	/*
	 * public ItemsSourceDetails fetch(ItemsSourceDetails source) {
	 * SessionFactory factory = Utility.getSessionFactory(); Session session =
	 * factory.openSession(); session.beginTransaction();
	 * 
	 * ItemsSourceDetails result = (ItemsSourceDetails)
	 * session.get(ItemsSourceDetails.class, source.getId());
	 * //createSQLQuery(sql).list(); result.getItemDetailsList();
	 * session.getTransaction().commit(); session.close(); return result; }
	 */

	public <T extends IVyasamModel> T fetch(T source, Class c)
			throws VyasamModelsException {
		SessionFactory factory = Utility.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();
		T result = (T) session.get(c, source.getId());
		session.getTransaction().commit();
		session.close();
		return result;
	}

	/*-------------------------------Using Plain SQL and JDBC-------------------------------*/

	public ResultSet executeSQL(Query query) throws VyasamModelsException {
		ResultSet rs = null;
		try {
			Connection conn = ConnectionFactory.getConnection(DBType.MYSQL,
					CommonConstants.MYSQL_JDBC_URL, CommonConstants.USERNAME,
					CommonConstants.PASSWORD);
			PreparedStatement ps = conn.prepareStatement(query.getQuery());
			addParameter(ps,query);
			rs = ps.executeQuery();			
		} catch (Exception e) {
			throw new VyasamModelsException("Error while executeSQL(): " + e.getMessage());
		}
		return rs;
	}

	private void addParameter(PreparedStatement ps, Query query) throws VyasamModelsException {
		
		List<String> parameters = query.getParameters();
		List<DataType> types = query.getParametersTypes();

		if(parameters == null || types == null)
			return;
			
		if (parameters.size() != types.size())
			throw new VyasamModelsException(
					"No proper input. Either Query has null parameters or types or different number of parameters and types");
		try {
			System.out.println("Query: " + query.getQuery());
			for (int i = 1; i < parameters.size()+1; i++) {
				switch (types.get(i-1)) {

					case INTEGER:
						ps.setInt(i, Integer.parseInt(parameters.get(i-1)));
					case STRING:
						ps.setString(i, parameters.get(i-1));
				}
			}
		} catch (Exception e) {
			throw new VyasamModelsException(
					"Error while forming prepared statment: " + e.getMessage());
		}
	}

	/*-------------------------------Others-------------------------------*/

	public void close() {
		Utility.getSessionFactory().close();
	}

	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		//close();
	}

	/*
	public static void main(String[] args) {
		PersistanceManager manager = new PersistanceManager();
		try {

		} catch (Exception e) {
		}
		manager.close();
	}
*/
}
