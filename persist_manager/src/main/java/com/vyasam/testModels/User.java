package com.vyasam.testModels;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity (name="USER")
public class User {
	
	@Id  @GeneratedValue
	private Integer id;
	private String name = "";
	@OneToMany (cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	private List<Address> address = new ArrayList<Address>();

	private Job jobDetails = new Job();

	public User()
	{
		
	}
	
	public User( String uName)
	{
	//	id = userId;
		name = uName;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Address>  getAddress() {
		return address;
	}

	public void setAddress(List<Address>  address) {
		this.address = address;
	}
	public Job getJobDetails() {
		return jobDetails;
	}

	public void setJobDetails(Job jobDetails) {
		this.jobDetails = jobDetails;
	}

}
