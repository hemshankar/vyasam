package com.cleanview.persistservices;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.cleanview.models.GroupDetails;
import com.cleanview.persist.common.Status;
import com.cleanview.persistservices.util.PersistUtils;
@Path("featuregroup")
public class FeatureGroupServices {
	
	@Path("add")
	@POST
	@Produces({ MediaType.APPLICATION_XML })	
	public Response addGroupDetails(GroupDetails groupDetails){
		try{
			if(PersistUtils.persistManager.addFeatureGroupDetails(groupDetails)==Status.SUCCESS){
				return Response.status(200).entity("Added fature group successfully").build();
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}
	
	@Path("fetchAll")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response fetchAllgroupDetails(GroupDetails groupDetails){
		try{
			List<GroupDetails> groupList = new ArrayList<GroupDetails>();
			if(PersistUtils.persistManager.fetchAllGroupDetails(groupDetails.getCategoryId(), groupList) == Status.SUCCESS){
				return Response.status(200).entity(groupList).build();
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}
	
	@Path("update")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response updateGroupDetails(GroupDetails groupDetails, @Context UriInfo ui){
		try{
			MultivaluedMap<String, String> queryParams = ui.getQueryParameters();
    		String originalgroupId = queryParams.getFirst("originalgroupId");
			if(PersistUtils.persistManager.updateFeatureGroupDetails(groupDetails, originalgroupId)==Status.SUCCESS){
				return Response.status(200).entity("Updated group details successfully").build();
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}
	
	@Path("delete")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response deleteGroupDetails(GroupDetails groupDetails){
		try{			
			if(PersistUtils.persistManager.deleteFeatureGroup(groupDetails)==Status.SUCCESS){
				return Response.status(200).entity("Deleted group details successfully").build();
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}
}
