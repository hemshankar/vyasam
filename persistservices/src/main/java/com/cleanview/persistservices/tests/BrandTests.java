package com.cleanview.persistservices.tests;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import com.cleanview.models.BrandDetails;
import com.cleanview.persist.common.Status;

public class BrandTests {
	public BrandDetails addBrand(BrandDetails obj1){
		String URL = "http://localhost:8080//persistservices/cleanview/brand/add";		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);		
		
		Response res= target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(obj1, MediaType.APPLICATION_XML));
		return res.readEntity(BrandDetails.class);
		
	}
	/*
	public BrandDetails fetchFeatureGroupById(String id){
		String URL = "http://localhost:8080//persistservices/cleanview/brand/fetch_by_id";		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);
		
		return target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(id, MediaType.APPLICATION_XML),BrandDetails.class);
		
	}*/
	
	public List<BrandDetails> fetchAllBrands(BrandDetails brand){
		String URL = "http://localhost:8080//persistservices/cleanview/brand/fetch_all";
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);
		
		/*return  target.request(MediaType.APPLICATION_XML).
				post(Entity.entity(brand, MediaType.APPLICATION_XML),new GenericType<List<BrandDetails>>(){});*/
		
		Response res =  target.request(MediaType.APPLICATION_XML).
				post(Entity.entity(brand, MediaType.APPLICATION_XML));
		return res.readEntity(new GenericType<List<BrandDetails>>(){});
	}
	
	public Status updateBrandDetails(BrandDetails brandDetails){
		String URL = "http://localhost:8080//persistservices/cleanview/brand/update";
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);
		
		Response res = target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(brandDetails, MediaType.APPLICATION_XML));
		
		if(res.getStatus()==200){
			return Status.SUCCESS;
		}
		return Status.FALIUER;
		
	}
	
	public static void main(String []args){
		
		BrandTests tests = new BrandTests();
		
		BrandDetails brand = new BrandDetails();
		brand.setCategoryId(27);
		brand.setBrandName("Brand"+TestUtils.getRandoNumber());
		//brand.setBrandId(20);
		
		tests.testAddBrand(brand);
		//tests.testFetchBrandDetails(brand);
		//tests.testUpdateBrandDetails(brand);
	//	tests.testfetchAllBrands(brand);
	}
	
	//===========================================Tests==================================
	public void testAddBrand(BrandDetails brand){
		brand = addBrand(brand);
		System.out.println(brand);
	}
	
	public void testFetchBrandDetails(BrandDetails brand){
		List<BrandDetails> detailsList = fetchAllBrands(brand);
		System.out.println(detailsList);
	}
	
	public void testUpdateBrandDetails(BrandDetails brand){
		brand.setBrandName(brand.getBrandName()+ "_updated");
		if(updateBrandDetails(brand)==Status.SUCCESS){
			System.out.println("Updated brand " + brand.getBrandName());
		};
	}
}
