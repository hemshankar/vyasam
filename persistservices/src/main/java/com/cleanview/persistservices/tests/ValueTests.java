package com.cleanview.persistservices.tests;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import com.cleanview.models.ValueDetails;
import com.cleanview.persist.common.Status;

public class ValueTests {
	public Status addValue(ValueDetails obj1){
		String URL = "http://localhost:8080//persistservices/cleanview/feature_values/add";		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);
		
		Response res = target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(obj1, MediaType.APPLICATION_XML));
		
		if(res.getStatus()==200){
			return Status.SUCCESS;
		}
		return Status.FALIUER;
	}
	/*
	public ValueDetails fetchFeatureGroupById(String id){
		String URL = "http://localhost:8080//persistservices/cleanview/feature_values/fetch_by_id";		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);
		
		return target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(id, MediaType.APPLICATION_XML),ValueDetails.class);
		
	}*/
	
	public List<ValueDetails> fetchAllValues(ValueDetails value){
		String URL = "http://localhost:8080//persistservices/cleanview/feature_values/fetch_all";
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);
		
		Response res = target.request(MediaType.APPLICATION_XML).
				post(Entity.entity(value, MediaType.APPLICATION_XML));
		
		return res.readEntity(new GenericType<List<ValueDetails>>(){});
	}
	
	public Status updateValueDetails(ValueDetails valueDetails,String originalValue){
		String URL = "http://localhost:8080//persistservices/cleanview/feature_values/update?value=" + originalValue;
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);
		
		Response res = target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(valueDetails, MediaType.APPLICATION_XML));
		
		if(res.getStatus()==200){
			return Status.SUCCESS;
		}
		return Status.FALIUER;
	}
	
	public static void main(String[]args){
		
		ValueTests test = new ValueTests();
		
		ValueDetails details = new ValueDetails();
		details.setCategoryId(7);
		details.setFeatureId("RAM8");
		details.setValue("Mem" + TestUtils.getRandoNumber());
		details.setWeight(TestUtils.getRandoNumber());
		
		/*if(test.addValue(details)==Status.SUCCESS){
			System.out.println("Added feature");
		}*/		
		
		List<ValueDetails> valueDetailsList = test.fetchAllValues(details);
		ValueDetails vd = valueDetailsList.get(0);
		String originalValue = vd.getValue();
				vd.setValue("NewValue" + TestUtils.getRandoNumber());
		if(test.updateValueDetails(vd, originalValue)==Status.SUCCESS){
			System.out.println("Updated " + originalValue + " to " + vd.getValue());
		}
		
	}
}
