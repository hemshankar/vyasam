package com.cleanview.persistservices.tests;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import com.cleanview.models.FeatureDetails;
import com.cleanview.persist.common.Status;

public class FeatureTests {
	public Status addFeature(FeatureDetails obj1){
		String URL = "http://localhost:8080//persistservices/cleanview/feature/add";		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);
		
		Response res = target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(obj1, MediaType.APPLICATION_XML));
		if(res.getStatus()==200)
			return Status.SUCCESS;
		return Status.FALIUER;
		
		
	}
	/*
	public FeatureDetails fetchFeatureGroupById(String id){
		String URL = "http://localhost:8080//persistservices/cleanview/feature/fetch_by_id";		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);
		
		return target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(id, MediaType.APPLICATION_XML),FeatureDetails.class);
		
	}*/
	
	public List<FeatureDetails> fetchAllFeatures(FeatureDetails feature){
		String URL = "http://localhost:8080//persistservices/cleanview/feature/fetchAll";
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);
		
		Response res = target.request(MediaType.APPLICATION_XML).
				post(Entity.entity(feature, MediaType.APPLICATION_XML));
		
		return res.readEntity(new GenericType<List<FeatureDetails>>(){});
	}
	
	public Status updateFeatureDetails(FeatureDetails featureDetails,String OriginalFeatureId){
		String URL = "http://localhost:8080//persistservices/cleanview/feature/update";
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);
		
		Response res = target.queryParam("originalFeatureId", OriginalFeatureId).request(MediaType.APPLICATION_XML).
					post(Entity.entity(featureDetails, MediaType.APPLICATION_XML));
		if(res.getStatus()==200)
			return Status.SUCCESS;
		return Status.FALIUER;
		
	}
	

	public static void main(String [] args){
		FeatureTests tests = new FeatureTests();
		
		FeatureDetails fd = new FeatureDetails();
		fd.setFeatureId("Memory" + TestUtils.getRandoNumber());
		fd.setCategoryId(22);
		
		/*if(tests.addFeature(fd)==Status.SUCCESS){
			System.out.println("Added column");
		}*/
		
		List<FeatureDetails> fdList = tests.fetchAllFeatures(fd);
		//System.out.println(fdList);
		
		
		//fd.setCategoryId(21);
		String orgi = fdList.get(0).getFeatureId();
		fdList.get(0).setFeatureId("New FeatureId" + TestUtils.getRandoNumber());
		if(tests.updateFeatureDetails(fdList.get(0),orgi)==Status.SUCCESS){
			System.out.println("Updated sussessfully featureId: " + fdList.get(0).getFeatureId());
		};
	}
	
	//==================================Tests============================
	
	public void testFetchAllFeatures(FeatureDetails fd){
		
	}
	
	public void testUpdateFeatureDetails(FeatureDetails fd){
		
	}
}
