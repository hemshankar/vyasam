package com.cleanview.persistservices.tests;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.ClientConfig;

import com.cleanview.models.GroupDetails;


public class FeatureGroupTest {
	public void addFeatureGroup(GroupDetails obj1){
		String URL = "http://localhost:8080//persistservices/cleanview/featuregroup/add";		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);
		
		target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(obj1, MediaType.APPLICATION_XML),GroupDetails.class);
		
	}
	/*
	public GroupDetails fetchFeatureGroupById(String id){
		String URL = "http://localhost:8080//persistservices/cleanview/featuregroup/fetch_by_id";		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);
		
		return target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(id, MediaType.APPLICATION_XML),GroupDetails.class);
		
	}*/
	
	public List<GroupDetails> fetchAllFeatureGroups(Integer cateId){
		String URL = "http://localhost:8080//persistservices/cleanviewc/featuregroup/fetchaAll";
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);
		
		return target.request(MediaType.APPLICATION_XML).
				post(Entity.entity(cateId, MediaType.APPLICATION_XML),new GenericType<List<GroupDetails>>(){});
	}
	
	public void updateFeatureGroupDetails(GroupDetails groupDetails){
		String URL = "http://localhost:8080//persistservices/cleanview/featuregroup/update";
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);
		
		target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(groupDetails, MediaType.APPLICATION_XML),GroupDetails.class);
		
	}
}
