package com.cleanview.persistservices.tests;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import com.cleanview.models.ItemDetails;
import com.cleanview.persist.common.Status;
import com.cleanview.persistservices.util.PersistUtils;

public class ItemsTest {
	public ItemDetails addItemDetails(ItemDetails obj1) {
		String URL = "http://localhost:8080//persistservices/cleanview/item_details/add";
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);

		Response res = target.request(MediaType.APPLICATION_XML).post(
				Entity.entity(obj1, MediaType.APPLICATION_XML));
		return res.readEntity(ItemDetails.class);
	}

	public ItemDetails addItemDetailsSerialized(ItemDetails obj1)
			throws Exception {
		String URL = "http://localhost:8080//persistservices/cleanview/item_details/serialized/add";
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);

		String serializedItem = obj1.toString();
		Response res = target.request(MediaType.TEXT_HTML).post(
				Entity.entity(serializedItem, MediaType.TEXT_HTML));
		String itemStr = res.readEntity(String.class);
		return ItemDetails.populateFromString(itemStr);
	}

	public ItemDetails featchItem(ItemDetails item) {
		String URL = "http://localhost:8080//persistservices/cleanview/item_details/fetch";
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);

		Response res = target.request(MediaType.APPLICATION_XML).post(
				Entity.entity(item, MediaType.APPLICATION_XML));
		return res.readEntity(ItemDetails.class);
	}

	public List<ItemDetails> featchAllItemDetails(ItemDetails item) {
		String URL = "http://localhost:8080//persistservices/cleanview/item_details/fetch_all";
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);

		Response res = target.request(MediaType.APPLICATION_XML).post(
				Entity.entity(item, MediaType.APPLICATION_XML));

		return res.readEntity(new GenericType<List<ItemDetails>>() {
		});
	}

	public Status updateItemDetails(ItemDetails valueDetails) {
		String URL = "http://localhost:8080//persistservices/cleanview/item_details/update";
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);

		Response res = target.request(MediaType.APPLICATION_XML).post(
				Entity.entity(valueDetails, MediaType.APPLICATION_XML));
		if (res.getStatus() == 200) {
			return Status.SUCCESS;
		}
		return Status.FALIUER;
	}

	public static void addItem(){
		try {
			ItemsTest test = new ItemsTest();

			ItemDetails id = new ItemDetails();

			id.setCategoryId(102);
			id.setBrandId(56);
			id = test.addItemDetailsSerialized(id);
			System.out.println(id.getItemId());
			/*
			 * for(String key: id.getFeatures().keySet()){
			 * id.getFeatures().put(key, TestUtils.getRandoNumber() + ""); }
			 * 
			 * if(test.updateItemDetails(id)==Status.SUCCESS){
			 * System.out.println("Item updated successfully"); }
			 */

			// System.out.println("All items:" + test.featchAllItemDetails(id));
			System.out.println("\n\nItem details for itemId " + id.getItemId()
					+ ": " + test.featchItem(id));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void testItemDetailsToFromString(){
		try{
			ItemDetails id = new ItemDetails();
	
			id.setCategoryId(102);
			id.setBrandId(56);
			
			String str = id.toString();
			
			System.out.println(str);
			
			id = ItemDetails.populateFromString(str);
					
			System.out.println(id);
			
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		
		addItem();
	}
}
