package com.cleanview.persistservices.tests;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import com.cleanview.models.BrandDetails;
import com.cleanview.models.CategoryDetailsForPersistance;
import com.cleanview.models.FeatureDetails;
import com.cleanview.models.GroupDetails;
import com.cleanview.persist.common.Status;

public class CategoryTests {
	
	public CategoryDetailsForPersistance addCategory(CategoryDetailsForPersistance obj1){
		String URL = "http://localhost:8080//persistservices/cleanview/categories/add";		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);
		
		Response res = target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(obj1, MediaType.APPLICATION_XML));
		return res.readEntity(CategoryDetailsForPersistance.class);
	}
	
	public CategoryDetailsForPersistance fetchCategoryById(CategoryDetailsForPersistance cate){
		String URL = "http://localhost:8080//persistservices/cleanview/categories/fetch_by_id";		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);
		
		Response res = target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(cate, MediaType.APPLICATION_XML));
		return res.readEntity(CategoryDetailsForPersistance.class);
		
	}
	
	public List<CategoryDetailsForPersistance> fetchAllCategories(){
		String URL = "http://localhost:8080//persistservices/cleanview/categories/fetchaAll";
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);
		
		Response res = target.request(MediaType.APPLICATION_XML).get();
		return res.readEntity(new GenericType<List<CategoryDetailsForPersistance>>(){});
	}
	
	public Status updateCategoryDetails(CategoryDetailsForPersistance categoryDetails){
		String URL = "http://localhost:8080//persistservices/cleanview/categories/update";
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);
		
		Response response = target.request().post(Entity.entity(categoryDetails, MediaType.APPLICATION_XML));
		if(response.getStatus()==200)
			return Status.SUCCESS;
		return Status.FALIUER;
	}
	
	public static void main(String [] args){
		CategoryTests tests = new CategoryTests();
		
		CategoryDetailsForPersistance details = new CategoryDetailsForPersistance();
		
		tests.testAddCategoryDetails(details);
		tests.testFeatchAllCategories();
		
		tests.testFetchCategoryById(details);
		
		details.setCategoryId(7);
		details.setCategoryName("Pressure Cooker");
		
		tests.testUpdateCategoryDetails(details);
		
	}
	
	//=============================Tests=====================================
	public void testAddCategoryDetails(CategoryDetailsForPersistance details){
		details.setCategoryName("TestCategoty" + TestUtils.getRandoNumber());
		details.setParentId(1);
		
		FeatureDetails fd = new FeatureDetails();
		fd.setFeatureId("Feature" + TestUtils.getRandoNumber());
				
		GroupDetails gd = new GroupDetails();
		gd.setGroupId("" + TestUtils.getRandoNumber());
		
		BrandDetails brand = new BrandDetails();
		brand.setBrandName("Nokia");
		
		details.getFeatureDetails().put(fd.getFeatureId(),fd);
		details.getGroupDetails().put(gd.getGroupId(), gd);
		details.getBrands().add(brand);
		
		details = addCategory(details);
		System.out.println("Added categoryID: " + details.getCategoryId());
	}

	public void testFetchCategoryById(CategoryDetailsForPersistance details){
		details.setCategoryId(7);
		CategoryDetailsForPersistance cateDetais = fetchCategoryById(details);
		System.out.println(cateDetais);
	}

	public void testFeatchAllCategories(){
		List<CategoryDetailsForPersistance> details = fetchAllCategories();
		System.out.println(details);
	}
	
	public void testUpdateCategoryDetails(CategoryDetailsForPersistance details){
		if(updateCategoryDetails(details)==Status.SUCCESS){
			System.out.println("updated successfully");
		}
	}
}
