package com.cleanview.persistservices;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.cleanview.models.BrandDetails;
import com.cleanview.models.CategoryDetailsForPersistance;
import com.cleanview.models.FeatureDetails;
import com.cleanview.models.GroupDetails;
import com.cleanview.models.ValueDetails;
import com.cleanview.persist.common.Status;
import com.cleanview.persistservices.util.PersistUtils;

@Path("categories")
public class CategoryServices {

	@Path("add")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response addCategory(CategoryDetailsForPersistance categoryDetails){
		synchronized ("ADD_CATEGORY") {
			try{
				if(PersistUtils.persistManager.addCategory(categoryDetails)== Status.SUCCESS){
					if(PersistUtils.persistManager.fetchLastCreatedCategory(categoryDetails)==Status.SUCCESS){
						//create the items table for this category
						if(PersistUtils.persistManager.createItemDetailsTable(categoryDetails.getCategoryId())==Status.SUCCESS){
							if(addCategoryMetadata(categoryDetails)==Status.SUCCESS){
								return Response.status(200).entity(categoryDetails).build();
							}
						}
						
						return Response.status(500).entity("Error while creating metadata.").build();
					}
					return Response.status(500).entity("Cannot get the category Id").build();
				}
			
			}catch(Exception e){
				return Response.status(500).entity(e.getMessage()).build();
			}
			return Response.status(500).entity("Unknown exception").build();
		}
	}
	
	@Path("fetch_by_id")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response fetchCategoryById(CategoryDetailsForPersistance cate){
		try{
			Integer id = cate.getCategoryId();
			CategoryDetailsForPersistance details = new CategoryDetailsForPersistance();
			details.setCategoryId(id);
			if(PersistUtils.persistManager.fetchCategoryDetailsById(details)==Status.SUCCESS){
				List<FeatureDetails> featureDetailsList = new ArrayList<FeatureDetails>();
				if(PersistUtils.persistManager.fetchAllFeatureDetails(details.getCategoryId(), featureDetailsList)==Status.SUCCESS){
					for(FeatureDetails feature: featureDetailsList){
						
						List<ValueDetails> valueDetailsList = new ArrayList<ValueDetails>();
						
						if(PersistUtils.persistManager.fetchAllValuesDetails(details.getCategoryId(), feature.getFeatureId(), valueDetailsList)==Status.SUCCESS){
							for(ValueDetails value: valueDetailsList){
								feature.getValueDetails().put(value.getValue(), value);
							}
						}	    					
						details.getFeatureDetails().put(feature.getFeatureId(), feature);
					}
				}
				
				List<BrandDetails> brandsList = new ArrayList<BrandDetails>();
				if(PersistUtils.persistManager.fetchAllBrandDetails(brandsList, details.getCategoryId()) == Status.SUCCESS){
					details.setBrands(brandsList);
				}
				List<GroupDetails> groupList = new ArrayList<GroupDetails>();
				if(PersistUtils.persistManager.fetchAllGroupDetails(details.getCategoryId(),groupList) == Status.SUCCESS){
					for(GroupDetails group: groupList){
						details.getGroupDetails().put(group.getGroupId(), group);
					}
				}
				return Response.status(200).entity(details).build();
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}
	
	@Path("fetchaAll")
	@GET
	@Produces({ MediaType.APPLICATION_XML })
	public Response fetchAllCategories(){
		try{
			List<CategoryDetailsForPersistance> detailsList = new ArrayList<CategoryDetailsForPersistance>();
			
			if(PersistUtils.persistManager.fetchAllCategoryDetails(detailsList)==Status.SUCCESS){
				
				/*for(CategoryDetailsForPersistance cate: detailsList){
					List<FeatureDetails> featureDetailsList = new ArrayList<FeatureDetails>();
	    			if(PersistUtils.persistManager.fetchAllFeatureDetails(cate.getCategoryId(), featureDetailsList)==Status.SUCCESS){
	    				for(FeatureDetails feature: featureDetailsList){
	    					
	    					List<ValueDetails> valueDetailsList = new ArrayList<ValueDetails>();
	    					
	    					if(PersistUtils.persistManager.fetchAllValuesDetails(cate.getCategoryId(), feature.getFeatureId(), valueDetailsList)==Status.SUCCESS){
	    						for(ValueDetails value: valueDetailsList){
	    							feature.getValueDetails().put(value.getValue(), value);
	    						}
	    					}	    					
	    					cate.getFeatureDetails().put(feature.getFeatureId(), feature);
	    				}
	    			}
	    			
	    			List<BrandDetails> brandsList = new ArrayList<BrandDetails>();
	    			if(PersistUtils.persistManager.fetchAllBrandDetails(brandsList, cate.getCategoryId()) == Status.SUCCESS){
	    				cate.setBrands(brandsList);
	    			}
	    			List<GroupDetails> groupList = new ArrayList<GroupDetails>();
	    			if(PersistUtils.persistManager.fetchAllGroupDetails(cate.getCategoryId(),groupList) == Status.SUCCESS){
	    				for(GroupDetails group: groupList){
	    					cate.getGroupDetails().put(group.getGroupId(), group);
	    				}
	    			}
				}*/
				GenericEntity<List<CategoryDetailsForPersistance>> entity = new GenericEntity<List<CategoryDetailsForPersistance>>(detailsList){};
				 return Response.status(200).entity(entity).build();
			}		
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}
	
	
	@Path("update")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response updateCategoryId(CategoryDetailsForPersistance category){
		try{
			if(PersistUtils.persistManager.updateCategoryDetails(category)==Status.SUCCESS){
				return Response.status(200).entity("Updated category details.").build();
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}
	
	@Path("delete")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response deleteCategory(CategoryDetailsForPersistance category){
		try{
			if(PersistUtils.persistManager.deleteCategoryDetail(category)==Status.SUCCESS){
				return Response.status(200).entity("Deleted category details.").build();
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}
	
	
	//============================Preivate methods================================
	private Status addCategoryMetadata(CategoryDetailsForPersistance categoryDetails){
		try{
			Map<String,FeatureDetails> featureDetailsList = categoryDetails.getFeatureDetails();
			for(String featureId: featureDetailsList.keySet()){	
				FeatureDetails fd = featureDetailsList.get(featureId);
				fd.setCategoryId(categoryDetails.getCategoryId());
				
				if(PersistUtils.persistManager.addFeatureDetails(fd)==Status.FALIUER){
					return Status.FALIUER; 
				}
			}
			
			Map<String,GroupDetails> groupDetailsList = categoryDetails.getGroupDetails();
			for(String group: groupDetailsList.keySet()){	
				GroupDetails gd = groupDetailsList.get(group);
				gd.setCategoryId(categoryDetails.getCategoryId());
				if(PersistUtils.persistManager.addFeatureGroupDetails(gd)==Status.FALIUER){
					return Status.FALIUER; 
				}
			}
			
			List<BrandDetails> brandsList = categoryDetails.getBrands();
			for(BrandDetails brand: brandsList){				
				brand.setCategoryId(categoryDetails.getCategoryId());
				if(PersistUtils.persistManager.addBrand(brand)==Status.FALIUER){
					return Status.FALIUER; 
				}
			}
			
		}catch(Exception e){
			return Status.FALIUER;
		}
		return Status.SUCCESS;
	}
	
}
