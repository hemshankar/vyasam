package com.cleanview.persistservices;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.cleanview.models.ItemDetails;
import com.cleanview.persist.common.Status;
import com.cleanview.persistservices.util.PersistUtils;

@Path("item_details")
public class ItemServices {
	
	@Path("add")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response addItem(ItemDetails item){
		try{
			if(PersistUtils.persistManager.addItemDetails(item)==Status.SUCCESS){
				if(PersistUtils.persistManager.fetchLastCreatedItem(item, item.getCategoryId())==Status.SUCCESS){
					return Response.status(200).entity(item).build();
				}
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}
	
	@Path("serialized/add")
	@POST
	@Produces({ MediaType.TEXT_HTML })
	@Consumes({ MediaType.TEXT_HTML })
	public Response addItemSerialized(String itemStr){
		try{
			ItemDetails item =ItemDetails.populateFromString(itemStr);
			if(PersistUtils.persistManager.addItemDetails(item)==Status.SUCCESS){
				if(PersistUtils.persistManager.fetchLastCreatedItem(item, item.getCategoryId())==Status.SUCCESS){
					String itemSerialized = item.toString();
					return Response.status(200).entity(itemSerialized).build();
				}
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}
	
	@Path("fetch")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response fetchItem(ItemDetails item){
		try{		
			if(PersistUtils.persistManager.fetchItem(item)==Status.SUCCESS){				
				return Response.status(200).entity(item).build();
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}
	
	@Path("serialized/fetch")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response fetchItemSerialized(ItemDetails item){
		try{		
			if(PersistUtils.persistManager.fetchItem(item)==Status.SUCCESS){
				String strItem = item.toString();
				return Response.status(200).entity(strItem).build();
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}
	
	@Path("fetch_all")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response fetchAllItems(ItemDetails item,@Context UriInfo ui){
		try{
			MultivaluedMap<String, String> queryParams = ui.getQueryParameters();
    		Integer offset = Integer.parseInt(queryParams.getFirst("offset"));
    		Integer limit = Integer.parseInt(queryParams.getFirst("limit"));
			
			List<ItemDetails> itemDetailsList = new ArrayList<ItemDetails>();		
			if(PersistUtils.persistManager.fetchAllItemDetails(item.getCategoryId(), itemDetailsList, offset, limit)==Status.SUCCESS){
				GenericEntity<List<ItemDetails>> entity = new GenericEntity<List<ItemDetails>>(itemDetailsList){};
				return Response.status(200).entity(entity).build();
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}
	
	@Path("serialized/fetch_all")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response fetchAllItemsSerialized(ItemDetails item,@Context UriInfo ui){
		try{
			MultivaluedMap<String, String> queryParams = ui.getQueryParameters();
    		Integer offset = Integer.parseInt(queryParams.getFirst("offset"));
    		Integer limit = Integer.parseInt(queryParams.getFirst("limit"));
			
			List<ItemDetails> itemDetailsList = new ArrayList<ItemDetails>();
			List<String> serializeditems = new ArrayList<String>();
						
			if(PersistUtils.persistManager.fetchAllItemDetails(item.getCategoryId(), itemDetailsList, offset, limit)==Status.SUCCESS){
				for(ItemDetails details : itemDetailsList){
					serializeditems.add(details.toString());
				}
				GenericEntity<List<String>> entity = new GenericEntity<List<String>>(serializeditems){};
				return Response.status(200).entity(entity).build();
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}
	
	@Path("update")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response updateItemDetails(ItemDetails item){
		try{
			if(PersistUtils.persistManager.updateItemDetails(item)==Status.SUCCESS){				
				return Response.status(200).entity("Updated itemId: " + item.getItemId() + " successfully.").build();
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}
	
	@Path("delete")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response deleteItemDetails(ItemDetails item){
		try{
			if(PersistUtils.persistManager.deleteItem(item)==Status.SUCCESS){				
				return Response.status(200).entity("deleted itemId: " + item.getItemId() + " successfully.").build();
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}
	/*
	public Response addFeatureToItemDetails(ItemDetails item){
		try{
			
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}*/
}
