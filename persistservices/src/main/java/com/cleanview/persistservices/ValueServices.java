package com.cleanview.persistservices;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.cleanview.models.ValueDetails;
import com.cleanview.persist.common.Status;
import com.cleanview.persistservices.util.PersistUtils;

@Path("feature_values")

public class ValueServices {
	
	
	@Path("add")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response addValue(ValueDetails value){
		try{
			if(PersistUtils.persistManager.addValueDetails(value)==Status.SUCCESS){
				return Response.status(200).entity("Added value: " + value.getValue() + " successfully").build();
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}
	
	
	@Path("fetch_all")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response fetchAllValue(ValueDetails value){
		try{
			List<ValueDetails> valueList = new ArrayList<ValueDetails>();
			if(PersistUtils.persistManager.fetchAllValuesDetails(value.getCategoryId(),value.getFeatureId(), valueList) == Status.SUCCESS){
				
				GenericEntity<List<ValueDetails>> entity = new GenericEntity<List<ValueDetails>>(valueList){};
				
				return Response.status(200).entity(entity).build();
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}
	
	
	@Path("update")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response updateValue(ValueDetails value,@Context UriInfo ui){
		try{
			MultivaluedMap<String, String> queryParams = ui.getQueryParameters();
    		String originalValue = queryParams.getFirst("value");
			if(PersistUtils.persistManager.updateValueDetails(value.getCategoryId(), 
						value.getFeatureId(), value, originalValue)==Status.SUCCESS){
				return Response.status(200).entity("Updated value:" + value.getValue() + " successfully").build();
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}
	
	@Path("delete")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response deleteValue(ValueDetails value){
		try{
			
			if(PersistUtils.persistManager.deleteValueDetails(value)==Status.SUCCESS){
				return Response.status(200).entity("Deleted value:" + value.getValue() + " successfully").build();
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}
}
