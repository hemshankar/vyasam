package com.cleanview.persistservices;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.cleanview.models.BrandDetails;
import com.cleanview.persist.common.Status;
import com.cleanview.persistservices.util.PersistUtils;

@Path("brand")
public class BrandServices {
	
	@Path("add")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response addBrand(BrandDetails brand){
		synchronized ("ADD_BRAND") {
			try{
				if(PersistUtils.persistManager.addBrand(brand)==Status.SUCCESS){
					if(PersistUtils.persistManager.fetchLastCreatedBrand(brand)==Status.SUCCESS){
						
						return Response.status(200).entity(brand).build();
					}
				}
			}catch(Exception e){
				return Response.status(500).entity(e.getMessage()).build();
			}
			return Response.status(500).entity("Unknown exception").build();
		}
	}
	
	@Path("fetch_all")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response fetchAllBrand(BrandDetails brand){
		try{
			List<BrandDetails> brandsList = new ArrayList<BrandDetails>();
			if(PersistUtils.persistManager.fetchAllBrandDetails(brandsList, brand.getCategoryId())==Status.SUCCESS){
				GenericEntity<List<BrandDetails>> entity = new GenericEntity<List<BrandDetails>>(brandsList){};
				return Response.status(200).entity(entity).build();	
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}
	
	@Path("update")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response testFetchAllBrand(BrandDetails brand){
		try{
			if(PersistUtils.persistManager.updateBrandDetails(brand)==Status.SUCCESS){
				return Response.status(200).entity("Updated brand successfully").build();
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}	
	@Path("delete")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response deleteBrand(BrandDetails brand){
		try{
			if(PersistUtils.persistManager.deleteBrand(brand)==Status.SUCCESS){
				return Response.status(200).entity("Deleted brand successfully").build();
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}
}
