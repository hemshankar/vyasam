package com.cleanview.persistservices.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.cleanview.persist.MySQLPersistanceManager;

public class PersistUtils {

	public static MySQLPersistanceManager persistManager = new MySQLPersistanceManager();
	
	public static Object objFromString(String s) throws Exception{
		byte b[] = s.getBytes(); 
	     ByteArrayInputStream bi = new ByteArrayInputStream(b);
	     ObjectInputStream si = new ObjectInputStream(bi);
	     return si.readObject();		
	}
	
	public static String objToString(Object o) throws Exception {
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
	     ObjectOutputStream so = new ObjectOutputStream(bo);
	     so.writeObject(o);
	     so.flush();
	     return bo.toString();
	}
	
}
