package com.cleanview.persistservices;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.cleanview.models.FeatureDetails;
import com.cleanview.persist.common.Status;
import com.cleanview.persistservices.util.PersistUtils;

@Path("feature")
public class FeatureServices {
	@Path("add")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response addFeature(FeatureDetails feature){
		try{
			if(PersistUtils.persistManager.addFeatureDetails(feature)==Status.SUCCESS){
				return Response.status(200).entity("Added feature successfully").build();
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}
	
	@Path("fetchAll")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response fetchAllFeature(FeatureDetails feature){
		try{
			List<FeatureDetails> featureDetailsList = new ArrayList<FeatureDetails>();
			if(PersistUtils.persistManager.fetchAllFeatureDetails(feature.getCategoryId(), featureDetailsList)==Status.SUCCESS){
				GenericEntity<List<FeatureDetails>> entity = new GenericEntity<List<FeatureDetails>>(featureDetailsList){};
				return Response.status(200).entity(entity).build();				
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}
	
	@Path("update")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response updateFeature(FeatureDetails feature,@Context UriInfo ui){
		try{
			MultivaluedMap<String, String> queryParams = ui.getQueryParameters();
    		String originalFeatureId = queryParams.getFirst("originalFeatureId");
			if(PersistUtils.persistManager.updateFeatureDetails(feature, feature.getCategoryId(), originalFeatureId)==Status.SUCCESS){
				return Response.status(200).entity("Feature updated successfully").build();
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}	
	
	@Path("delete")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public Response deleteFeature(FeatureDetails feature){
		try{
			if(PersistUtils.persistManager.deleteFeature(feature)==Status.SUCCESS){
				return Response.status(200).entity("Feature deleted successfully").build();
			}
		}catch(Exception e){
			return Response.status(500).entity(e.getMessage()).build();
		}
		return Response.status(500).entity("Unknown exception").build();
	}
}
