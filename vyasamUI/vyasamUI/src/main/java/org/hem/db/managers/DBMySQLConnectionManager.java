/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hem.db.managers;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

/**
 *
 * @author Hem
 */
public class DBMySQLConnectionManager implements Serializable
{  
    private static  Connection conn;
    private static ResultSet  rs;
    
    public static Connection openConnection(String username, String password)
    {
        conn = null;
        Properties prop = new Properties();
        try
        {
            String url="jdbc:mysql://localhost:3306/mysql";
            prop.setProperty("user",username); 
            prop.setProperty("password",password); 
            Driver driver = new com.mysql.jdbc.Driver();
            conn = driver.connect(url,prop);
            if(conn==null)   
            {
                //Logger.log(Level.WARNING,"connection failed");                
            }
        }
        catch(Exception e)
        {
            //Logger.log("error","DBConnection.openConnection() Error:" + e.toString());
        }
        return conn;
    }
    
    public static Connection openDefaultConnection()
    {
        conn = null;
        Properties prop = new Properties();
        try
        {
            String url="jdbc:mysql://localhost:3306/mysql";
            prop.setProperty("user", "root");
            prop.setProperty("password","infa");
            Driver driver = new com.mysql.jdbc.Driver();
            conn = driver.connect(url,prop);
            if(conn==null)   
            {
                //Logger.log(TraceTags.LEVEL_ERROR,"connection failed");                 
            }
        }
        catch(Exception e)
        {
             //Logger.log("error","DBConnection.openDefaultConnection() Error:" + e.toString());
        }
        return conn;
    }
    
    public static void closeConnection(Connection conn)
    {
        try
        {
            conn.close();
        }
        catch(Exception e)
        {
             //Logger.log("error","DBConnection.closeConnection() Error:" + e.toString());
        }
    }
    
    public static ResultSet getResultSet(String query)
    {
        rs = null;
        try
        {            
            Statement stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
        }
        catch(Exception e)
        {
            //Logger.log("error","DBConnection.getResultSet() Error for query: '" + query + "' :" + e.toString());                     
        }
        return rs;
    }    
}
