/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.compareo.constants;

/**
 *
 * @author hsahu
 */
public class DBObjects {
    
    public static final String SCHEMA = "compareo";
    public static final String TABLE_ITEM_LIST_SOURCE = "item_list_source";
    public static final String TABLE_ITEM_DETAILS = "item_details";
}
