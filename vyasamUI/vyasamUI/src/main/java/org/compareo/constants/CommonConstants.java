/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.compareo.constants;

/**
 *
 * @author Hem
 */
public class CommonConstants 
{
    public static final String LOG_LOCATION = "C:/D Drive/Projects/CompareO/logs/";
    public static final String LOG_FILENAME = "comparo";
          
    //generic constants
    public static final String SUCCESS = "SUCCESS";
    public static final String ITEMS_DETIALS_LIST = "ITEMS_DETIALS";
    public static final String ITEMS_CLASS_LIST = "ITEMS_CLASS_LIST";
    public static final String ITEMS_SELECTED_CLASS = "ITEMS_SELECTED_CLASS";
    public static final String ITEMS_SOURCE_DETAILS = "ITEMS_DETIALS_SOURCE";
	public final static String VSERVER_HOSTNAME = "VSERVER_HOSTNAME";
	public final static String VSERVER_PORT = "VSERVER_PORT";
	public final static String VSERVER_HOME = "VSERVER_HOME";
    public static final String EXTRACTION_STEPS = "EXTRACTION_STEPS";
    public static final String HTML_CONTENT = "HTML_CONTENT";
}
