/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.compareo.item.extractor.details;

import com.compareo.classes.ClassesPM;
import com.comparo.utility.GenericUtilities;
import com.vyasam.constants.FetchStatus;
import com.vyasam.jsoup.models.DetailsExtractionSteps;
import com.vyasam.jsoup.models.ElementProperties;
import com.vyasam.jsoup.models.ElementProperty;
import com.vyasam.models.ItemDetails;
import com.vyasam.models.ItemsSourceDetails;
import com.vyasam.utility.logger.Logger;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author hsahu
 */
@ManagedBean(name = "detailsExtractionVB")
@RequestScoped

public class DetailsExtractorVB implements Serializable{
    private DetailsExtractorPM pm = null;
    private ClassesPM cpm = null;
    
    public DetailsExtractorVB()
    {
        pm = (DetailsExtractorPM) GenericUtilities.getPageModel("#{detailsExtractorPM}",DetailsExtractorPM.class);
        cpm = (ClassesPM) GenericUtilities.getPageModel("#{classesPM}", ClassesPM.class);
    }
    public void fetchAttributes()
    {        
        pm.setItemDetails(pm.getItemDetailsList().get(Integer.parseInt(pm.getSelecteItemSno())));
                
        DetailsExtractionSteps detailsExtrationSteps = new DetailsExtractionSteps();
        detailsExtrationSteps.setRootProps(pm.getRootElemProps().updateProperties());
        detailsExtrationSteps.setKeyProps(pm.getAttrKeyElemProps().updateProperties());
        detailsExtrationSteps.setValueProps(pm.getAttrValueElemProps().updateProperties());
        
        pm.getItemDetails().setExtractionSteps(detailsExtrationSteps);
        pm.setFeatures(pm.getExtractmanager().fetchItemDetails(pm.getItemDetails()));
        pm.getItemDetails().setStatus(FetchStatus.FETCHED);
        pm.getItemDetails().setFetchedTime(System.currentTimeMillis() + "");
        pm.getItemDetails().setItemFeatures(pm.getFeatures());        
    }
    
    public void save()
    {
        try
        {
            ItemsSourceDetails source = pm.getSourceDetails();
            source.setClassId(cpm.getSelectedClass().getId());            
            
            for(ItemDetails item: source.getItemDetailsList())
            {
                DetailsExtractionSteps detailsES = new DetailsExtractionSteps();
                
                detailsES.setRootProps(pm.getRootElemProps());
                detailsES.setKeyProps(pm.getAttrKeyElemProps());
                detailsES.setValueProps(pm.getAttrValueElemProps());
                
                item.setExtractionSteps(detailsES);
                //item.setStatus(FetchStatus.NEW);
                item.setClassId(cpm.getSelectedClass().getId());
                item.setSourceName(source.getSourceName());
                // this should called after all the set methods on ItemDetails has been called.
                item.generateXMLs();                
            }
            
            // this should called after all the set methods on ItemsSourceDetails has been called.
            source.generateXMLs();
            DetailsExtractorService.saveItemsSource(source);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            Logger.log("error", "Error while saving the items list: " + e.getLocalizedMessage());
        }
    }
    
    public void addNewElement(ElementProperties elemProps)
    {
        elemProps.addProperty((new ElementProperty()).add("", ""));        
    }

    public void addNewProperty(ElementProperty elemProp)
    {
        elemProp.add("", "");
    }
}
