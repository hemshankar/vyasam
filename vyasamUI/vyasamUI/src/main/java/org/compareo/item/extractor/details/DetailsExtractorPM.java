/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.compareo.item.extractor.details;

import com.comparo.utility.GenericUtilities;
import com.vyasam.category.models.VClass;
import com.vyasam.constants.StepsConstants;
import com.vyasam.extractor.ExtractionManager;
import com.vyasam.jsoup.models.DetailsExtractionSteps;
import com.vyasam.jsoup.models.ElementProperties;
import com.vyasam.jsoup.models.ElementProperty;
import com.vyasam.models.ExtractionSteps;
import com.vyasam.models.Features;
import com.vyasam.models.ItemDetails;
import com.vyasam.models.ItemsSourceDetails;
import com.vyasam.utilities.VyasamModelsUtility;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.compareo.constants.CommonConstants;

import com.comparo.utility.ClassDetails;

/**
 *
 * @author hsahu
 */

@ManagedBean(name="detailsExtractorPM")
@ViewScoped
public class DetailsExtractorPM implements Serializable
{
    private ItemsSourceDetails sourceDetails = null;

    private ItemDetails itemDetails = null;
    private String selecteItemSno = null;
    
    private List<ItemListForSelection> itemListForSelection = new ArrayList<>();

     //-------------------For feature extraction
    
    private ExtractionManager extractmanager = null;

    private List<String> propOptionsList = null;
    private List<ItemDetails> itemDetailsList = null;
    
    private ElementProperties rootElemProps = null;
    private ElementProperties attrKeyElemProps = null;
    private ElementProperties attrValueElemProps = null;
    
    private Features features = null;
    private ExtractionSteps steps= null;
    public ExtractionSteps getSteps() {
		return steps;
	}

	public void setSteps(ExtractionSteps steps) {
		this.steps = steps;
	}

	public class ItemListForSelection implements Serializable
    {
        String itemName;
        String sNo;

        public ItemListForSelection(String title,String sno)
        {
            this.itemName = title;
            this.sNo = sno;
        }
        
        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public String getSNo() {
            return sNo;
        }

        public void setSNo(String sNo) {
            this.sNo = sNo;
        }
    }
    
    public DetailsExtractorPM()
    {
    	try{
	        rootElemProps = ElementProperties.newElementProperties().addProperty((new ElementProperty().add("", "")));
	        attrKeyElemProps = ElementProperties.newElementProperties().addProperty((new ElementProperty().add("", "")));
	        attrValueElemProps = ElementProperties.newElementProperties().addProperty((new ElementProperty().add("", "")));
	        
	        //itemDetailsList = (List<ItemDetails>)GenericUtilities.getFromSession(CommonConstants.ITEMS_DETIALS_LIST);
	        sourceDetails = (ItemsSourceDetails) GenericUtilities.getFromSession(CommonConstants.ITEMS_SOURCE_DETAILS);        
	                
	        steps = (ExtractionSteps) GenericUtilities.getFromSession(CommonConstants.EXTRACTION_STEPS);
	        
	        if(steps!=null && steps.getDetailsExtrectionStepsXML() != null){
	        	try{
			        DetailsExtractionSteps detailsSteps = VyasamModelsUtility
			        											.serializationUtility
			        											.XMLToObjetc(steps.getDetailsExtrectionStepsXML(),DetailsExtractionSteps.class);
			        rootElemProps = detailsSteps.getRootProps();
			        attrKeyElemProps = detailsSteps.getKeyProps();
			        attrValueElemProps = detailsSteps.getValueProps();
	        	}catch(Exception e){
	        		e.printStackTrace();
	        	}
	        }
	        itemDetailsList = sourceDetails.getItemDetailsList();
	        if(itemDetailsList!=null &&  itemDetailsList.size()>=01)
	        {
	            itemDetails = itemDetailsList.get(0);   
	            int count = 0;
	            for(ItemDetails details: itemDetailsList)
	            {
	                itemListForSelection.add(new ItemListForSelection(details.getTitle() ,"" + count++));                        
	            }
	        }
	        
	        propOptionsList = new ArrayList<String>();
	        
	        propOptionsList.add(StepsConstants.HAS_ID);        
	        propOptionsList.add(StepsConstants.HAS_CLASS);
	        propOptionsList.add(StepsConstants.HAS_TAG);
	        propOptionsList.add(StepsConstants.HAS_ATTRIBUTE);
	        propOptionsList.add(StepsConstants.HAS_ATTRIBUTE_WITH_VALUE);
	        
	        propOptionsList.add(StepsConstants.HAS_TEXT_LIKE);
	        propOptionsList.add(StepsConstants.HAS_NO_TEXT_LIKE);
	        propOptionsList.add(StepsConstants.CONTAINS_TEXT_LIKE);      
	        propOptionsList.add(StepsConstants.CONTAINS_NO_TEXT_LIKE);      
	        propOptionsList.add(StepsConstants.HAS_EMPTY_TEXT);
	        propOptionsList.add(StepsConstants.HAS_NO_EMPTY_TEXT);   
	        
	        extractmanager = new ExtractionManager();
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    }
    
      public Features getFeatures() {
        return features;
    }

    public void setFeatures(Features features) {
        this.features = features;
    }
    
    public List<ItemListForSelection> getItemListForSelection() {
        return itemListForSelection;
    }

    public void setItemListForSelection(List<ItemListForSelection> itemListForSelection) {
        this.itemListForSelection = itemListForSelection;
    }
    
    public String getSelecteItemSno() {
        return selecteItemSno;
    }

    public void setSelecteItemSno(String selecteItem) {
        this.selecteItemSno = selecteItem;
    }    
    
    public ItemDetails getItemDetails() {
        return itemDetails;
    }

    public void setItemDetails(ItemDetails itemDetails) {
        this.itemDetails = itemDetails;
    }
    
    public ExtractionManager getExtractmanager() {
        return extractmanager;
    }

    public void setExtractmanager(ExtractionManager extractmanager) {
        this.extractmanager = extractmanager;
    }

    public List<String> getPropOptionsList() {
        return propOptionsList;
    }

    public void setPropOptionsList(List<String> propOptionsList) {
        this.propOptionsList = propOptionsList;
    }
    
    public List<ItemDetails> getItemDetailsList() {
        return itemDetailsList;
    }

    public void setItemDetailsList(List<ItemDetails> itemDetailsList) {
        this.itemDetailsList = itemDetailsList;
    }
    
    public ElementProperties getRootElemProps() {
        return rootElemProps;
    }

    public void setRootElemProps(ElementProperties rootElemProps) {
        this.rootElemProps = rootElemProps;
    }

    public ElementProperties getAttrKeyElemProps() {
        return attrKeyElemProps;
    }

    public void setAttrKeyElemProps(ElementProperties attrKeyElemProps) {
        this.attrKeyElemProps = attrKeyElemProps;
    }

    public ElementProperties getAttrValueElemProps() {
        return attrValueElemProps;
    }

    public void setAttrValueElemProps(ElementProperties attrValueElemProps) {
        this.attrValueElemProps = attrValueElemProps;
    }
        
    public ItemsSourceDetails getSourceDetails() {
        return sourceDetails;
    }

    public void setSourceDetails(ItemsSourceDetails sourceDetails) {
        this.sourceDetails = sourceDetails;
    }    
}
