/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.compareo.item.extractor.details;

import com.vyasam.common.serialization.SerializationUtility;
import com.vyasam.jsoup.models.DetailsExtractionSteps;
import com.vyasam.models.Features;
import com.vyasam.models.ItemDetails;
import com.vyasam.models.ItemsSourceDetails;
import com.vyasam.utility.logger.Logger;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import org.compareo.constants.CommonConstants;
import org.glassfish.jersey.client.ClientConfig;
import org.hem.db.managers.DBMySQLConnectionManager;

/**
 *
 * @author hsahu
 */
public class DetailsExtractorService implements Serializable{
     public static String insertItemsDetails(ItemDetails itemDetails) throws Exception
    {
        String insertionResult = CommonConstants.SUCCESS;
        Connection conn =  null;
        PreparedStatement pst = null;
        try
        { 
            //Inser the ansid in the questions ansererIds coulmn 
            conn = DBMySQLConnectionManager.openDefaultConnection();
            pst = conn.prepareStatement(DetailsExtractorSQLList.INSERT_ITEMS_DETAILS);

            SerializationUtility sutility = new SerializationUtility();
            //pst.setInt(1, answer.getAnsId());
            pst.setString(1, itemDetails.getTitle());
            pst.setString(2, itemDetails.getSourceLink());
            pst.setString(3, sutility.objectToXML(Features.class,itemDetails.getItemFeatures()));
            pst.setString(4, itemDetails.getFetchedTime());
            pst.setInt(5, itemDetails.getId());
            pst.setString(6, sutility.objectToXML(DetailsExtractionSteps.class,itemDetails.getExtractionSteps()));
//            pst.setInt(6, answer.getRating());
//            pst.setInt(7,answer.getQuestionId());
//            pst.setInt(8, answer.getUserSno());

            int inserted = pst.executeUpdate();

            if(inserted >=0)
            {
                Logger.log("info","Inserted Items List Source: " + itemDetails.getTitle()+ " SUCCESSFULLY");
            }
            else
            {
                insertionResult = "FAILED";
                Logger.log("error","Insertion for ItemsListSource : " + itemDetails.getTitle() + " FAILED");                
            }           
        }
        catch(Exception e)
        {
            insertionResult = "FAILED";
            Logger.log("error","Insertion for ItemsListSource: " + itemDetails.getTitle()+ " FAILED with exception: " + e.toString());
        }
        finally
        {
            //Close the DB connection
            DBMySQLConnectionManager.closeConnection(conn);
        }
        
        return insertionResult;        
    }
     
     public static String saveItemsSource(ItemsSourceDetails itemsSource) throws Exception
    {
        String insertionResult = CommonConstants.SUCCESS;

        
        String URL2 = DetailsExtractorURIs.SAVE_UPDATE_ITEM_SOURCE;
        ClientConfig config = new ClientConfig();
        Client client = ClientBuilder.newClient(config);
        WebTarget target = client.target(URL2);

        target.request(MediaType.APPLICATION_XML).
                                post(Entity.entity(itemsSource, MediaType.APPLICATION_XML),ItemsSourceDetails.class);

        return insertionResult;        
    }
}
