/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.compareo.item.extractor.itemlist;

import com.compareo.classes.ClassesPM;
import com.compareo.extractionsteps.ExtractionStepsPM;
import com.comparo.utility.ClassDetails;
import com.comparo.utility.GenericUtilities;
import com.vyasam.category.models.VClass;

import java.io.Serializable;

import com.vyasam.jsoup.models.*;
import com.vyasam.jsoup.models.exceptions.VyasamModelsException;
import com.vyasam.models.ItemDetails;
import com.vyasam.utility.logger.Logger;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.compareo.constants.CommonConstants;
import org.compareo.item.browser.BrowsePM;
import org.jsoup.Jsoup;

/**
 * 
 * @author hsahu
 */
@ManagedBean(name = "extractorVB")
@RequestScoped
public class ExtractorViewBean implements Serializable {

	private ExtractorPageModel pm = null;
	private ClassesPM cpm = null;
	ExtractionStepsPM spm = null;

	public ExtractorViewBean() {
		pm = (ExtractorPageModel) GenericUtilities.getPageModel("#{extractor}",
				ExtractorPageModel.class);
		cpm = (ClassesPM) GenericUtilities.getPageModel("#{classesPM}",
				ClassesPM.class);
		spm = (ExtractionStepsPM) GenericUtilities.getPageModel(
				"#{extractionStepsPM}", ExtractionStepsPM.class);
	}

	public void useSourceLink() {
		try {
			pm.getItmesSoruce().setHtmlContent(
					Jsoup.connect(pm.getItmesSoruce().getSourceLink()).get()
							.html());
		} catch (IOException ex) {
			ex.printStackTrace();
			Logger.log(Level.SEVERE.toString(), ex.getMessage());
		}
	}

	public void extractItemList() {

		// pm.getRootElementProperties();
		ListExtractionSteps leSteps = new ListExtractionSteps();
		leSteps.setRootProps(pm.getRootElementProperties().updateProperties());
		leSteps.setLinkProps(pm.getItemsElementProperties().updateProperties());

		pm.getItmesSoruce().setExtractionSteps(leSteps);
		pm.setItemDetailsList(pm.getExtractmanager().getItemList(
				pm.getItmesSoruce()));
		pm.getItmesSoruce().setItemDetailsList(pm.getItemDetailsList());
	}

	public void saveToSession() {
		pm.getItmesSoruce().setClassId(cpm.getSelectedClass().getId());
		/*
		 * for(ItemDetails item: pm.getItemDetailsList()){ if(!item.getValid()){
		 * pm.getItemDetailsList().remove(item); } }
		 */

		// remove invalid items
		Iterator<ItemDetails> itr = pm.getItemDetailsList().iterator();
		ItemDetails ids = null;
		if (itr != null) {
			while (itr.hasNext()) {
				ids = itr.next();
				if (!ids.getValid()) {
					itr.remove();
				}
			}
		}

		GenericUtilities.putToSession(CommonConstants.ITEMS_DETIALS_LIST,
				pm.getItemDetailsList());
		GenericUtilities.putToSession(CommonConstants.ITEMS_SOURCE_DETAILS,
				pm.getItmesSoruce());
		GenericUtilities.putToSession(CommonConstants.EXTRACTION_STEPS,
				spm.getSelecetedStep());

		GenericUtilities.redirect("itemDetails.xhtml");
	}

	public void addNewElement(ElementProperties elemProps) {
		elemProps.addProperty((new ElementProperty()).add("", ""));
	}

	public void addNewProperty(ElementProperty elemProp) {
		elemProp.add("", "");
	}

	public void proceed() {
		GenericUtilities.putToSession(CommonConstants.HTML_CONTENT, pm
				.getItmesSoruce().getHtmlContent());
		GenericUtilities.putToSession(CommonConstants.ITEMS_SOURCE_DETAILS,
				pm.getItmesSoruce());
		GenericUtilities.redirect("itemList.xhtml");
	}

	public void selecteAllItems() {
		for (ItemDetails item : pm.getItemDetailsList()) {
			item.setValid(true);
		}
	}

	public void deSelecteAllItems() {
		for (ItemDetails item : pm.getItemDetailsList()) {
			item.setValid(false);
		}
	}
}
