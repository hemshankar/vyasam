/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.compareo.item.extractor.itemlist;

import com.comparo.utility.ClassDetails;
import com.comparo.utility.GenericUtilities;
import com.vyasam.category.models.VClass;
import com.vyasam.models.ItemDetails;
import com.vyasam.models.ItemsSourceDetails;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.compareo.constants.CommonConstants;

import com.vyasam.constants.StepsConstants;
import com.vyasam.extractor.ExtractionManager;
import com.vyasam.jsoup.models.ElementProperties;
import com.vyasam.jsoup.models.ElementProperty;

/**
 *
 * @author hsahu
 */

@ManagedBean (name="extractor")
@ViewScoped
public class ExtractorPageModel implements Serializable{

    //private VClass selectedClass = new VClass();
    
    private ItemsSourceDetails itemsSoruce = null;
    
    private List<String> propOptionsList = null;
    
    private ExtractionManager extractmanager = null;

    private List<ItemDetails> itemDetailsList = null;
   
    private ElementProperties rootElementProperties = null;
    private ElementProperties itemsElementProperties = null;    
    //private List<ClassDetails> classDetailsList = new ArrayList();

    private String newClass="";
    
    public ExtractorPageModel()
    {
        rootElementProperties = ElementProperties.newElementProperties()
                                        .addProperty((new ElementProperty()).add("", ""));
        itemsElementProperties = ElementProperties.newElementProperties()
                                        .addProperty((new ElementProperty()).add("", ""));
        
        itemsSoruce = (ItemsSourceDetails) GenericUtilities.getFromSession(CommonConstants.ITEMS_SOURCE_DETAILS);
        if(itemsSoruce == null)
        	itemsSoruce = new ItemsSourceDetails();
        
        propOptionsList = new ArrayList<String>();
        propOptionsList.add(StepsConstants.HAS_ID);
        propOptionsList.add(StepsConstants.HAS_CLASS);
        propOptionsList.add(StepsConstants.HAS_TAG);
        propOptionsList.add(StepsConstants.HAS_ATTRIBUTE);
        propOptionsList.add(StepsConstants.HAS_ATTRIBUTE_WITH_VALUE);
        
        propOptionsList.add(StepsConstants.HAS_TEXT_LIKE);
        propOptionsList.add(StepsConstants.HAS_NO_TEXT_LIKE);
        propOptionsList.add(StepsConstants.CONTAINS_TEXT_LIKE); 
        propOptionsList.add(StepsConstants.CONTAINS_NO_TEXT_LIKE); 
        propOptionsList.add(StepsConstants.HAS_EMPTY_TEXT);
        propOptionsList.add(StepsConstants.HAS_NO_EMPTY_TEXT);
        
        extractmanager = new ExtractionManager();
        
       /*ClassDetails classDetails = new ClassDetails();
        classDetails.setAvailableClasses(GenericUtilities.getTopLevelClasses());
        classDetailsList.add(classDetails);        */
    }

    public List<ItemDetails> getItemDetailsList() {
        return itemDetailsList;
    }

    public void setItemDetailsList(List<ItemDetails> itemDetailsList) {
        this.itemDetailsList = itemDetailsList;
    }
    
    public ExtractionManager getExtractmanager() {
        return extractmanager;
    }

    public void setExtractmanager(ExtractionManager extractmanager) {
        this.extractmanager = extractmanager;
    }    
    
    public ItemsSourceDetails getItmesSoruce() {
        return itemsSoruce;
    }

    public void setItemsSoruce(ItemsSourceDetails itmesSoruce) {
        this.itemsSoruce = itmesSoruce;
    }

    
     public List<String> getPropOptionsList() {
        return propOptionsList;
    }

    public void setPropOptionsList(List<String> propOptionsList) {
        this.propOptionsList = propOptionsList;
    }

    public ElementProperties getRootElementProperties() {
        return rootElementProperties;
    }

    public void setRootElementProperties(ElementProperties rootElementProperties) {
        this.rootElementProperties = rootElementProperties;
    }

    public ElementProperties getItemsElementProperties() {
        return itemsElementProperties;
    }

    public void setItemsElementProperties(ElementProperties itemsElementProperties) {
        this.itemsElementProperties = itemsElementProperties;
    }    
    
   /* public List<ClassDetails> getClassDetailsList() {
        return classDetailsList;
    }

    public void setClassDetailsList(List<ClassDetails> classDetailsList) {
        this.classDetailsList = classDetailsList;
    }*/
    
    /*public VClass getSelectedClass() {
        return selectedClass;
    }

    public void setSelectedClass(VClass selectedClass) {
        this.selectedClass = selectedClass;
    }*/

    public String getNewClass() {
        return newClass;
    }

    public void setNewClass(String newClass) {
        this.newClass = newClass;
    }
}
