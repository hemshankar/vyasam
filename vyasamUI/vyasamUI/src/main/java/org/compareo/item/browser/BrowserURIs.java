/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.compareo.item.browser;

import java.io.Serializable;

import org.compareo.constants.CommonConstants;

import com.cleanview.ui.constants.CommonUIConstants;
import com.cleanview.utilities.UIUtil;

/**
 *
 * @author hsahu
 */
public class BrowserURIs implements Serializable{
    
    /*public final static String FETCH_ITEM_DETAILS_FOR_A_CLASS = "http://" 
                                                            + CommonConstants.VSERVER_HOST 
                                                            + ":" + CommonConstants.VSERVER_PORT 
                                                            + "/vServer/persist/ItemDetailsPM/fetch/itemDetails/query";*/
    public static final String FETCH_ITEM_DETAILS_FOR_A_CLASS = UIUtil.getConfig(CommonUIConstants.VSERVER_HOME) 
			+ UIUtil.getConfig(CommonUIConstants.FETCH_ITEM_DETAILS_FOR_A_CLASS);
}
