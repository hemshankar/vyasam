/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.compareo.item.extractor.details;

import org.compareo.constants.CommonConstants;

import com.cleanview.ui.constants.CommonUIConstants;
import com.cleanview.utilities.UIUtil;

/**
 *
 * @author hsahu
 */
public class DetailsExtractorURIs {
    
   /* public final static String SAVE_UPDATE_ITEM_SOURCE = "http://" 
                                                            + CommonConstants.VSERVER_HOST 
                                                            + ":" + CommonConstants.VSERVER_PORT 
                                                            + "/vServer/persist/ItemSourceDetailsPM/save_update/itemsSource";*/
    
    public static final String SAVE_UPDATE_ITEM_SOURCE = UIUtil.getConfig(CommonUIConstants.VSERVER_HOME) 
			+ UIUtil.getConfig(CommonUIConstants.SAVE_UPDATE_ITEM_SOURCE);
    
}
