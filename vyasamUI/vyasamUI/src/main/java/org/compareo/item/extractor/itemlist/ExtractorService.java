/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.compareo.item.extractor.itemlist;


import com.vyasam.common.serialization.SerializationUtility;
import com.vyasam.jsoup.models.ListExtractionSteps;
import com.vyasam.models.ItemsSourceDetails;
import com.vyasam.utility.logger.Logger;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import org.compareo.constants.CommonConstants;
import org.hem.db.managers.DBMySQLConnectionManager;

/**
 *
 * @author hsahu
 */
public class ExtractorService implements Serializable{
    
    
    public static String insertItemsListSource(ItemsSourceDetails ils) throws Exception
    {
        String insertionResult = CommonConstants.SUCCESS;
        Connection conn =  null;
        PreparedStatement pst = null;
        try
        { 
            //Inser the ansid in the questions ansererIds coulmn 
            conn = DBMySQLConnectionManager.openDefaultConnection();
            pst = conn.prepareStatement(ExtractorSQLList.INSERT_ITEMS_LIST_SORUCE);

             SerializationUtility sutility = new SerializationUtility();
            //pst.setInt(1, answer.getAnsId());
            pst.setString(1, ils.getTitle());
            pst.setInt(2, ils.getClassId());
            pst.setString(3, ils.getSourceLink());
            pst.setString(4, ils.getHtmlContent());
            pst.setString(5, ils.getFetchedTime());
            pst.setString(6, sutility.objectToXML(ListExtractionSteps.class,ils.getExtractionSteps()));
//            pst.setInt(6, answer.getRating());
//            pst.setInt(7,answer.getQuestionId());
//            pst.setInt(8, answer.getUserSno());

            int inserted = pst.executeUpdate();

            if(inserted >=0)
            {
                Logger.log("info","Inserted Items List Source: " + ils.getTitle()+ " SUCCESSFULLY");
            }
            else
            {
                insertionResult = "FAILED";
                Logger.log("error","Insertion for ItemsListSource : " + ils.getTitle() + " FAILED");                
            }           
        }
        catch(Exception e)
        {
            insertionResult = "FAILED";
            Logger.log("error","Insertion for ItemsListSource: " + ils.getTitle()+ " FAILED with exception: " + e.toString());
        }
        finally
        {
            //Close the DB connection
            DBMySQLConnectionManager.closeConnection(conn);
        }
        
        return insertionResult;        
    }
    
}
