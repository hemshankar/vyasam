/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.compareo.item.browser;

import com.compareo.classes.ClassesPM;
import com.comparo.utility.GenericUtilities;
import com.vyasam.category.models.VClass;
import com.vyasam.models.ItemDetails;
import com.vyasam.query.models.DataType;
import com.vyasam.query.models.Query;
import java.io.Serializable;
import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author hsahu
 */
@ManagedBean(name = "browseVB")
@RequestScoped
public class BrowseVB implements Serializable {

    BrowsePM pm = null;
    ClassesPM cpm = null;

    public BrowseVB() {
        pm = (BrowsePM) GenericUtilities.getPageModel("#{browsePM}", BrowsePM.class);
        cpm = (ClassesPM) GenericUtilities.getPageModel("#{classesPM}", ClassesPM.class);
    }

    public void updateList() {
        try {
            VClass selectedClass = cpm.getSelectedClass();
            List<Integer> classIds = new ArrayList<>();
            GenericUtilities.getClassIds(selectedClass, classIds);
            String placeHolders = "";
            Query query = new Query();
            query.setParameters(new ArrayList<String>());
            query.setParametersTypes(new ArrayList<DataType>());
            for (Integer classId : classIds) {
                query.getParameters().add(classId.toString());
                query.getParametersTypes().add(DataType.INTEGER);
                placeHolders = placeHolders + ",?";
            }

            if (placeHolders.length() > 0) {
                placeHolders = placeHolders.substring(1);
            }

            String extension = " WHERE classId in (" + placeHolders + ")";
            query.setQuery(BrowserSQL.GET_ALL_ITEMS + extension);

            pm.setItemList(BrowseServices.saveItemsSource(query));
            for (ItemDetails id : pm.getItemList()) {
                id.generateFromXML();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
