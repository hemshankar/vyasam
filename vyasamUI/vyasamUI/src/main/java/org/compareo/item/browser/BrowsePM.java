/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.compareo.item.browser;

import com.vyasam.models.ItemDetails;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author hsahu
 */
@ManagedBean(name="browsePM")
@ViewScoped
public class BrowsePM implements Serializable{
    
    private List<ItemDetails> itemList;
    public BrowsePM()
    {
    }
    
    public List<ItemDetails> getItemList() {
        return itemList;
    }

    public void setItemList(List<ItemDetails> itemList) {
        this.itemList = itemList;
    }    
}
