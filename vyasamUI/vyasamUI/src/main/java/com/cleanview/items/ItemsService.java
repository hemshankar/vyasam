package com.cleanview.items;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import com.cleanview.models.ItemDetails;
import com.cleanview.persist.common.Status;
import com.cleanview.utilities.UIUtil;

public class ItemsService implements Serializable{
	
	private static final long serialVersionUID = 1L;

	public static ItemDetails addItemDetails(ItemDetails obj1){
				
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(ItemsServiceURLS.ADD_ITEM);
		
		Response res = target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(obj1, MediaType.APPLICATION_XML));
		return res.readEntity(ItemDetails.class);
	}
	
	public static ItemDetails addItemDetailsSerialized(ItemDetails obj1) throws Exception{
		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(ItemsServiceURLS.ADD_ITEM_SERIALIZED);
		String serializedItem = obj1.toString();
		Response res = target.request(MediaType.TEXT_HTML).
					post(Entity.entity(serializedItem, MediaType.TEXT_HTML));
		String itemStr = res.readEntity(String.class);
		return ItemDetails.populateFromString(itemStr);
	}
	
	public static ItemDetails featchItem(ItemDetails item){
		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(ItemsServiceURLS.FETCH_ITEM);
		
		Response res = target.request(MediaType.APPLICATION_XML).
				post(Entity.entity(item, MediaType.APPLICATION_XML));
		return res.readEntity(ItemDetails.class);
	}
	
	public static ItemDetails featchItemSerialized(ItemDetails item) throws Exception{
			
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(ItemsServiceURLS.FETCH_ITEM_SERIALIZED);
		
		Response res = target.request(MediaType.APPLICATION_XML).
				post(Entity.entity(item, MediaType.APPLICATION_XML));
		String itemStr = res.readEntity(String.class);
		return ItemDetails.populateFromString(itemStr);
	}
	
	public static List<ItemDetails> featchAllItemDetails(ItemDetails item, Integer offSet, Integer limit){
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(ItemsServiceURLS.FETCH_ALL_ITEMS);
		
		Response res= target.queryParam("offset", offSet).queryParam("limit", limit).request(MediaType.APPLICATION_XML).
				post(Entity.entity(item, MediaType.APPLICATION_XML));
		
		return res.readEntity(new GenericType<List<ItemDetails>>(){});
	}
	
	public static List<ItemDetails> featchAllItemDetailsSerialized(ItemDetails item, Integer offSet, Integer limit) throws Exception{
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(ItemsServiceURLS.FETCH_ALL_ITEMS_SERIALIZED);
		
		Response res= target.queryParam("offset", offSet).queryParam("limit", limit).request(MediaType.APPLICATION_XML).
				post(Entity.entity(item, MediaType.APPLICATION_XML));
		List<String> itemsString = res.readEntity(new GenericType<List<String>>(){});
		List<ItemDetails> itemList = new ArrayList<ItemDetails>();
		for(String itemStr: itemsString){
			itemList.add(ItemDetails.populateFromString(itemStr));
		}
		return itemList;
	}
	
	public static Status updateItemDetails(ItemDetails valueDetails){
		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(ItemsServiceURLS.UPDATE_ITEMS);
		
		Response res = target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(valueDetails, MediaType.APPLICATION_XML));
		if(res.getStatus()==200){
			return Status.SUCCESS;
		}
		return Status.FALIUER;
	}

}
