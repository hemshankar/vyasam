package com.cleanview.items;

import java.io.Serializable;

import com.cleanview.ui.constants.CommonUIConstants;
import com.cleanview.utilities.UIUtil;

public class ItemsServiceURLS implements Serializable{

	private static final long serialVersionUID = 1L;
	public final static String FETCH_ITEM = UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("FETCH_ITEM");
	public final static String FETCH_ITEM_SERIALIZED = UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("FETCH_ITEM_SERIALIZED");
	public final static String ADD_ITEM = UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("ADD_ITEM");
	public final static String ADD_ITEM_SERIALIZED = UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("ADD_ITEM_SERIALIZED");
	public final static String UPDATE_ITEMS = UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("UPDATE_ITEMS");
	public final static String FETCH_ALL_ITEMS = UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("FETCH_ALL_ITEMS");
	public final static String FETCH_ALL_ITEMS_SERIALIZED = UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("FETCH_ALL_ITEMS_SERIALIZED");
}
