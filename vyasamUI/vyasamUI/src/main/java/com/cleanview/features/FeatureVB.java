package com.cleanview.features;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.cleanview.features.FeaturePM.PMFeatureDetails;
import com.cleanview.items.ItemsPM;
import com.cleanview.models.FeatureDetails;
import com.cleanview.persist.common.Status;
import com.cleanview.session.CleanViewSession;
import com.cleanview.ui.constants.SessionKeys;
import com.cleanview.utilities.UIUtil;


@ManagedBean
@RequestScoped
public class FeatureVB implements Serializable{
	
	private static final long serialVersionUID = 1L;
	FeaturePM pm = null;
	CleanViewSession session = null;
	ItemsPM ipm = null;
	public FeatureVB(){
        pm = (FeaturePM) UIUtil.getPageModel("#{featurePM}",FeaturePM.class);    
        ipm = (ItemsPM) UIUtil.getPageModel("#{itemsPM}",ItemsPM.class);
        session = UIUtil.getSession();
    }
	
	public void addFeature(){				
		FeatureDetails newFeature = pm.getNewFeature();
		newFeature.setCategoryId(pm.getSelectedCategory().getCategoryId());
		
		addFeature(newFeature);
	}
	
	public void addFeature(FeatureDetails newFeature){			
		try{
			if(FeatureService.addFeature(newFeature)==Status.SUCCESS){
				
				pm.getFeaturesMap().put(newFeature.getFeatureId(), newFeature);
				pm.getFeaturesList().add(pm.newPMFeatureDetails(newFeature.getFeatureId(), newFeature));
				pm.setNewFeature(new FeatureDetails());
				//ipm.useSelectedCategory();
				System.out.println("===================Added feature successfully");
			}else{
				System.out.println("===================Error while adding the  feature");
			}
			
		} catch(Exception e) {
			System.out.println("===================Error while adding the  feature" + e.getMessage());
		}
	}
	
	public void updateFeature(Integer index){
		try{
			PMFeatureDetails feature = pm.getFeaturesList().get(index);
			if(FeatureService.updateFeatureDetails(feature.getFeatureDetails(),feature.getOriginalName())==Status.SUCCESS){
				
				if(!feature.getFeatureDetails().getFeatureId().equals(feature.getOriginalName())){
					pm.getFeaturesMap().remove(feature.getOriginalName());
				}
				feature.setOriginalName(feature.getFeatureDetails().getFeatureId());
				pm.getFeaturesMap().put(feature.getOriginalName(), feature.getFeatureDetails());
				ipm.useSelectedCategory();
			
				System.out.println("===================Update success");
			}else{
				System.out.println("===================Error while updating the feature");
			}
		} catch(Exception e) {
			System.out.println("===================Error while updating the feature" + e.getMessage());
		}
	}
	
	public void deleteFeature(Integer index){
		System.out.println("Not Implemented");
	}
	
	public void addValue(Integer index){
		PMFeatureDetails feature = pm.getFeaturesList().get(index);
		UIUtil.getSession().put(SessionKeys.SELECTED_FEATURE, feature.getFeatureDetails());
		UIUtil.redirect("valuesedit.xhtml");
	}
}
