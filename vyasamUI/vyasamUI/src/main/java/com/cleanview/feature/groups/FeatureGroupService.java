package com.cleanview.feature.groups;

import java.io.Serializable;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import com.cleanview.models.GroupDetails;
import com.cleanview.persist.common.Status;

public class FeatureGroupService implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public static Status addFeatureGroup(GroupDetails obj1){
			
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(FeatureGroupURLS.ADD_GROUP);
		
		Response res = target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(obj1, MediaType.APPLICATION_XML));
		
		if(res.getStatus()==200){
			return Status.SUCCESS;
		}
		return Status.FALIUER;
	}
	
	public static List<GroupDetails> fetchAllFeatureGroups(Integer cateId){
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(FeatureGroupURLS.FETCH_ALL_GROUPS);
		
		return target.request(MediaType.APPLICATION_XML).
				post(Entity.entity(cateId, MediaType.APPLICATION_XML),new GenericType<List<GroupDetails>>(){});
	}
	
	public static Status updateFeatureGroupDetails(GroupDetails groupDetails,String originalGroupId){
		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(FeatureGroupURLS.UPDATE_GROUPS + originalGroupId);
		
		Response res = target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(groupDetails, MediaType.APPLICATION_XML));
		if(res.getStatus()==200){
			return Status.SUCCESS;
		}
		return Status.FALIUER;
		
	}
}
