package com.cleanview.feature.groups;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.cleanview.models.GroupDetails;
import com.cleanview.ui.abstractImpls.AbstractChangeViewListner;
import com.cleanview.ui.constants.SessionKeys;
import com.cleanview.ui.models.CategoryDetails;
import com.cleanview.utilities.UIUtil;

@ViewScoped
@ManagedBean
public class FeatureGroupPM extends AbstractChangeViewListner implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private List<PMGroupDetails> groupsList = null;
	private Map<String,GroupDetails> groupsMap = null;
	private CategoryDetails selectedCategory = null;
	private GroupDetails selectedGroup = null;
	private GroupDetails newGroup = null;
	public FeatureGroupPM(){
		super();
		newGroup = new GroupDetails();		
		selectedGroup = new GroupDetails();
		useSelectedCategory();		
	}
	
	public void useSelectedCategory(){
		selectedCategory = (CategoryDetails) UIUtil.getSession().getValueAt(SessionKeys.SELECTED_CATEGORY);
		if(selectedCategory == null)
			return;
		groupsMap = selectedCategory.getDetails().getGroupDetails();
		groupsList = mapToList(groupsMap);
	}

	private List<PMGroupDetails> mapToList(Map<String,GroupDetails> map){
		List<PMGroupDetails> list = new ArrayList<PMGroupDetails>();
		
		for(String key: map.keySet()){
			list.add(new PMGroupDetails(key,map.get(key)));
		}
		return list;
	}
	
	public List<PMGroupDetails> getGroupsList() {
		return groupsList;
	}

	public void setGroupsList(List<PMGroupDetails> groupsList) {
		this.groupsList = groupsList;
	}

	public CategoryDetails getSelectedCategory() {
		return selectedCategory;
	}

	public void setSelectedCategory(CategoryDetails selectedCategory) {
		this.selectedCategory = selectedCategory;
	}

	public GroupDetails getSelectedGroup() {
		return selectedGroup;
	}

	public void setSelectedGroup(GroupDetails selectedGroup) {
		this.selectedGroup = selectedGroup;
	}

	public GroupDetails getNewGroup() {
		return newGroup;
	}

	public void setNewGroup(GroupDetails newGroup) {
		this.newGroup = newGroup;
	}

	public Map<String, GroupDetails> getGroupsMap() {
		return groupsMap;
	}

	public void setGroupsMap(Map<String, GroupDetails> groupsMap) {
		this.groupsMap = groupsMap;
	}
	
	public class PMGroupDetails implements Serializable{
		private static final long serialVersionUID = 1L;
		GroupDetails groupDetails = null;
		String originalName = null;
		
		public PMGroupDetails(String origName, GroupDetails gd){
			originalName = origName;
			groupDetails = gd;
		}

		public GroupDetails getGroupDetails() {
			return groupDetails;
		}

		public void setGroupDetails(GroupDetails groupDetails) {
			this.groupDetails = groupDetails;
		}

		public String getOriginalName() {
			return originalName;
		}

		public void setOriginalName(String originalName) {
			this.originalName = originalName;
		}
	}
	
	public PMGroupDetails newPMGroupDetails(String groupId, GroupDetails gd){
		return new PMGroupDetails(groupId, gd);
	}

	@Override
	public void onChange() {
		useSelectedCategory();		
	}
	
	@Override
	public String getId(){
		return FeatureGroupPM.class.getName();
	}
}
