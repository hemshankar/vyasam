package com.cleanview.feature.groups;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.cleanview.feature.groups.FeatureGroupPM.PMGroupDetails;
import com.cleanview.models.GroupDetails;
import com.cleanview.persist.common.Status;
import com.cleanview.session.CleanViewSession;
import com.cleanview.utilities.UIUtil;


@ManagedBean
@RequestScoped
public class FeatureGroupVB implements Serializable{
	
	private static final long serialVersionUID = 1L;
	FeatureGroupPM pm = null;
	CleanViewSession session = null;
	public FeatureGroupVB(){
        pm = (FeatureGroupPM) UIUtil.getPageModel("#{featureGroupPM}",FeatureGroupPM.class);        
        session = UIUtil.getSession();
    }
	
	public void addGroup(){				
		GroupDetails newGroup = pm.getNewGroup();
		newGroup.setCategoryId(pm.getSelectedCategory().getCategoryId());
		
		try{
			if(FeatureGroupService.addFeatureGroup(newGroup)==Status.SUCCESS){
				//pm.getSelectedCategory().getDetails().getGroupDetails().put(newGroup.getGroupId(), newGroup);
				pm.getGroupsMap().put(newGroup.getGroupId(), newGroup);
				pm.getGroupsList().add(pm.newPMGroupDetails(newGroup.getGroupId(), newGroup));
				pm.setNewGroup(new GroupDetails());
				System.out.println("===================Added group successfully");
			}else{
				System.out.println("===================Error while adding the  group");
			}
			
		} catch(Exception e) {
			System.out.println("===================Error while adding the  group" + e.getMessage());
		}
	}
	
	public void updateGroup(Integer index){
		try{
			PMGroupDetails group = pm.getGroupsList().get(index);
			if(FeatureGroupService.updateFeatureGroupDetails(group.getGroupDetails(),group.getOriginalName())==Status.SUCCESS){
				
				if(!group.getGroupDetails().getGroupId().equals(group.getOriginalName())){
					pm.getGroupsMap().remove(group.getOriginalName());
					group.setOriginalName(group.getGroupDetails().getGroupId());
					pm.getGroupsMap().put(group.getOriginalName(), group.getGroupDetails());
					
				}
				System.out.println("===================Update success");
			}else{
				System.out.println("===================Error while updating the group");
			}
		} catch(Exception e) {
			System.out.println("===================Error while updating the group" + e.getMessage());
		}
	}
	
	public void deleteGroup(Integer index){
		System.out.println("Not Implemented");
	}
}
