package com.cleanview.feature.groups;

import java.io.Serializable;

import com.cleanview.ui.constants.CommonUIConstants;
import com.cleanview.utilities.UIUtil;

public class FeatureGroupURLS implements Serializable{
	
	private static final long serialVersionUID = 1L;
	public static final String UPDATE_GROUPS = UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("UPDATE_GROUPS");
	public static final String ADD_GROUP = UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("ADD_GROUP");
	public static final String FETCH_ALL_GROUPS = UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("FETCH_ALL_GROUPS");
	
}
