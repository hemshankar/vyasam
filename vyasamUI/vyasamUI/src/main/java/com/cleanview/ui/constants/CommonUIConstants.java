package com.cleanview.ui.constants;

public class CommonUIConstants {
	
	//========================Connection Related
	public final static String SERVER_HOSTNAME = "SERVER_HOSTNAME";
	public final static String SERVER_PORT = "SERVER_PORT";
	public final static String APP_HOME="APP_HOME";
	public final static String PERSIST_SERVICES_HOME = "PERSIST_SERVICES_HOME";
	public final static String VSERVER_HOSTNAME = "VSERVER_HOSTNAME";
	public final static String VSERVER_PORT = "VSERVER_PORT";
	public final static String VSERVER_HOME = "VSERVER_HOME";
	public final static String SAVE_STEPS = "SAVE_STEPS";
	public final static String FETCH_STEPS = "FETCH_STEPS";
	public final static String UPDATE_STEPS = "UPDATE_STEPS";
	public final static String DELETE_STEPS = "DELETE_STEPS";
	public final static String SAVE_MAPPING = "SAVE_MAPPING";
	public final static String FETCH_MAPPINGS = "FETCH_MAPPINGS";
	public final static String FETCH_ITEM_DETAILS_FOR_A_CLASS = "FETCH_ITEM_DETAILS_FOR_A_CLASS";
	public final static String SAVE_UPDATE_ITEM_SOURCE = "SAVE_UPDATE_ITEM_SOURCE";
}
