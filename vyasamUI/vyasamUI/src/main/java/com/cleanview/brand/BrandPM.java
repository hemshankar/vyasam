package com.cleanview.brand;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.cleanview.models.BrandDetails;
import com.cleanview.ui.abstractImpls.AbstractChangeViewListner;
import com.cleanview.ui.constants.SessionKeys;
import com.cleanview.ui.models.CategoryDetails;
import com.cleanview.utilities.UIUtil;

@ViewScoped
@ManagedBean
public class BrandPM extends AbstractChangeViewListner implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private List<BrandDetails> brandsList = null;
	private CategoryDetails selectedCategory = null;
	private BrandDetails selectedBrand = null;
	private Integer selectedBrandId = 0;
	private BrandDetails newBrand = null;
	public BrandPM(){
		super();
		newBrand = new BrandDetails();		
		selectedBrand = new BrandDetails();
		useSelectedCategory();
	}
	
	public void useSelectedCategory(){
		selectedCategory = (CategoryDetails) UIUtil.getSession().getValueAt(SessionKeys.SELECTED_CATEGORY);
		if(selectedCategory == null)
			return;
		
		brandsList = selectedCategory.getDetails().getBrands();
	}
	
	public List<BrandDetails> getBrandsList() {
		return brandsList;
	}
	public void setBrandsList(List<BrandDetails> brandsList) {
		this.brandsList = brandsList;
	}
	public CategoryDetails getSelectedCategory() {
		return selectedCategory;
	}
	public void setSelectedCategory(CategoryDetails selectedCategory) {
		this.selectedCategory = selectedCategory;
	}
	public BrandDetails getNewBrand() {
		return newBrand;
	}
	public void setNewBrand(BrandDetails newBrand) {
		this.newBrand = newBrand;
	}

	public BrandDetails getSelectedBrand() {
		return selectedBrand;
	}

	public void setSelectedBrand(BrandDetails selectedBrand) {
		this.selectedBrand = selectedBrand;
	}

	public Integer getSelectedBrandId() {
		return selectedBrandId;
	}

	public void setSelectedBrandId(Integer selectedBrandId) {
		this.selectedBrandId = selectedBrandId;
	}

	
	@Override
	public void onChange(){
		useSelectedCategory();
	}
	
	@Override
	public String getId(){
		return BrandPM.class.getName();
	}
}
