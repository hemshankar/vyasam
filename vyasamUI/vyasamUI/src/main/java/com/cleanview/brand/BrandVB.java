package com.cleanview.brand;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.cleanview.models.BrandDetails;
import com.cleanview.persist.common.Status;
import com.cleanview.session.CleanViewSession;
import com.cleanview.utilities.UIUtil;


@ManagedBean
@RequestScoped
public class BrandVB implements Serializable{
	
	private static final long serialVersionUID = 1L;
	BrandPM pm = null;
	CleanViewSession session = null;
	public BrandVB(){
        pm = (BrandPM) UIUtil.getPageModel("#{brandPM}",BrandPM.class);        
        session = UIUtil.getSession();
    }
	
	public void addBrand(){				
		BrandDetails newBrand = pm.getNewBrand();
		newBrand.setCategoryId(pm.getSelectedCategory().getCategoryId());
		
		try{
			newBrand = BrandService.addBrand(newBrand);
			pm.getSelectedCategory().getDetails().getBrands().add(newBrand);
			pm.setNewBrand(new BrandDetails());
		} catch(Exception e) {
			System.out.println("===================Error while adding the brand" + e.getMessage());
		}
	}
	
	public void updateBrand(Integer index){
		try{
			BrandDetails brand = pm.getBrandsList().get(index);
			if(BrandService.updateBrandDetails(brand)==Status.SUCCESS){
				
				System.out.println("===================Brand update Success");
			}else{
				System.out.println("===================Error while updating the brand");
			}
		} catch(Exception e) {
			System.out.println("===================Error while updating the brand" + e.getMessage());
		}
	}
	
	public void deleteBrand(Integer index){
		System.out.println("Not Implemented");
	}
}
