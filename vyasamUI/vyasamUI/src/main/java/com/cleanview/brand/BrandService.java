package com.cleanview.brand;

import java.io.Serializable;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import com.cleanview.models.BrandDetails;
import com.cleanview.persist.common.Status;

public class BrandService implements Serializable{
	
	private static final long serialVersionUID = 1L;

	public static BrandDetails addBrand(BrandDetails obj1){
			
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(BrandServiceURLS.ADD_BRAND);		
		
		Response res= target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(obj1, MediaType.APPLICATION_XML));
		return res.readEntity(BrandDetails.class);
		
	}
	
	public static List<BrandDetails> fetchAllBrands(BrandDetails brand){
		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(BrandServiceURLS.FETCH_ALL_BRANDS);
		
		Response res =  target.request(MediaType.APPLICATION_XML).
				post(Entity.entity(brand, MediaType.APPLICATION_XML));
		return res.readEntity(new GenericType<List<BrandDetails>>(){});
	}
	
	public static Status updateBrandDetails(BrandDetails brandDetails){
		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(BrandServiceURLS.UPDATE_BRAND);
		
		Response res = target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(brandDetails, MediaType.APPLICATION_XML));
		
		if(res.getStatus()==200){
			return Status.SUCCESS;
		}
		return Status.FALIUER;
		
	}
}
