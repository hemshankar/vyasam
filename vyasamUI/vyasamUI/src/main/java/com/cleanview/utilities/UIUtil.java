package com.cleanview.utilities;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import com.cleanview.category.CategoryDetailsService;
import com.cleanview.models.CategoryDetailsForPersistance;
import com.cleanview.models.ItemDetails;
import com.cleanview.session.CleanViewSession;
import com.cleanview.ui.constants.CommonUIConstants;
import com.cleanview.ui.constants.SyncConstants;
import com.cleanview.ui.models.CategoryDetails;
import com.common.exceptions.UtilityException;
import com.common.utilities.Utility;
import com.mysql.jdbc.StringUtils;

public class UIUtil {

	// public static Map<Integer,CategoryDetails> categoryList = null;
	public static CategoryDetails rootCategory = null;
	public static Map<String, String> config = null;
	private static UIUtil util = new UIUtil();
	private static Random rand = new Random();
	private static String[] colors = {"#186A3B","#1D8348","#239B56","#28B463","#2ECC71","#58D68D","#82E0AA","#ABEBC6","#D5F5E3","#CACFD2"};
		//{"0ECD08","4FF04A","63F85E","77F873","9CFC99","AFFAAC","C0FCBE","D1FCD0","E1FDE1","EDFEED","F8FEF8"};
	
	private static PropertyValues propValues = new PropertyValues();
	String result = "";
	InputStream inputStream;
	static {
		// clear previous session
		getSession().clear();
	}

	public static class PropertyValues {
		String result = "";
		InputStream inputStream;
		Properties prop = null;
		
		public void populateProps() throws IOException{
			try{
				String propFileName = "config.properties";
				prop = new Properties(); 
				inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
	 
				if (inputStream != null) {
					prop.load(inputStream);
				} else {
					throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
				}
			}catch(Exception e){
				e.printStackTrace();
			}finally {
				inputStream.close();
			}
		}
		
		public PropertyValues(){			
			if(prop == null){
				try {
					populateProps();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		public String getPropValues(String key) {
 			try {
				result = prop.getProperty(key);
			} catch (Exception e) {
				System.out.println("Exception: " + e);
			} 
			return result;
		}
	}
	
	public static Object getPageModel(String name, Class c) {
		FacesContext context = FacesContext.getCurrentInstance();
		return context.getApplication().evaluateExpressionGet(context, name, c);
	}

	public static CleanViewSession getSession() {
		CleanViewSession session = (CleanViewSession) getPageModel(
				"#{cleanViewSession}", CleanViewSession.class);
		if (session != null) {
			return session;
		} else {
			return new CleanViewSession();
		}
	}

	public static CategoryDetails getRootCategories() {
		return rootCategory;
	}

	public static String getServerHostName() {
		return config.get(CommonUIConstants.SERVER_HOSTNAME);
	}

	public static String getServerPort() {
		return config.get(CommonUIConstants.SERVER_PORT);
	}

	public static void getRootCategory(
			List<CategoryDetailsForPersistance> persistCategoryList) {
		synchronized (SyncConstants.FETCH_ROOT_CATEGORY) {		
			rootCategory = util.getRootCatgory(persistCategoryList);
		}
	}

	public static String getConfig(String key){
		//return config.get(key);
		return propValues.getPropValues(key);
	}
	private CategoryDetails getRootCatgory(
			List<CategoryDetailsForPersistance> persistCategoryList) {
		CategoryDetails rootCategory = new CategoryDetails();
		Map<Integer, CategoryDetails> categoryList = new HashMap<Integer, CategoryDetails>();
		Map<Integer, List<CategoryDetails>> parentChildMap = new HashMap<Integer, List<CategoryDetails>>();

		// create a CategoryDetails for each CategoryDetailsForPersistance
		// object
		// All all the CategoryDetails to a list attached to there parent
		for (CategoryDetailsForPersistance cate : persistCategoryList) {
			CategoryDetails details = new CategoryDetails();
			details.setCategoryId(cate.getCategoryId());
			details.setCategoryName(cate.getCategoryName());
			details.setDetails(cate);
			categoryList.put(details.getCategoryId(), details);

			// if the category is the root element
			if (cate.getCategoryId() == cate.getParentId()) {
				rootCategory = details;
				continue;
			}

			List<CategoryDetails> childList = parentChildMap.get(cate
					.getParentId());
			if (childList == null) {
				childList = new ArrayList<CategoryDetails>();
				parentChildMap.put(cate.getParentId(), childList);
			}
			childList.add(details);
		}

		// use the parentChild map to set children of each category
		for (Integer cateId : categoryList.keySet()) {
			List<CategoryDetails> childList = parentChildMap.get(cateId);
			if (childList != null) {
				for (CategoryDetails child : childList) {
					categoryList.get(cateId).getChildren()
							.put(child.getCategoryId(), child);
				}
			}
		}
		// all the categories whose parent is one are the root categories
		return rootCategory;
	}

	public static CategoryDetails getCategoryFromId(String categoryId) {
		synchronized (SyncConstants.FETCH_CATOGORY) {
			if (!StringUtils.isNullOrEmpty(categoryId)) {
				CategoryDetailsForPersistance cate = new CategoryDetailsForPersistance();
				cate.setCategoryId(Integer.parseInt(categoryId));
				return getCategoryDetailsFromPersistantObject(CategoryDetailsService
						.fetchCategoryById(cate));
			}
			return null;
		}
	}

	public static CategoryDetails getCategoryDetailsFromPersistantObject(
			CategoryDetailsForPersistance cateDetails) {
		synchronized (SyncConstants.CONVERT_TO_CATEGORY_DETAILS) {
			CategoryDetails result = new CategoryDetails();
			result.setCategoryId(cateDetails.getCategoryId());
			result.setCategoryName(cateDetails.getCategoryName());
			result.setDetails(cateDetails);
			return result;
		}
	}

	public static Integer getRandoNumber() {
		return Math.abs(rand.nextInt());
	}

	public static String getRootPath() {
		ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance()
				.getExternalContext().getContext();
		String realPath = ctx.getRealPath("/");
		return realPath;
	}

	public static void redirect(String uri) {
		try {
			
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		    externalContext.redirect(getConfig(CommonUIConstants.APP_HOME) + uri);
			
		} catch (Exception e) {

			e.printStackTrace();
		}
	}
	
	public static List<CategoryDetails> getClassHirarchyforCategory(String id_,CategoryDetails root){
		synchronized (SyncConstants.FETCH_CATEGORY_LIST) {
			
			if(root==null){
				UIUtil.getRootCategory(CategoryDetailsService.fetchAllCategories());			
				root = UIUtil.getRootCategories();
			}
			List<CategoryDetails> lst = new ArrayList<CategoryDetails>();
			if(id_==null){
				lst.add(root);
				
			}else{				
				Integer id = Integer.parseInt(id_);
				util.forListForSelectedCategory(id,root,lst);
				lst = util.invertList(lst);
			}
			return lst;
		}
	}
	
	public static String getColor(Integer percent){
		if(percent > 90)
			return colors[0];
		if(percent > 80)
			return colors[1];
		if(percent > 70)
			return colors[2];
		if(percent > 60)
			return colors[3];
		if(percent > 50)
			return colors[4];
		if(percent > 40)
			return colors[5];
		if(percent > 30)
			return colors[6];
		if(percent > 20)
			return colors[7];
		if(percent > 10)
			return colors[8];
		return colors[9];
		
	}
	
	private List<CategoryDetails> invertList(List<CategoryDetails> list){
		List<CategoryDetails> lst = new ArrayList<CategoryDetails>();
		
		for(int i=list.size()-1;i>=0;i--){
			lst.add(list.get(i));
		}
		
		return lst;
	}
	private boolean forListForSelectedCategory(Integer id,CategoryDetails root,List<CategoryDetails> list){
		if(root==null || id==null || list == null){
			return false;
		}
		
		if(root.getCategoryId()==id){
			list.add(root);
			return true;
		}
		for(CategoryDetails cds: root.getChildren().values()){
			if(forListForSelectedCategory(id,cds,list)==true){
				list.add(root);
				root.setSelectedCategoryId(cds.getCategoryId());
				
				return true;
			}
		}
		return false;
	}

	public static List<String> getImages(ItemDetails iDetails){
		List<String> images = new ArrayList<String>();
		String folderName = iDetails.getPrimaryImages() + "/" + iDetails.getItemId();
		
		
		//String path = UIUtil.getRootPath()+ File.separator + "resources" +  File.separator + folderName;
		String path = UIUtil.getRootPath() + "resources" + File.separator + folderName;
		
		try{
			if(iDetails.getItemId()!=null){
				images.clear();
				File folder = new File(path);
				File[] listOfFiles = folder.listFiles();
				for(File f: listOfFiles){
					if(!f.isDirectory()){
						images.add(f.getName());
					}
				}
			}
		}catch(Exception e){
			//e.printStackTrace();
		}
		return images;
	}
	
	public static String removeWhiteSpaces(String str){    	
    	str =  str.replaceAll("\\W+", "_");
    	return str;
    }
	
	public static Object objFromString(String s) throws Exception{
		byte b[] = s.getBytes(); 
	     ByteArrayInputStream bi = new ByteArrayInputStream(b);
	     ObjectInputStream si = new ObjectInputStream(bi);
	     return si.readObject();		
	}
	
	public static String objToString(Object o) throws Exception {
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
	     ObjectOutputStream so = new ObjectOutputStream(bo);
	     so.writeObject(o);
	     so.flush();
	     return bo.toString();
	}
}
