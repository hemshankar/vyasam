package com.cleanview.utilities;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped

public class UtilBean implements Serializable {


	private static final long serialVersionUID = 1L;

	public String getColor(Integer percent){
		return UIUtil.getColor(percent);
	}
	
}
