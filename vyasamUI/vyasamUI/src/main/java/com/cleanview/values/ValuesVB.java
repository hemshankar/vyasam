package com.cleanview.values;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.cleanview.features.FeaturePM;
import com.cleanview.models.FeatureDetails;
import com.cleanview.models.ValueDetails;
import com.cleanview.persist.common.Status;
import com.cleanview.session.CleanViewSession;
import com.cleanview.utilities.UIUtil;
import com.cleanview.values.ValuesPM.PMValueDetails;


@ManagedBean
@RequestScoped
public class ValuesVB implements Serializable{
	
	private static final long serialVersionUID = 1L;
	FeaturePM fpm = null;
	CleanViewSession session = null;
	ValuesPM pm = null;
	public ValuesVB(){
        fpm = (FeaturePM) UIUtil.getPageModel("#{featurePM}",FeaturePM.class);    
        pm = (ValuesPM) UIUtil.getPageModel("#{valuesPM}",ValuesPM.class);        
        session = UIUtil.getSession();
    }
	
	public void addValue(){				
		ValueDetails newValue = pm.getNewValue();
		newValue.setCategoryId(pm.getSelectedCategory().getCategoryId());
		newValue.setFeatureId(pm.getSelectedFeature().getFeatureId());
		
		try{
			if(ValuesService.addValue(newValue)==Status.SUCCESS){
				
				pm.getValueMap().put(newValue.getValue(), newValue);
				pm.getValueList().add(pm.new PMValueDetails(newValue));
				pm.setNewValue(new ValueDetails());

				System.out.println("===================Added value successfully");
			}else{
				System.out.println("===================Error while adding the  value");
			}
			
		} catch(Exception e) {
			System.out.println("===================Error while adding the  value" + e.getMessage());
		}
	}
	
	public void updateValue(Integer index){
		try{
			PMValueDetails value = pm.getValueList().get(index);
			FeatureDetails fd = pm.getSelectedFeature();
			if(ValuesService.updateValueDetails(value.getvDetails(),value.getOriginalName())==Status.SUCCESS){
				
				if(!value.getvDetails().getValue().equals(value.getOriginalName())){
					fd.getValueDetails().remove(value.getOriginalName());
				}
				value.setOriginalName(value.getvDetails().getValue());
				fd.getValueDetails().put(value.getOriginalName(), value.getvDetails());				
			
				System.out.println("===================Update success");
			}else{
				System.out.println("===================Error while updating the value");
			}
		} catch(Exception e) {
			System.out.println("===================Error while updating the value" + e.getMessage());
		}
	}
	
	public void deleteValue(Integer index){
		System.out.println("Not Implemented");
	}
}
