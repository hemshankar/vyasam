/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.compareo.feature.update;

import com.vyasam.classSpecific.models.DefaultFeature;
import com.compareo.classes.ClassesPM;
import com.comparo.utility.GenericUtilities;
import com.vyasam.category.models.VClass;
import com.vyasam.classSpecific.models.DefaultValue;
import com.vyasam.models.Feature;
import com.vyasam.models.ItemDetails;
import com.vyasam.query.models.DataType;
import com.vyasam.query.models.Query;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import org.compareo.item.browser.BrowseServices;
import org.compareo.item.browser.BrowserSQL;

/**
 *
 * @author hsahu
 */

@ManagedBean (name="featureUpdateVB")
@RequestScoped
public class FeatureUpdateVB implements Serializable {
    
    FeatureUpdatePM pm = null;
    ClassesPM cpm = null;

    public FeatureUpdateVB(){
        pm = (FeatureUpdatePM) GenericUtilities.getPageModel("#{featureUpdatePM}", FeatureUpdatePM.class);
        cpm = (ClassesPM) GenericUtilities.getPageModel("#{classesPM}", ClassesPM.class);
    }

    public void updateList() {
        try {
            VClass selectedClass = cpm.getSelectedClass();
            List<Integer> classIds = new ArrayList<>();
            GenericUtilities.getClassIds(selectedClass, classIds);
            String placeHolders = "";
            Query query = new Query();
            query.setParameters(new ArrayList<String>());
            query.setParametersTypes(new ArrayList<DataType>());
            Map<String,DefaultFeature> featureList =  pm.getFeaturesList();
            for (Integer classId : classIds) {
                query.getParameters().add(classId.toString());
                query.getParametersTypes().add(DataType.INTEGER);
                placeHolders = placeHolders + ",?";
            }

            if (placeHolders.length() > 0) {
                placeHolders = placeHolders.substring(1);
            }

            String extension = " WHERE classId in (" + placeHolders + ")";
            query.setQuery(BrowserSQL.GET_ALL_ITEMS + extension);

            pm.setItemList(BrowseServices.saveItemsSource(query));
            for (ItemDetails item : pm.getItemList()) {
                item.generateFromXML();
                
                for(Feature feature: item.getItemFeatures().getFeaturesList())
                {
                    addToHashmap(pm.getFeaturesList(), feature.getKey(), feature.getValue());
                }
            }
            /*
            Set<String> keySet = new TreeSet<String>();
            keySet.addAll(pm.getFeaturesList().keySet());
            DefaultFeature dFeature = null;
            for(String key: keySet) {
                dFeature = featureList.get(key);
                if(dFeature.getValues().size()<2) {
                    dFeature.setRelevent(false);
                    featureList.remove(key);
                }
            }            
            pm.getFeaturePriorities().clear();
            
            
            for(Integer i = 0;i<pm.getFeaturesList().size();i++)
            {
                pm.getFeaturePriorities().add(i);
            }
            
            VyasamModelsUtility.THRESHOLD = 5;
            VyasamModelsUtility.groupFeaturesWithSimilarValues(pm.getFeaturesList());
            
            for(String key: featureList.keySet())
            {
                Map<String,List<DefaultFeature>> groupedMap = pm.getGroupedFeaturesList();
                String groupName = featureList.get(key).getGroupName();
                List<DefaultFeature> dfList = groupedMap.get(groupName);
                
                if(dfList==null)
                {
                    dfList = new ArrayList<>();                   
                    groupedMap.put(groupName, dfList);
                }
                 dfList.add(featureList.get(key));
            }
            */
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void saveFeatureMapping(){

    	Set<String> keySet = new TreeSet<String>();
        keySet.addAll(pm.getFeaturesList().keySet());
        
    	//Remove non relevant features
        DefaultFeature dFeature = null;
        for(String key: keySet) {
            dFeature = pm.getFeaturesList().get(key);
            System.out.println(dFeature.getName() + " ==> " + dFeature.getTargetFeature());
            if(!dFeature.getRelevent()) {
            	System.out.println(dFeature.getName() + " Is not relevant");
                pm.getFeaturesList().remove(key);                
            }
        } 
    	
    }
    
    private void addToHashmap(Map<String,DefaultFeature> map,String key, String value)
    {
        DefaultFeature feature = map.get(key);
        if(feature==null)
        {
            feature = new DefaultFeature();
            feature.setName(key);
        }
        
        DefaultValue dValue = feature.getValues().get(value);
        if(dValue == null) 
        {
            dValue = new DefaultValue();
            dValue.setName(value);
            feature.getValues().put(value,dValue);
        }
        map.put(feature.getName(), feature); 
    }

    public void mergeFeatures(String f1)
    {
        String f2 = pm.getSelectedF();
        if(f1 == null || f2 == null)
            return;
        
        DefaultFeature feature1 = pm.getFeaturesList().get(f1);
        DefaultFeature feature2 = pm.getFeaturesList().get(f2);
        if(feature1!=null && feature2!=null){
            feature1.getValues().putAll(feature2.getValues());
        } 
        pm.getFeaturesList().remove(f2);
    }
    
    public void addOnlyReleventFeatures()
    {
        Set<String> keySet = new TreeSet<String>();
            keySet.addAll(pm.getFeaturesList().keySet());
            DefaultFeature dFeature = null;
            for(String key: keySet) {
                dFeature = pm.getFeaturesList().get(key);
                if(!dFeature.getRelevent()) {
                    pm.getFeaturesList().remove(key);
                }
            } 
    }
}
