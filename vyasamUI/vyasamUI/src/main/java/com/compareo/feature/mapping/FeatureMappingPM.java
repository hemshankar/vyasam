/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.compareo.feature.mapping;

import com.vyasam.models.ItemDetails;
import com.vyasam.classSpecific.models.DefaultFeature;
import com.vyasam.feature.mapping.MappingDetails;
import com.vyasam.utilities.VyasamModelsUtility;
import com.vyasam.utilities.Group;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * 
 * @author hsahu
 */

@ManagedBean(name = "featureMappingPM")
@ViewScoped
public class FeatureMappingPM implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<ItemDetails> itemList;
	private Map<String, DefaultFeature> featuresList = new HashMap<>();
	private String selectedF;
	private List<MappingDetails> mappingList = null;
	private List<Integer> featurePriorities = new ArrayList<Integer>();
	private MappingDetails mappingDetails = new MappingDetails();
	private Integer selectedMappingId = -1;

	public FeatureMappingPM() {
		try {
			mappingList = FeatureMappingService.fetchMapping();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Map<String, List<DefaultFeature>> groupedFeaturesList = new HashMap<>();

	public Map<String, DefaultFeature> getFeaturesList() {
		return featuresList;
	}

	public void setFeaturesList(Map<String, DefaultFeature> featuresList) {
		this.featuresList = featuresList;
	}

	public List<ItemDetails> getItemList() {
		return itemList;
	}

	public void setItemList(List<ItemDetails> itemList) {
		this.itemList = itemList;
	}

	public String getSelectedF() {
		return selectedF;
	}

	public void setSelectedF(String selecteF) {
		this.selectedF = selecteF;
	}

	public List<Integer> getFeaturePriorities() {
		return featurePriorities;
	}

	public void setFeaturePriorities(List<Integer> featurePriorities) {
		this.featurePriorities = featurePriorities;
	}

	public Map<String, List<DefaultFeature>> getGroupedFeaturesList() {
		return groupedFeaturesList;
	}

	public void setGroupedFeaturesList(
			Map<String, List<DefaultFeature>> groupedFeaturesList) {
		this.groupedFeaturesList = groupedFeaturesList;
	}

	public Map<String, Group> getGroupMap() {
		return VyasamModelsUtility.groupsMap;
	}

	public MappingDetails getMappingDetails() {
		return mappingDetails;
	}

	public void setMappingDetails(MappingDetails mappingDetails) {
		this.mappingDetails = mappingDetails;
	}

	public List<MappingDetails> getMappingList() {
		return mappingList;
	}

	public void setMappingList(List<MappingDetails> mappingList) {
		this.mappingList = mappingList;
	}

	public Integer getSelectedMappingId() {
		return selectedMappingId;
	}

	public void setSelectedMappingId(Integer selectedMappingId) {
		this.selectedMappingId = selectedMappingId;
	}
}
