/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.compareo.feature.mapping;

import java.io.Serializable;

import org.compareo.constants.CommonConstants;

import com.cleanview.ui.constants.CommonUIConstants;
import com.cleanview.utilities.UIUtil;

/**
 *
 * @author hsahu
 */
public class FeatureMappingURIs implements Serializable{
    /*public final static String FETCH_ITEM_DETAILS_FOR_A_CLASS = "http://" 
                                                            + CommonConstants.VSERVER_HOST 
                                                            + ":" + CommonConstants.VSERVER_PORT 
                                                            + "/vServer/persist/ItemDetailsPM/fetch/itemDetails/query";*/
    
    public static final String FETCH_ITEM_DETAILS_FOR_A_CLASS = UIUtil.getConfig(CommonUIConstants.VSERVER_HOME) 
			+ UIUtil.getConfig(CommonUIConstants.FETCH_ITEM_DETAILS_FOR_A_CLASS);
    
    /*public final static String SAVE_MAPPING = "http://" 
            + CommonConstants.VSERVER_HOST 
            + ":" + CommonConstants.VSERVER_PORT 
            + "/vServer/persist/feature_mapping/save/mapping";*/
    public static final String SAVE_MAPPING = UIUtil.getConfig(CommonUIConstants.VSERVER_HOME) 
			+ UIUtil.getConfig(CommonUIConstants.SAVE_MAPPING);
    
    /*public final static String FETCH_MAPPINGS = "http://" 
            + CommonConstants.VSERVER_HOST 
            + ":" + CommonConstants.VSERVER_PORT 
            + "/vServer/persist/feature_mapping/fetch/mapping/HQL";*/
    public static final String FETCH_MAPPINGS = UIUtil.getConfig(CommonUIConstants.VSERVER_HOME) 
			+ UIUtil.getConfig(CommonUIConstants.FETCH_MAPPINGS);
}
