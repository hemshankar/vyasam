/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.compareo.feature.mapping;

import com.vyasam.feature.mapping.MappingDetails;
import com.vyasam.models.ItemDetails;
import com.vyasam.query.models.Query;

import java.io.Serializable;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.compareo.item.browser.BrowserURIs;
import org.glassfish.jersey.client.ClientConfig;

/**
 *
 * @author hsahu
 */
public class FeatureMappingService implements Serializable{
    public static List<ItemDetails> saveItemsSource(Query query) throws Exception
    {        
        String URL2 = BrowserURIs.FETCH_ITEM_DETAILS_FOR_A_CLASS;
        ClientConfig config = new ClientConfig();
        Client client = ClientBuilder.newClient(config);
        WebTarget target = client.target(URL2);
       
        List<ItemDetails> result = target.request(MediaType.APPLICATION_XML).
                                post(Entity.entity(query, MediaType.APPLICATION_XML),new GenericType<List<ItemDetails>>(){});
        return result;
     
    }
    
    public static void saveMappingDetails(MappingDetails map) throws Exception {
    	String URL = FeatureMappingURIs.SAVE_MAPPING;
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);
		
		target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(map, MediaType.APPLICATION_XML),ItemDetails.class);	
    }
    
    public static List<MappingDetails> fetchMapping()
	{
		String URL = FeatureMappingURIs.FETCH_MAPPINGS;
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);
		
		List<MappingDetails> list = target.queryParam("HQL", "from MappingDetails").request(MediaType.APPLICATION_XML).get(new GenericType<List<MappingDetails>>(){});
		
		return list;	
	}
}
