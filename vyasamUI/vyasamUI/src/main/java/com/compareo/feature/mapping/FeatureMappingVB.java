/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.compareo.feature.mapping;

import com.vyasam.classSpecific.models.DefaultFeature;
import com.cleanview.brand.BrandPM;
import com.cleanview.category.CategoryDetailsPM;
import com.cleanview.category.CategoryDetailsVB;
import com.cleanview.features.FeaturePM;
import com.cleanview.features.FeatureVB;
import com.cleanview.items.ItemsService;
import com.cleanview.models.FeatureDetails;
import com.cleanview.utilities.UIUtil;
import com.compareo.classes.ClassesPM;
import com.comparo.utility.GenericUtilities;
import com.vyasam.category.models.VClass;
import com.vyasam.classSpecific.models.DefaultValue;
import com.vyasam.feature.mapping.MappingDetails;
import com.vyasam.models.Feature;
import com.vyasam.models.ItemDetails;
import com.vyasam.query.models.DataType;
import com.vyasam.query.models.Query;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.compareo.item.browser.BrowseServices;
import org.compareo.item.browser.BrowserSQL;
import org.glassfish.jersey.client.ClientConfig;

/**
 * 
 * @author hsahu
 */

@ManagedBean(name = "featureMappingVB")
@RequestScoped
public class FeatureMappingVB implements Serializable {

	FeatureMappingPM pm = null;
	ClassesPM cpm = null;
	CategoryDetailsPM catePM = null;
	FeaturePM fpm = null;
	BrandPM bpm = null;

	public FeatureMappingVB() {
		pm = (FeatureMappingPM) GenericUtilities.getPageModel(
				"#{featureMappingPM}", FeatureMappingPM.class);
		cpm = (ClassesPM) GenericUtilities.getPageModel("#{classesPM}",
				ClassesPM.class);
		catePM = (CategoryDetailsPM) UIUtil.getPageModel(
				"#{categoryDetailsPM}", CategoryDetailsPM.class);
		fpm = (FeaturePM) UIUtil.getPageModel("#{featurePM}", FeaturePM.class);
		bpm = (BrandPM) UIUtil.getPageModel("#{brandPM}", BrandPM.class);
	}

	public void updateList() {
		try {
			VClass selectedClass = cpm.getSelectedClass();
			List<Integer> classIds = new ArrayList<>();
			GenericUtilities.getClassIds(selectedClass, classIds);

			String placeHolders = "";
			Query query = new Query();
			query.setParameters(new ArrayList<String>());
			query.setParametersTypes(new ArrayList<DataType>());

			Map<String, DefaultFeature> featureList = pm.getFeaturesList();

			for (Integer classId : classIds) {
				query.getParameters().add(classId.toString());
				query.getParametersTypes().add(DataType.INTEGER);
				placeHolders = placeHolders + ",?";
			}

			if (placeHolders.length() > 0) {
				placeHolders = placeHolders.substring(1);
			}

			String extension = " WHERE classId in (" + placeHolders + ")";
			query.setQuery(BrowserSQL.GET_ALL_ITEMS + extension);

			pm.setItemList(BrowseServices.saveItemsSource(query));
			for (ItemDetails item : pm.getItemList()) {
				item.generateFromXML();

				for (Feature feature : item.getItemFeatures().getFeaturesList()) {
					addToHashmap(pm.getFeaturesList(), feature.getKey(),
							feature.getValue());
				}
			}
			/*
			 * Set<String> keySet = new TreeSet<String>();
			 * keySet.addAll(pm.getFeaturesList().keySet()); DefaultFeature
			 * dFeature = null; for(String key: keySet) { dFeature =
			 * featureList.get(key); if(dFeature.getValues().size()<2) {
			 * dFeature.setRelevent(false); featureList.remove(key); } }
			 * pm.getFeaturePriorities().clear();
			 * 
			 * 
			 * for(Integer i = 0;i<pm.getFeaturesList().size();i++) {
			 * pm.getFeaturePriorities().add(i); }
			 * 
			 * VyasamModelsUtility.THRESHOLD = 5;
			 * VyasamModelsUtility.groupFeaturesWithSimilarValues
			 * (pm.getFeaturesList());
			 * 
			 * for(String key: featureList.keySet()) {
			 * Map<String,List<DefaultFeature>> groupedMap =
			 * pm.getGroupedFeaturesList(); String groupName =
			 * featureList.get(key).getGroupName(); List<DefaultFeature> dfList
			 * = groupedMap.get(groupName);
			 * 
			 * if(dfList==null) { dfList = new ArrayList<>();
			 * groupedMap.put(groupName, dfList); }
			 * dfList.add(featureList.get(key)); }
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addToHashmap(Map<String, DefaultFeature> map, String key,
			String value) {
		DefaultFeature feature = map.get(key);
		if (feature == null) {
			feature = new DefaultFeature();
			feature.setName(key);
		}

		DefaultValue dValue = feature.getValues().get(value);
		if (dValue == null) {
			dValue = new DefaultValue();
			dValue.setName(dValue.getName() + "," + value);
			feature.getValues().put(value, dValue);
		}
		map.put(feature.getName(), feature);
	}

	public void mergeFeatures(String f1) {
		String f2 = pm.getSelectedF();
		if (f1 == null || f2 == null)
			return;

		DefaultFeature feature1 = pm.getFeaturesList().get(f1);
		DefaultFeature feature2 = pm.getFeaturesList().get(f2);
		if (feature1 != null && feature2 != null) {
			feature1.getValues().putAll(feature2.getValues());
		}
		pm.getFeaturesList().remove(f2);
	}

	public void addOnlyReleventFeatures() {
		Set<String> keySet = new TreeSet<String>();
		keySet.addAll(pm.getFeaturesList().keySet());
		DefaultFeature dFeature = null;
		for (String key : keySet) {
			dFeature = pm.getFeaturesList().get(key);
			if (!dFeature.getRelevent()) {
				pm.getFeaturesList().remove(key);
			}
		}
	}

	public void selectMapping() {
		Integer selectedMapingId = pm.getSelectedMappingId();

		if (selectedMapingId == -1)
			return;

		MappingDetails selectedMapping = null;

		for (MappingDetails details : pm.getMappingList()) {
			if (details.getMappingId().equals(selectedMapingId)) {
				selectedMapping = details;
				pm.setMappingDetails(selectedMapping);
				break;
			}
		}

		if (selectedMapping.getCategoryId() != null) {
			catePM.updateFromCategoryId(selectedMapping.getCategoryId());
		}
		Map<String, DefaultFeature> featuresList = pm.getFeaturesList();
		
		//turn all the columns invalied
		for(String key: featuresList.keySet()){
			featuresList.get(key).setRelevent(false);
		}
		
		DefaultFeature df = null;
		Map<String, String> mapping = selectedMapping.getColumnMapping();
		for (String key : mapping.keySet()) {
			df = featuresList.get(key);
			if (df != null) {
				df.setTargetFeature(mapping.get(key));
				df.setRelevent(true);
			}
		}				
	}

	public void saveFeatureMapping() {

		Set<String> keySet = new TreeSet<String>();
		keySet.addAll(pm.getFeaturesList().keySet());

		MappingDetails map = pm.getMappingDetails();
		if (catePM.getSelectedCategory() != null) {
			map.setCategoryId(catePM.getSelectedCategory().getCategoryId() + "");
		}

		// Remove non relevant features
		DefaultFeature dFeature = null;
		System.out.println("Following feature mapping is done");
		for (String key : keySet) {
			dFeature = pm.getFeaturesList().get(key);
			if (!dFeature.getRelevent()) {
				// System.out.println(dFeature.getName() + " Is not relevant");
				pm.getFeaturesList().remove(key);
			} else {
				if (dFeature.getTargetFeature().equals("New_Feature")) {
					dFeature.setTargetFeature("f_" + UIUtil.removeWhiteSpaces(dFeature.getName()));
				}
				System.out.println(dFeature.getName() + " ==> "
						+ dFeature.getTargetFeature());
				map.getColumnMapping().put(dFeature.getName(),
						dFeature.getTargetFeature());
			}
		}
		try {
			FeatureMappingService.saveMappingDetails(map);
			pm.setMappingList(FeatureMappingService.fetchMapping());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addFeaturesToCleanView() {
		Map<String, FeatureDetails> features = fpm.getFeaturesMap();
		MappingDetails mapping = pm.getMappingDetails();
		Map<String, String> mappedColumns = mapping.getColumnMapping();
		FeatureVB featuresVB = new FeatureVB();
		for (String featureName : mappedColumns.values()) {
			if (features.get(featureName) == null) {
				FeatureDetails fd = new FeatureDetails();
				fd.setCategoryId(fpm.getSelectedCategory().getCategoryId());
				fd.setFeatureId(featureName);
				fd.setFeatureName(featureName.substring(2));
				fd.setMaxValue(100);
				fd.setMinValue(1);
				featuresVB.addFeature(fd);
			}
		}
	}

	public void selectAll() {
		Set<String> keySet = new TreeSet<String>();
		keySet.addAll(pm.getFeaturesList().keySet());

		DefaultFeature dFeature = null;
		for (String key : keySet) {
			dFeature = pm.getFeaturesList().get(key);
			dFeature.setRelevent(true);
		}
	}

	public void unSelectAll() {
		Set<String> keySet = new TreeSet<String>();
		keySet.addAll(pm.getFeaturesList().keySet());

		DefaultFeature dFeature = null;
		for (String key : keySet) {
			dFeature = pm.getFeaturesList().get(key);
			dFeature.setRelevent(false);
		}
	}

	public void pushItems() {
		if(bpm.getSelectedBrandId()==-2){
			//this is error... since the brand details are not provided and insert will fail
			return;
		}
		List<com.cleanview.models.ItemDetails> itemList = new ArrayList<com.cleanview.models.ItemDetails>();
		com.cleanview.models.ItemDetails newItem = null;
		MappingDetails mapping = pm.getMappingDetails();
		String folderName = "images" + catePM.getClassHirarchy();
		int count = 0;
		for (ItemDetails item : pm.getItemList()) {
			// expected is that the generateFromXML is called
			newItem = new com.cleanview.models.ItemDetails();
			newItem.setCategoryId(catePM.getSelectedCategory().getCategoryId());
			newItem.setBrandId(bpm.getSelectedBrandId());
			newItem.setPrimaryImages(folderName);
			newItem.setModelName(item.getTitle());
			for (Feature feature : item.getItemFeatures().getFeaturesList()) {
				String targetFeature = mapping.getColumnMapping().get(
						feature.getKey());
				if (targetFeature != null)
					newItem.getFeatures()
							.put(targetFeature, 
									GenericUtilities.trimTo400Chars(
											GenericUtilities.removeWhiteSpaces(feature.getValue())));
			}
			try {
				if(newItem.getFeatures().size()==0)
					continue;
				System.out.println("\n\nAdding=== " + "\n" + newItem);
				ItemsService.addItemDetailsSerialized(newItem);
				count++;
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		System.out.println("\n\nAdded " + count + " items");
	}
}
