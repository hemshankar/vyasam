/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.compareo.feature.update;

import com.vyasam.models.ItemDetails;
import com.vyasam.query.models.Query;
import java.io.Serializable;
import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import org.compareo.item.browser.BrowserURIs;
import org.glassfish.jersey.client.ClientConfig;

/**
 *
 * @author hsahu
 */
public class FeatureUpdateService implements Serializable{
    public static List<ItemDetails> saveItemsSource(Query query) throws Exception
    {        
        String URL2 = BrowserURIs.FETCH_ITEM_DETAILS_FOR_A_CLASS;
        ClientConfig config = new ClientConfig();
        Client client = ClientBuilder.newClient(config);
        WebTarget target = client.target(URL2);
       
        List<ItemDetails> result = target.request(MediaType.APPLICATION_XML).
                                post(Entity.entity(query, MediaType.APPLICATION_XML),new GenericType<List<ItemDetails>>(){});
        return result;
     
    }
}
