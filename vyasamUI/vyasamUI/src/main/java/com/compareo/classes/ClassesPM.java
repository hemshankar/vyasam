/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.compareo.classes;

import com.comparo.utility.ClassDetails;
import com.comparo.utility.GenericUtilities;
import com.vyasam.category.models.VClass;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.compareo.constants.CommonConstants;

/**
 *
 * @author hsahu
 */
@ManagedBean(name = "classesPM")
@ViewScoped
public class ClassesPM implements Serializable {

    private List<ClassDetails> classDetailsList =null;
    private VClass selectedClass = null;
    private String newClass = "";
    
    public ClassesPM() {
        try {
            classDetailsList = (List<ClassDetails>) GenericUtilities.getFromSession(CommonConstants.ITEMS_CLASS_LIST);
            selectedClass = (VClass) GenericUtilities.getFromSession(CommonConstants.ITEMS_SELECTED_CLASS);
            if (classDetailsList == null) {
                classDetailsList = new ArrayList();
                ClassDetails classDetails = new ClassDetails();
                classDetails.setAvailableClasses(GenericUtilities.getTopLevelClasses());
                classDetailsList.add(classDetails);
            } 
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<ClassDetails> getClassDetailsList() {
        return classDetailsList;
    }

    public void setClassDetailsList(List<ClassDetails> classDetailsList) {
        this.classDetailsList = classDetailsList;
    }

    public VClass getSelectedClass() {
        return selectedClass;
    }

    public void setSelectedClass(VClass selectedClass) {
        this.selectedClass = selectedClass;
    }
    
    public String getNewClass() {
        return newClass;
    }

    public void setNewClass(String newClass) {
        this.newClass = newClass;
    }
    
    public String getClassHirarchy()
    {
        if(classDetailsList==null || classDetailsList.size()<1)
            return "";
        String str="";
        
        for(ClassDetails iDetails:classDetailsList)
        {
            if(iDetails.getSelectedClass()!=null && iDetails.getSelectedClass().getName()!=null)
                str = str + iDetails.getSelectedClass().getName() + ">";
        }
        str = str.substring(0, str.length()-1);
        return str;
    }
}
