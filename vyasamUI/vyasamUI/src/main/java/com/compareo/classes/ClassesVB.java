/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.compareo.classes;

import com.comparo.utility.ClassDetails;
import com.comparo.utility.GenericUtilities;
import com.vyasam.category.models.VClass;
import com.vyasam.jsoup.models.exceptions.VyasamModelsException;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import org.compareo.constants.CommonConstants;

/**
 *
 * @author hsahu
 */
@ManagedBean(name = "classesVB")
@RequestScoped
public class ClassesVB implements Serializable{
    
    ClassesPM pm = null;
    
    public ClassesVB()
    {
        pm = (ClassesPM) GenericUtilities.getPageModel("#{classesPM}",ClassesPM.class);
    }
    
    public void selectSubClass(Integer index) {
        List<ClassDetails> classList = pm.getClassDetailsList();
        Integer selectedClassId = classList.get(index).getSelectedId();
        classList.get(index).setSelectedClass(null);
        if(selectedClassId==-2)
            return;
        
        int size = classList.size();

        for (int i = index + 1; i < size; i++) {
            classList.remove(classList.size() - 1);
        }

        List<VClass> availableClasses = classList.get(index).getAvailableClasses();

        for (VClass vClass : availableClasses) {
            if (vClass.getId().equals(selectedClassId)) {
                classList.get(index).setSelectedClass(vClass);
                pm.setSelectedClass(vClass);
                break;
            }
        }

        ClassDetails nextClassDetails = new ClassDetails();
        if(nextClassDetails!=null && classList!=null)
        nextClassDetails.setAvailableClasses(classList.get(index).getSelectedClass().getChildren());
        classList.add(nextClassDetails);
    }
    
    public void addNewClass() {
        try {
            List<ClassDetails> classList = pm.getClassDetailsList();
            String newClassName = pm.getNewClass();
            
            if (classList.size() == 1) {
                pm.setSelectedClass(VClass.addClass(null, newClassName));
                classList.get(0).getAvailableClasses().add(pm.getSelectedClass());                
            }
            else
            {
                ClassDetails details = classList.get(classList.size() - 2);
                if(details.getSelectedClass()!=null){
                    pm.setSelectedClass(VClass.addClass(details.getSelectedClass(), newClassName));
                }
                else
                {
                    details = classList.get(classList.size() - 3);
                    pm.setSelectedClass(VClass.addClass(details.getSelectedClass(), newClassName));
                }
            }            
        } catch (VyasamModelsException ex) {
            ex.printStackTrace();
        }
    }
    
    public void saveToSession() {    	
        GenericUtilities.putToSession(CommonConstants.ITEMS_CLASS_LIST, pm.getClassDetailsList());
        GenericUtilities.putToSession(CommonConstants.ITEMS_SELECTED_CLASS, pm.getSelectedClass());
        GenericUtilities.redirect("selectSource.xhtml");
    }
}
