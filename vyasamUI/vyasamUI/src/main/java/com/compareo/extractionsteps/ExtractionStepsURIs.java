/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.compareo.extractionsteps;

import java.io.Serializable;

import org.compareo.constants.CommonConstants;

import com.cleanview.ui.constants.CommonUIConstants;
import com.cleanview.utilities.UIUtil;

/**
 *
 * @author hsahu
 */
public class ExtractionStepsURIs implements Serializable{
	
	public static final String SAVE_STEPS = UIUtil.getConfig(CommonUIConstants.VSERVER_HOME) 
												+ UIUtil.getConfig(CommonUIConstants.SAVE_STEPS);
	
    /*public final static String SAVE_STEPS = "http://" 
            + CommonConstants.VSERVER_HOST 
            + ":" + CommonConstants.VSERVER_PORT 
            + "/vServer/persist/extrationStepsPM/save/steps";*/
    
	public static final String FETCH_STEPS = UIUtil.getConfig(CommonUIConstants.VSERVER_HOME) 
												+ UIUtil.getConfig(CommonUIConstants.FETCH_STEPS);
	
    /*public final static String FETCH_STEPS = "http://" 
            + CommonConstants.VSERVER_HOST 
            + ":" + CommonConstants.VSERVER_PORT 
            + "/vServer/persist/extrationStepsPM/fetch/steps/HQL";*/
    
	public static final String UPDATE_STEPS = UIUtil.getConfig(CommonUIConstants.VSERVER_HOME) 
												+ UIUtil.getConfig(CommonUIConstants.UPDATE_STEPS);
	
    /*public final static String UPDATE_STEPS = "http://" 
            + CommonConstants.VSERVER_HOST 
            + ":" + CommonConstants.VSERVER_PORT 
            + "/vServer/persist/extrationStepsPM/save_update/steps";*/

	public static final String DELETE_STEPS = UIUtil.getConfig(CommonUIConstants.VSERVER_HOME) 
												+ UIUtil.getConfig(CommonUIConstants.DELETE_STEPS);
	
    /*public final static String DELETE_STEPS = "http://" 
            + CommonConstants.VSERVER_HOST 
            + ":" + CommonConstants.VSERVER_PORT 
            + "/vServer/persist/extrationStepsPM/delete/steps";*/
    
}
