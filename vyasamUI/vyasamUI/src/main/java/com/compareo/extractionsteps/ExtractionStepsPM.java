package com.compareo.extractionsteps;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.vyasam.models.ExtractionSteps;
@ManagedBean (name="extractionStepsPM")
@ViewScoped
public class ExtractionStepsPM implements Serializable{

	private static final long serialVersionUID = 1L;
	List<ExtractionSteps> stepsList = null;
	ExtractionSteps selecetedStep = null;
	String selectedStepId = "";
	
	public ExtractionStepsPM(){
		stepsList = ExtractionStepsService.fetchSteps();
		selecetedStep = new ExtractionSteps();
	}
	
	
	public List<ExtractionSteps> getStepsList() {
		return stepsList;
	}
	public void setStepsList(List<ExtractionSteps> stepsList) {
		this.stepsList = stepsList;
	}
	public ExtractionSteps getSelecetedStep() {
		return selecetedStep;
	}
	public void setSelecetedStep(ExtractionSteps selecetedStep) {
		this.selecetedStep = selecetedStep;
	}
	public String getSelectedStepId() {
		return selectedStepId;
	}
	public void setSelectedStepId(String selectedStepId) {
		this.selectedStepId = selectedStepId;
	}
	
}
