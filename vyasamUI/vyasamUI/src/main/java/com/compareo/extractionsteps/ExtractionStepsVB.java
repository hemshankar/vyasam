/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.compareo.extractionsteps;

import com.mysql.jdbc.StringUtils;
import com.vyasam.classSpecific.models.DefaultFeature;
import com.cleanview.brand.BrandPM;
import com.cleanview.category.CategoryDetailsPM;
import com.cleanview.category.CategoryDetailsVB;
import com.cleanview.features.FeaturePM;
import com.cleanview.features.FeatureVB;
import com.cleanview.items.ItemsService;
import com.cleanview.models.FeatureDetails;
import com.cleanview.utilities.UIUtil;
import com.compareo.classes.ClassesPM;
import com.comparo.utility.GenericUtilities;
import com.vyasam.category.models.VClass;
import com.vyasam.classSpecific.models.DefaultValue;
import com.vyasam.constants.FetchStatus;
import com.vyasam.feature.mapping.MappingDetails;
import com.vyasam.jsoup.models.DetailsExtractionSteps;
import com.vyasam.jsoup.models.ListExtractionSteps;
import com.vyasam.models.ExtractionSteps;
import com.vyasam.models.Feature;
import com.vyasam.models.ItemDetails;
import com.vyasam.models.ItemsSourceDetails;
import com.vyasam.query.models.DataType;
import com.vyasam.query.models.Query;
import com.vyasam.utilities.VyasamModelsUtility;
import com.vyasam.utility.logger.Logger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.compareo.constants.CommonConstants;
import org.compareo.item.browser.BrowseServices;
import org.compareo.item.browser.BrowserSQL;
import org.compareo.item.extractor.details.DetailsExtractorPM;
import org.compareo.item.extractor.details.DetailsExtractorService;
import org.compareo.item.extractor.itemlist.ExtractorPageModel;
import org.glassfish.jersey.client.ClientConfig;
import org.jsoup.helper.StringUtil;

/**
 * 
 * @author hsahu
 */

@ManagedBean
@RequestScoped
public class ExtractionStepsVB implements Serializable {

	private static final long serialVersionUID = 1L;
	ExtractionStepsPM pm = null;	
	ExtractorPageModel epm = null;
	DetailsExtractorPM depm = null;
	public ExtractionStepsVB() {
		pm = (ExtractionStepsPM) GenericUtilities.getPageModel(
				"#{extractionStepsPM}", ExtractionStepsPM.class);
		epm = (ExtractorPageModel) GenericUtilities.getPageModel(
				"#{extractor}", ExtractorPageModel.class);
		depm = (DetailsExtractorPM) GenericUtilities.getPageModel("#{detailsExtractorPM}",DetailsExtractorPM.class);
		
	}

	public void saveSptes() {
		try {
		    ItemsSourceDetails source = depm.getSourceDetails();          
            ExtractionSteps selectedSteps = pm.getSelecetedStep();
            source.generateXMLs();
            selectedSteps.setListExtrectionStepsXML(source.getExtractionStepsXML());
            for(ItemDetails item: source.getItemDetailsList())
            {
                DetailsExtractionSteps detailsES = new DetailsExtractionSteps();
                
                detailsES.setRootProps(depm.getRootElemProps());
                detailsES.setKeyProps(depm.getAttrKeyElemProps());
                detailsES.setValueProps(depm.getAttrValueElemProps());
               
                item.generateXMLs();   	                
                selectedSteps.setDetailsExtrectionStepsXML(item.getExtractionStepsXML());
                
                break;
            }
			selectedSteps.setStepsId(null);
			ExtractionStepsService.saveSteps(selectedSteps);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void saveUpdateSptes() {
		try {
		    ItemsSourceDetails source = depm.getSourceDetails();          
            ExtractionSteps selectedSteps = pm.getSelecetedStep();
            source.generateXMLs();
            selectedSteps.setListExtrectionStepsXML(source.getExtractionStepsXML());
            for(ItemDetails item: source.getItemDetailsList())
            {
                DetailsExtractionSteps detailsES = new DetailsExtractionSteps();
                
                detailsES.setRootProps(depm.getRootElemProps());
                detailsES.setKeyProps(depm.getAttrKeyElemProps());
                detailsES.setValueProps(depm.getAttrValueElemProps());
               
                item.generateXMLs();   	                
                selectedSteps.setDetailsExtrectionStepsXML(item.getExtractionStepsXML());
                
                break;
            }
			selectedSteps.setStepsId(null);
			ExtractionStepsService.saveSteps(selectedSteps);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void deleteSteps() {
		try {
			ExtractionSteps steps = pm.getSelecetedStep();
			ExtractionStepsService.deleteSteps(steps);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateSteps() {
		try {
			for (ExtractionSteps steps : pm.getStepsList()) {
				if(StringUtils.isNullOrEmpty(pm.getSelectedStepId()))
					return;
				
				if (steps.getStepsId() == Integer.parseInt(pm.getSelectedStepId())) {
					pm.setSelecetedStep(steps);
					ListExtractionSteps lstSptes = VyasamModelsUtility.serializationUtility
																.XMLToObjetc(steps.getListExtrectionStepsXML(),
																				ListExtractionSteps.class);
					/*DetailsExtractionSteps detailsSteps = VyasamModelsUtility.serializationUtility
																.XMLToObjetc(steps.getDetailsExtrectionStepsXML(),
																				DetailsExtractionSteps.class);
					*/
					epm.setRootElementProperties(lstSptes.getRootProps());
					epm.setItemsElementProperties(lstSptes.getLinkProps());
									
					break;
				}
				
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
