/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.compareo.extractionsteps;

import com.vyasam.feature.mapping.MappingDetails;
import com.vyasam.models.ExtractionSteps;
import com.vyasam.models.ItemDetails;
import com.vyasam.query.models.Query;

import java.io.Serializable;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.compareo.item.browser.BrowserURIs;
import org.glassfish.jersey.client.ClientConfig;

/**
 *
 * @author hsahu
 */
public class ExtractionStepsService implements Serializable{
    
    
    public static void saveSteps(ExtractionSteps steps) throws Exception {
    	String URL = ExtractionStepsURIs.SAVE_STEPS;
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);
		
		target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(steps, MediaType.APPLICATION_XML),ExtractionSteps.class);	
    }
    
    public static void saveUpdateSteps(ExtractionSteps steps) throws Exception {
    	String URL = ExtractionStepsURIs.UPDATE_STEPS;
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);
		
		target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(steps, MediaType.APPLICATION_XML),ExtractionSteps.class);	
    }
    
    public static List<ExtractionSteps> fetchSteps()
	{
		String URL = ExtractionStepsURIs.FETCH_STEPS;
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);
		
		List<ExtractionSteps> list = target.queryParam("HQL", "from ExtractionSteps").request(MediaType.APPLICATION_XML).get(new GenericType<List<ExtractionSteps>>(){});
		
		return list;	
	}
    
    public static void deleteSteps(ExtractionSteps steps)
	{
		String URL = ExtractionStepsURIs.DELETE_STEPS;
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL);
		
		target.request(MediaType.APPLICATION_XML).
			post(Entity.entity(steps, MediaType.APPLICATION_XML),ExtractionSteps.class);
		
	}
    
    
}
