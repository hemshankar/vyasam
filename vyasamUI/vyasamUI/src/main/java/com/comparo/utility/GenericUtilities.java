/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comparo.utility;

import com.comparo.session.CompareOSession;
import com.vyasam.category.models.VClass;
import com.vyasam.utility.logger.Logger;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;

/**
 * 
 * @author hsahu
 */
public class GenericUtilities {

	public static VClass vClass = new VClass();

	static {
		try {
			vClass.updateClasses();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static Object getPageModel(String name, Class c) {
		FacesContext context = FacesContext.getCurrentInstance();
		return context.getApplication().evaluateExpressionGet(context, name, c);
	}

	public static CompareOSession getJexSession() {
		CompareOSession session = (CompareOSession) getPageModel(
				"#{compSession}", CompareOSession.class);
		if (session != null) {
			return session;
		} else {
			return new CompareOSession();
		}
	}

	public static void putToSession(String key, Object value) {
		getJexSession().put(key, value);
	}

	public static Object getFromSession(String key) {
		return getJexSession().getValueAt(key);
	}

	public static void redirect(String url) {
		try {
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect(url);
		} catch (IOException ex) {
			Logger.log("error",
					"Ill formed URL at Utility.redirect() : " + ex.toString());
		}
	}

	public static List<VClass> getTopLevelClasses() {
		List<VClass> classList = new ArrayList<>();
		classList.addAll(vClass.getListOfRootClasses().values());
		return classList;
	}

	public static void getClassIds(VClass selectedClass, List<Integer> classIds) {
		classIds.add(selectedClass.getId());
		for (VClass child : selectedClass.getChildren()) {
			getClassIds(child, classIds);
		}
	}

	public static String removeWhiteSpaces(String str) {
		str = str.replaceAll("\\W+", "_");
		return str;
	}

	public static Object objFromString(String s) throws Exception{
		byte b[] = s.getBytes(); 
	     ByteArrayInputStream bi = new ByteArrayInputStream(b);
	     ObjectInputStream si = new ObjectInputStream(bi);
	     return si.readObject();		
	}
	
	public static String objToString(Object o) throws Exception {
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
	     ObjectOutputStream so = new ObjectOutputStream(bo);
	     so.writeObject(o);
	     so.flush();
	     return bo.toString();
	}
	
	public static String trimTo400Chars(String str){
		if(str.length()>399)
			str = str.substring(0, 399);
		return str;
	}
}