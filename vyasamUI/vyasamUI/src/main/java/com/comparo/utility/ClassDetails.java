/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comparo.utility;

import com.vyasam.category.models.VClass;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author hsahu
 */
public class ClassDetails implements Serializable{
    private Integer selectedId=0;

    public Integer getSelectedId() {
        return selectedId;
    }

    public void setSelectedId(Integer selectedId) {
        this.selectedId = selectedId;
    }
    private VClass selectedClass = new VClass();
    private List<VClass> availableClasses;

    public VClass getSelectedClass() {
        return selectedClass;
    }

    public void setSelectedClass(VClass selectedClass) {
        this.selectedClass = selectedClass;
    }

    public List<VClass> getAvailableClasses() {
        return availableClasses;
    }

    public void setAvailableClasses(List<VClass> availableClasses) {
        this.availableClasses = availableClasses;
    }
}
