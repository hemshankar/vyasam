/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.comparo.session;

import java.io.Serializable;
import java.util.HashMap;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author hsahu
 */
@ManagedBean(name="compSession")
@SessionScoped
public class CompareOSession implements Serializable{
     private HashMap m_values;

    public CompareOSession()
    {
        m_values = new HashMap();        
    }
    
    public void put(String key, Object value)
    {
        m_values.put(key, value);
    }
    
    public HashMap getValues() {
        return m_values;
    }
    
    public Object getValueAt(String key) 
    {
        return m_values.get(key);
    }
}
