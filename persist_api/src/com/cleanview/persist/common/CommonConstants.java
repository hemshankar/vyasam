package com.cleanview.persist.common;

public class CommonConstants {
	public static final String MYSQL_JDBC_URL = "MYSQL_JDBC_URL";
	public static final String SCHEMA_NAME	=	"SCHEMA_NAME";
	public static final String CATEGORIES	=	"CATEGORIES";
	public static final String BRANDS	=	"BRANDS";
	public static final String DB_HOSTNAME = "DB_HOSTNAME";
	public static final String USER = "USER";
	public static final String PASSWORD = "PASSWORD";
	public static final String DRIVER_CLASS = "DRIVER_CLASS";
	public static final String FEATURE_DETAILS="FEATURE_DETAILS";
	public static final String FEATURE_GROUPS="FEATURE_GROUPS";
	public static final String VALUE_DETAILS="VALUE_DETAILS";
	
}
