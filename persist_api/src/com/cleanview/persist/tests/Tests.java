package com.cleanview.persist.tests;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.cleanview.models.BrandDetails;
import com.cleanview.models.CategoryDetailsForPersistance;
import com.cleanview.models.FeatureDetails;
import com.cleanview.models.GroupDetails;
import com.cleanview.models.ItemDetails;
import com.cleanview.models.ValueDetails;
import com.cleanview.persist.MySQLPersistanceManager;
import com.cleanview.persist.common.Status;

public class Tests {
	
	MySQLPersistanceManager persistmanager = new MySQLPersistanceManager();
	Random rand = new Random(System.currentTimeMillis());
	
	//=============================Category only tests==========================//
	/**
	 * checks 
	 * 1. MySQLPersistanceManager.addCategory  
	 * 2. MySQLPersistanceManager.fetchCategoryDetailsByName
	 */
	public void insertCategoryDetails(){
		CategoryDetailsForPersistance details = new CategoryDetailsForPersistance();  
		
		details.setCategoryName("Category:" + Math.abs(rand.nextInt()) + "");		
		details.setDescription("From test");
		persistmanager.addCategory(details);
		System.out.println("Created category with name: " + details.getCategoryName());
	}
	/**
	 * checks 
	 * 1. MySQLPersistanceManager.fetchAllCategoryDetails()
	 * 1. MySQLPersistanceManager.updateCategoryDetails()
	 */
	public void fetchAllClassAndUpdateFirst(){
		List<CategoryDetailsForPersistance> details = new ArrayList<CategoryDetailsForPersistance>();
		persistmanager.fetchAllCategoryDetails(details);
		
		
		System.out.println("All category ids: ");
		for(CategoryDetailsForPersistance x: details) {
			System.out.println(x.getCategoryName());
		}
		System.out.println("\n\nUpdating category details with id: " + details.get(0).getCategoryId());
		
		System.out.println("original Values: " + details.get(0));
		details.get(0).setCategoryName(details.get(0).getCategoryName() + "_");
		details.get(0).setIsValid(true);
		persistmanager.updateCategoryDetails(details.get(0));
		System.out.println("Update Values: " + details.get(0));
	}
	
	//============================Brand only tests============================//
	public void insertBrandDetails(){
		BrandDetails brand = new BrandDetails();  
		
		brand.setBrandName("Brand_" + Math.abs(rand.nextInt()) + "");		
		brand.setDescription("From test");
		brand.setCategoryId(7);
		persistmanager.addBrand(brand);
		System.out.println("Create brand : " + brand.getBrandName());
	}
	
	public void fetchAllBrandsAndUpdateOne(){
		List<BrandDetails> details = new ArrayList<BrandDetails>();
		persistmanager.fetchAllBrandDetails(details,7);		
		
		System.out.println("All Brand ids: ");
		for(BrandDetails x: details) {
			System.out.println(x.getBrandName());
		}
		System.out.println("\n\nUpdating category details with id: " + details.get(0).getBrandId());
		
		System.out.println("original Values: " + details.get(0));
		details.get(0).setBrandName(details.get(0).getBrandName() + "_");		
		persistmanager.updateBrandDetails(details.get(0));
		System.out.println("Update Values: " + details.get(0));
	}
	
	//===========================Features test calls (item details test)===============================//
		
	public void testFeatureDetails(Integer category_id){
		//String tableName = "Sample_feature_details";
		FeatureDetails details = new FeatureDetails();
		
		details.setFeatureId("RAM" + Math.abs(rand.nextInt())%100 +10);
		details.setPriority(8);
		details.setMaxValue(10);
		details.setMinValue(0);
		details.setDescription("Higher is better");
		details.setUnit("GB");
		details.setGroupId("Memory");
		details.setCategoryId(category_id);
		
		if(persistmanager.addFeatureDetails(details) == Status.SUCCESS){
			System.out.println("Insert success: " + details);
		}
		System.out.println("Featring all the features of same category: ");
		fetchAllFeatures(category_id);
	}
	
	public void fetchAllFeatures(Integer categoryId){
		List<FeatureDetails> detailsList = new ArrayList<FeatureDetails>();
		if(persistmanager.fetchAllFeatureDetails(categoryId, detailsList) == Status.SUCCESS){
			
			for(FeatureDetails details: detailsList){
				System.out.println(details);
			}
		}
		updateFeatureDetails(categoryId, detailsList.get(0));
	}
	
	public void updateFeatureDetails(Integer categoryId, FeatureDetails details){
		String originalFeatureId = details.getFeatureId();
		details.setFeatureId(details.getFeatureId() + "_updated");
		if(persistmanager.updateFeatureDetails(details, categoryId, originalFeatureId) == Status.SUCCESS){
			System.out.println("Updated value: " + details);
		}
	}
		
	//============================Feature Group Test================================//
	public void featureGroupTest(Integer categoryId){
		String feature_group = "feature_group" +  Math.abs(rand.nextInt())%100 +10;
		String feature_group2 = "feature_group" +  Math.abs(rand.nextInt())%100 +10;
		GroupDetails gDetails = new GroupDetails();
		GroupDetails gDetails2 = new GroupDetails();
		gDetails.setGroupId(feature_group);
		gDetails.setDescription("More the pixel better the display");
		gDetails.setCategoryId(7);
		gDetails2.setGroupId(feature_group2);
		gDetails2.setDescription("The More the Better");
		gDetails2.setCategoryId(7);
		List<GroupDetails> gList = new ArrayList<GroupDetails>();
		//if(persistmanager.createCategoryFeatureGroupDetailsTable(tableName) == Status.SUCCESS){
		//	System.out.println(tableName +" created.");
			if(persistmanager.addFeatureGroupDetails(gDetails) == Status.SUCCESS){
				persistmanager.addFeatureGroupDetails(gDetails2);
				System.out.println("Added 2 Feature Group.");
				String originalId = gDetails.getGroupId();
				gDetails.setGroupId("Smart Display" + feature_group2);
				if(persistmanager.updateFeatureGroupDetails(gDetails,  originalId)==Status.SUCCESS){
					System.out.println("Updated feature group with Id " + originalId + " to " + gDetails.getGroupId());
					if(persistmanager.fetchAllGroupDetails(7, gList) == Status.SUCCESS){
						System.out.println(gList);
					}
				}
			}
		//}
	}
	
	//============================Value Test================================//
		public void valueTest(Integer categoryId, String featureId){
			//String tableName = "TEST" + Math.abs(rand.nextInt());
			ValueDetails vDetails = new ValueDetails();
			ValueDetails vDetails2 = new ValueDetails();
			
			vDetails.setValue("1");
			vDetails.setWeight(3);
			vDetails.setDescription("Average");
			vDetails.setCategoryId(categoryId);
			vDetails.setFeatureId(featureId);
			
			vDetails2.setValue("2");
			vDetails2.setWeight(4);
			vDetails2.setDescription("Good");
			vDetails2.setCategoryId(categoryId);
			vDetails2.setFeatureId(featureId);
			
			//String featureName = "Memory";
			List<ValueDetails> vList = new ArrayList<ValueDetails>();
			//if(persistmanager.createValueDetailsTable(categoryId, featureId) == Status.SUCCESS){
				//System.out.println(categoryId +" created.");
				if(persistmanager.addValueDetails(vDetails) == Status.SUCCESS){
					persistmanager.addValueDetails(vDetails2);
					System.out.println("Added 2 value details.");
					String originalValue = vDetails.getValue();
					vDetails.setValue(".5");
					if(persistmanager.updateValueDetails(categoryId, featureId, vDetails, originalValue)==Status.SUCCESS){
						System.out.println("Updated value " + originalValue + " to " + vDetails.getValue());
						if(persistmanager.fetchAllValuesDetails(categoryId, featureId,vList) == Status.SUCCESS){
							System.out.println(vList);
						}
					}
				}
			//}
		}
	
	
	//===========================Item Details test depends on feature Details test=======================//
	public void ItemDetailsTest(Integer categoryId){
		//String categoryName = "Cate_" + Math.abs(rand.nextInt());
		ItemDetails iDetails = new ItemDetails();
		iDetails.setCategoryId(1);
		iDetails.setBrandId(1);
		iDetails.setPrimaryImages("primary_image_url");
		iDetails.setSecondaryImages("secondary_image_url");
		iDetails.setModelName("TestModel");
		if(persistmanager.createItemDetailsTable(categoryId) == Status.SUCCESS){
			System.out.println("Created table: " + categoryId);
			if(persistmanager.addFeatureColumn(categoryId, "RAM8")== Status.SUCCESS){
				System.out.println("Create column for feature: RAM8");
				iDetails.getFeatures().put("RAM8", "4");
				if(persistmanager.addItemDetails(iDetails)==Status.SUCCESS){
					System.out.println("Added item:" + iDetails);
				}
				iDetails.setModelName("Model2");
				if(persistmanager.addItemDetails(iDetails)==Status.SUCCESS){
					System.out.println("Added item:" + iDetails);
				}
				iDetails.setModelName("Model3");
				if(persistmanager.addItemDetails(iDetails)==Status.SUCCESS){
					System.out.println("Added item:" + iDetails);
				}
				
				List<ItemDetails> fetchedResults = new ArrayList<ItemDetails>();
				if(persistmanager.fetchAllItemDetails(categoryId, fetchedResults, null, null)==Status.SUCCESS){
					System.out.println("Fetched: " + fetchedResults.size());
				}
				fetchedResults.clear();
				if(persistmanager.fetchAllItemDetails(categoryId, fetchedResults, 1, null)==Status.SUCCESS){
					System.out.println("Fetched: " + fetchedResults.size());
				}
				fetchedResults.clear();
				if(persistmanager.fetchAllItemDetails(categoryId, fetchedResults, 1, 2)==Status.SUCCESS){
					System.out.println("Fetched: " + fetchedResults.size());
				}
				fetchedResults.clear();
				if(persistmanager.fetchAllItemDetails(categoryId, fetchedResults, 1, 3)==Status.SUCCESS){
					System.out.println("Fetched: " + fetchedResults.size());
					System.out.println(fetchedResults.get(0));
				}
				
				fetchedResults.get(0).setModelName("Updated Model Name");
				if(persistmanager.updateItemDetails(fetchedResults.get(0))==Status.SUCCESS){
					System.out.println("Updated item id: " + fetchedResults.get(0).getItemId());
				}
			}
		}
		
		
	}
	
	public static void main(String[] args)
	{
		Tests tests = new Tests();
		
		//====================Category details test=================//
		//tests.insertCategoryDetails();
		//tests.fetchAllClassAndUpdateFirst();
		
		//====================Brand Details Test===================//
		//tests.insertBrandDetails();
		//tests.fetchAllBrandsAndUpdateOne();
		
		//===================Feature Details Test and ItemDetails Test==================//
		//tests.testFeatureDetails(7);
		
		//===================Feature Group Details ==================//
		//tests.featureGroupTest(7);
		

		//===================Feature Value Details ==================//
		//tests.valueTest(7,"RAM8");
		
		//===================== Add Item details =======================//
		tests.ItemDetailsTest(7);
	}
}
