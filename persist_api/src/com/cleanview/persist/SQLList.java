package com.cleanview.persist;

import com.cleanview.persist.common.CommonConstants;

public class SQLList {

	
	//====================================Categories============================================//
	public final static String  INSERT_CATEGORIES_DETAILS = 
			"INSERT INTO " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + "." + PersistUtils.getConfig(CommonConstants.CATEGORIES)
			+ "(category_id, name, parent_id, description, valid) "
			+ "VALUES(0,?,?,?,?)";	
	
	public final static String  UPDATE_CATEGORY_DETAILS = 			
			"UPDATE " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + "." + PersistUtils.getConfig(CommonConstants.CATEGORIES) 
			+ "  SET "
			+ "name=?, "
			+ "parent_id=?, "		
			+ "description=?, "
			+ "valid=? "
			+ "WHERE category_id=?;";	
	
	public final static String  FETCH_LAST_CREATED_CATEGORY_DETAILS =
			"SELECT category_id, name, parent_id, description, valid "
			+ " FROM " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + "." + PersistUtils.getConfig(CommonConstants.CATEGORIES)
			+ " ORDER BY category_id DESC LIMIT 1";
	
	public final static String  FETCH_CATEGORY_DETAILS_BY_CATEGORY_ID = 
			"SELECT category_id, name, parent_id, description, valid "
			+ " FROM " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + "." + PersistUtils.getConfig(CommonConstants.CATEGORIES)
			+ " WHERE category_id = ?";
	
	public final static String  FETCH_ALL_CATEGORIES = 
			"SELECT category_id, name, parent_id, description, valid "
			+ " FROM " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + "." + PersistUtils.getConfig(CommonConstants.CATEGORIES);
	
	public final static String  DELETE_CATEGORY = 
			"DELETE "
			+ " FROM " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + "." + PersistUtils.getConfig(CommonConstants.CATEGORIES) 
			+ " WHERE category_id = ?"	;
	
	//====================================Brands============================================//
	public final static String  INSERT_BRAND_DETAILS = 
			"INSERT INTO " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + "." + PersistUtils.getConfig(CommonConstants.BRANDS) 
			+ "(brand_id, name, description,category_id) "
			+ "VALUES(0,?,?,?)";	
	
	public final static String  FETCH_LAST_CREATED_BRAND_DETAILS =
			"SELECT brand_id, name, description, category_id "
			+ " FROM " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + "." + PersistUtils.getConfig(CommonConstants.BRANDS)
			+ " ORDER BY  brand_id DESC LIMIT 1";
	
	
	public final static String  UPDATE_BRAND_DETAILS = 			
			"UPDATE " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + "." + PersistUtils.getConfig(CommonConstants.BRANDS) 
			+ "  SET "
			+ " name=? ,"
			+ " description=?, "
			+ " category_id=? "
			+ " WHERE brand_id=?";
	
	public final static String  FETCH_BRAND_DETAILS_BY_BRAND_ID = 
			"SELECT brand_id, name, description, category_id "
			+ " FROM " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + "." + PersistUtils.getConfig(CommonConstants.BRANDS)
			+ " WHERE brand_id = ?";
		
	public final static String  FETCH_ALL_BRANDS_FOR_A_CATEGORY = 
			"SELECT brand_id, name, description, category_id "
			+ " FROM " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + "." + PersistUtils.getConfig(CommonConstants.BRANDS)
			+ " WHERE category_id=?";
	
	public final static String  DELETE_BRAND = 
			"DELETE "
			+ " FROM " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + "." + PersistUtils.getConfig(CommonConstants.BRANDS) 
			+ " WHERE brand_id = ?"	;
	
	//====================================Feature Details============================================//
/*	public final static String CREATE_FEATURE_DETAILS = 
			"CREATE TABLE  " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + ".[1] (  "
			+ "`feature_id` VARCHAR(50) NOT NULL COMMENT '',  "
			+ "`priority` INT NULL COMMENT '',  "
			+ "`max_value` INT NULL COMMENT '',  "
			+ "`min_value` INT NULL COMMENT '',  "
			+ "`description` VARCHAR(400) NULL COMMENT '',  "
			+ "`unit` VARCHAR(45) NULL COMMENT '',  "
			+ "`group_Id` VARCHAR(45) NULL COMMENT '',  "
			+ "PRIMARY KEY (`feature_id`)  COMMENT '');";*/
	
	public final static String  INSERT_FEATURE_DETAILS = 
			"INSERT INTO " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + "." + PersistUtils.getConfig(CommonConstants.FEATURE_DETAILS)//feature_details " 
			+ " (feature_id,feature_name, priority, max_value, min_value, description, unit, group_Id, category_id, hide) "
			+ "VALUES(?,?,?,?,?,?,?,?,?,?)";
	
	public final static String  UPDATE_FEATURE_DETAILS = 			
			"UPDATE " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + "." + PersistUtils.getConfig(CommonConstants.FEATURE_DETAILS)
					+ " SET "
					+ " feature_id =?, "
					+ " feature_name=?, "
					+ " priority =?, "
					+ " max_value =?, "
					+ " min_value =?, "
					+ " description =?, "
					+ " unit =?, "
					+ " group_id =?, "
					+ " category_id=?, "
					+ " hide=? "
					+ " WHERE feature_id =? and category_id=?";
	
	public final static String  FETCH_ALL_FEATURE_DETAILS = 
			"SELECT feature_id, feature_name, priority, max_value, min_value, description, unit, group_Id, category_id, hide "
			+ " FROM " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + "." + PersistUtils.getConfig(CommonConstants.FEATURE_DETAILS)
			+ " WHERE category_id=?";
	
	
	public final static String  DELETE_FEATURE = 
			"DELETE "
			+ " FROM " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + "." + PersistUtils.getConfig(CommonConstants.FEATURE_DETAILS) 
			+ " WHERE feature_id = ? and category_id=?"	;
	//====================================Feature Group Details============================================//
/*	public final static String CREATE_FEATURE_GROUP_DETAILS = 
			"CREATE TABLE  " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + ".[1] (  "
				+ "`group_id` VARCHAR(100) NOT NULL COMMENT '',  "
				+ "`description` VARCHAR(400) NULL COMMENT '',  "
				+ "PRIMARY KEY (`group_id`)  COMMENT '')";*/
	
	public final static String  INSERT_FEATURE_GROUP_DETAILS = 
			"INSERT INTO " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + "." + PersistUtils.getConfig(CommonConstants.FEATURE_GROUPS)//feature_groups"
			+" (group_id, description, category_id) "
				+ "VALUES(?,?,?)";
	
	public final static String  UPDATE_FEATURE_GROUP_DETAILS = 			
			"UPDATE " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + "." + PersistUtils.getConfig(CommonConstants.FEATURE_GROUPS)
					+ " SET "
					+ " group_id =?, "
					+ " description =?, "
					+ " category_id=? "
					+ " WHERE group_id =? AND category_id=?";
	
	public final static String  FETCH_ALL_GROUP_DETAILS = 
			"SELECT group_id, description, category_id "
			+ " FROM " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + "." + PersistUtils.getConfig(CommonConstants.FEATURE_GROUPS)
			+ " WHERE category_id=?";
	
	public final static String  DELETE_FEATURE_GROUP = 
			"DELETE "
			+ " FROM " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + "." + PersistUtils.getConfig(CommonConstants.FEATURE_GROUPS) 
			+ " WHERE group_id = ? and category_id=?";
	
	//====================================Value Details============================================//
	
	//====================================Value Details============================================//
	/*public final static String CREATE_VALUE_DETAILS = 
			"CREATE TABLE  " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + ".[1] (  "
				+ "`value` VARCHAR(100) NOT NULL COMMENT '',  "
				+ "`weight` INT NULL COMMENT '',  "
				+ "`description` VARCHAR(400) NULL COMMENT '',  "
				+ "PRIMARY KEY (`value`)  COMMENT '');";*/
	
	public final static String  INSERT_VALUE_DETAILS = 
			"INSERT INTO " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + "." + PersistUtils.getConfig(CommonConstants.VALUE_DETAILS)//value_details " 
				+" (value, value_name, weight, description, category_id, feature_id) "
				+ " VALUES(?,?,?,?,?,?)";
	
	public final static String  UPDATE_VALUE_DETAILS = 			
			"UPDATE " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + "." + PersistUtils.getConfig(CommonConstants.VALUE_DETAILS)
					+ " SET "
					+ " value =?, "
					+ " value_name =?, "
					+ " weight =?, "
					+ " description =?, "
					+ " category_id=?, "
					+ " feature_id=? "
					+ " WHERE value=? and category_id=? and feature_id=?";
	
	public final static String  FETCH_ALL_VALUE_DETAILS = 
			"SELECT value, value_name, weight, description, feature_id, category_id "
			+ " FROM " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + "." + PersistUtils.getConfig(CommonConstants.VALUE_DETAILS)
			+ " WHERE category_id=? and feature_id=?";
	
	public final static String  DELETE_VALUE = 
			"DELETE "
			+ " FROM " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + "." + PersistUtils.getConfig(CommonConstants.VALUE_DETAILS) 
			+ " WHERE value=? and category_id=? and feature_id=?";
		
	//====================================Item Details============================================//
	public final static String CREATE_ITEM_DETATILS = 
			"CREATE TABLE " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + ".[1] ("
					+ " `item_id` INT NOT NULL AUTO_INCREMENT COMMENT '',"
					+ " `category_id` INT NULL COMMENT '',"
					+ " `brand_id` INT NULL COMMENT '',"
					+ " `model_name` VARCHAR(100) NULL COMMENT '',"
					+ " `primary_images` TEXT NULL COMMENT '',"
					+ " `secondary_images` TEXT NULL COMMENT '',"
					+ " PRIMARY KEY (`item_id`) COMMENT '',"
					+ " INDEX `categoryID_[1]` (`category_id` ASC) COMMENT '',"
					+ " INDEX `brandID_[1]` (`brand_id` ASC) COMMENT '',"
					+ " CONSTRAINT `categoryID[1]`"
					+ " FOREIGN KEY (`category_id`)"
					+ " REFERENCES `cleanview`.`categories` (`category_id`)"
					+ " ON DELETE CASCADE"
					+ " ON UPDATE CASCADE,"
					+ " CONSTRAINT `brandID[1]`"
					+ " FOREIGN KEY (`brand_id`)"
					+ " REFERENCES `cleanview`.`brands` (`brand_id`)"
					+ " ON DELETE CASCADE"
					+ " ON UPDATE CASCADE)";

	public final static String ADD_FEATURE_TO_ITEM_DETAILS = 
			"ALTER TABLE " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + ".[1] "
					+ "ADD COLUMN [2] TEXT NULL ";

	public final static String CHANGE_FEATURE_NAME = 
			"ALTER TABLE " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + ".[1] "
			+ " CHANGE COLUMN [2] [3] TEXT NULL DEFAULT NULL";
	
	public final static String DELETE_FEATURE_COLUMN = 
			"ALTER TABLE " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + ".[1] "
			+ " DROP COLUMN [2]";
	
	public final static String REMOVE_FEATURE_FROM_ITEM_DETAILS = 
			"ALTER TABLE " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + ".[1] "
					+ "DROP COLUMN `?`;";
	
	public final static String  INSERT_ITEM_DETAILS = 
			"INSERT INTO " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + ".[1] " ;
	
	public final static String  UPDATE_ITEM_DETAILS = 			
			"UPDATE " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + ".[1]  "
					+ " SET category_id=?, "
					+ " brand_id = ?, "
					+ " model_name = ?, "
					+ " primary_images = ?, "
					+ " secondary_images = ? ";
	
	public final static String FETCH_ALL_ITEM_DETAILS =
			"SELECT [1] FROM " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + ".[2] ";
	
	public final static String  FETCH_LAST_CREATED_ITEM_DETAILS =
			"SELECT [1] FROM " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + ".[2] "
			+ " ORDER BY item_id DESC LIMIT 1";
	
	public final static String  FETCH_ITEM_DETAILS =
			"SELECT [1] FROM " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + ".[2] "
			+ " WHERE item_id = ?";
	
	public final static String  DELETE_ITEM = 
			"DELETE "
			+ " FROM " + PersistUtils.getConfig(CommonConstants.SCHEMA_NAME) + ".[1]  " 
			+ " WHERE item_id=?";
}