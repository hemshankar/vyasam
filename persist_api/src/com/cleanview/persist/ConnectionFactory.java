package com.cleanview.persist;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.cleanview.persist.common.CommonConstants;

public class ConnectionFactory {
	 private static ConnectionFactory instance = new ConnectionFactory();
	    
	    public static  String MYSQL_JDBC_URL = PersistUtils.getConfig(CommonConstants.MYSQL_JDBC_URL)
	    												+ PersistUtils.getConfig(CommonConstants.SCHEMA_NAME);
	    		//"jdbc:mysql://localhost:3306/" + CommonConstants.SCHEMA_NAME;
	    public static  String USER = PersistUtils.getConfig(CommonConstants.USER);//"root";
	    public static  String PASSWORD = PersistUtils.getConfig(CommonConstants.PASSWORD);//"infa";
	    public static  String DRIVER_CLASS = PersistUtils.getConfig(CommonConstants.DRIVER_CLASS);//"com.mysql.jdbc.Driver"; 
	     
	    //private constructor
	    private ConnectionFactory() {
	        try {
	            Class.forName("com.mysql.jdbc.Driver");//PersistUtils.getConfig(CommonConstants.DRIVER_CLASS));
	        } catch (ClassNotFoundException e) {
	            e.printStackTrace();
	        }
	    }
	     
	    private Connection createConnection() {
	        Connection connection = null;
	        try {
	            connection = DriverManager.getConnection(MYSQL_JDBC_URL, USER, PASSWORD);
	        } catch (SQLException e) {
	        	e.printStackTrace();
	            System.out.println("ERROR: Unable to Connect to Database.");
	        }
	        return connection;
	    }   
	     
	    public static Connection getConnection() {
	        return instance.createConnection();
	    }
}
