package com.cleanview.persist;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import com.common.exceptions.UtilityException;
import com.common.utilities.Utility;

public class PersistUtils {
	public static Random r = new Random();
	
	private static PropertyValues propValues = new PropertyValues();
	
	public static class PropertyValues {
		String result = "";
		InputStream inputStream;
		Properties prop = null;
		
		public void populateProps() throws IOException{
			try{
				String propFileName = "config.properties";
				prop = new Properties(); 
				inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
	 
				if (inputStream != null) {
					prop.load(inputStream);
				} else {
					throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
				}
			}catch(Exception e){
				e.printStackTrace();
			}finally {
				inputStream.close();
			}
		}
		
		public PropertyValues(){			
			if(prop == null){
				try {
					populateProps();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		public String getPropValues(String key) {
 			try {
				result = prop.getProperty(key);
			} catch (Exception e) {
				System.out.println("Exception: " + e);
			} 
			return result;
		}
	}
	
	public static String getConfig(String key){
		return propValues.getPropValues(key);
	}
	
	public static String removeWhiteSpaces(String str){    	
    	str =  str.replaceAll("\\W+", "_");
    	return str;
    }
	
	public Integer getRandomNumberBetween(Integer high, Integer low){
		return r.nextInt(high-low) + low;
	}
}
