package com.cleanview.persist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.cleanview.models.BrandDetails;
import com.cleanview.models.CategoryDetailsForPersistance;
import com.cleanview.models.FeatureDetails;
import com.cleanview.models.GroupDetails;
import com.cleanview.models.ItemDetails;
import com.cleanview.models.ValueDetails;
import com.cleanview.persist.common.Status;
import com.common.utilities.Utility;

public class MySQLPersistanceManager {
	
	//==============================CategoryDetails persistance apis=============================//
	public Status addCategory(CategoryDetailsForPersistance categoryDetails) {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.INSERT_CATEGORIES_DETAILS;
		
		try{
			conn = ConnectionFactory.getConnection();
			ps = conn.prepareStatement(query);
			int i = 1;
			ps.setString(i++, categoryDetails.getCategoryName());
			ps.setInt(i++, categoryDetails.getParentId());
			ps.setString(i++, categoryDetails.getDescription());
			ps.setString(i++, categoryDetails.getIsValid() + "");
			
			ps.execute();
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	
	public Status fetchLastCreatedCategory(CategoryDetailsForPersistance categoryDetails){
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.FETCH_LAST_CREATED_CATEGORY_DETAILS;
		
		try{
			conn = ConnectionFactory.getConnection();
			ps = conn.prepareStatement(query);
			
			rs = ps.executeQuery();			
			while(rs.next()){
				categoryDetails.setCategoryId(rs.getInt("category_id"));
				categoryDetails.setCategoryName(rs.getString("name"));
				categoryDetails.setParentId(rs.getInt("parent_id"));						
				categoryDetails.setDescription(rs.getString("description"));
				categoryDetails.setIsValid(rs.getString("valid").equalsIgnoreCase("true")?true:false);
			}
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	
	public Status fetchCategoryDetailsById(CategoryDetailsForPersistance cateDetails){
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.FETCH_CATEGORY_DETAILS_BY_CATEGORY_ID;
		
		try{
			conn = ConnectionFactory.getConnection();
			ps = conn.prepareStatement(query);
			
			ps.setInt(1, cateDetails.getCategoryId());			
			rs = ps.executeQuery();
			
			while(rs.next()){
				cateDetails.setCategoryId(rs.getInt("category_id"));
				cateDetails.setCategoryName(rs.getString("name"));
				cateDetails.setParentId(rs.getInt("parent_id"));						
				cateDetails.setDescription(rs.getString("description"));
				cateDetails.setIsValid(rs.getString("valid").equalsIgnoreCase("true")?true:false);
			}
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	
	public Status fetchAllCategoryDetails(List<CategoryDetailsForPersistance> cateDetailsList){
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.FETCH_ALL_CATEGORIES;
		
		try{
			conn = ConnectionFactory.getConnection();
			ps = conn.prepareStatement(query);			
					
			rs = ps.executeQuery();
			CategoryDetailsForPersistance cateDetails = null;
			while(rs.next()){
				cateDetails = new CategoryDetailsForPersistance();
				
				cateDetails.setCategoryId(rs.getInt("category_id"));
				cateDetails.setCategoryName(rs.getString("name"));
				cateDetails.setParentId(rs.getInt("parent_id"));				
				cateDetails.setDescription(rs.getString("description"));
				cateDetails.setIsValid(rs.getString("valid").equalsIgnoreCase("true")?true:false);
				
				cateDetailsList.add(cateDetails);
			}
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	
	public Status updateCategoryDetails(CategoryDetailsForPersistance cateDetails) {
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.UPDATE_CATEGORY_DETAILS;
		
		try{
			conn = ConnectionFactory.getConnection();
			ps = conn.prepareStatement(query);
			int i = 1;
			ps.setString(i++, cateDetails.getCategoryName());
			ps.setInt(i++, cateDetails.getParentId());
			ps.setString(i++, cateDetails.getDescription());
			ps.setString(i++, cateDetails.getIsValid() + "");
			ps.setInt(i++, cateDetails.getCategoryId());
			ps.execute();
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}

	public Status deleteCategoryDetail(CategoryDetailsForPersistance categoryDetail){
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.DELETE_CATEGORY;
		
		try{
			conn = ConnectionFactory.getConnection();
			ps = conn.prepareStatement(query);
			int i = 1;		
			ps.setInt(i++, categoryDetail.getCategoryId());
			ps.execute();
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	
	//==============================BrandDetails persistance apis=============================//
	public Status addBrand(BrandDetails brandDetails){
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.INSERT_BRAND_DETAILS;
		
		try{
			conn = ConnectionFactory.getConnection();
			ps = conn.prepareStatement(query);
			int i = 1;
			ps.setString(i++, brandDetails.getBrandName());
			ps.setString(i++, brandDetails.getDescription());
			ps.setInt(i++, brandDetails.getCategoryId());
			
			ps.execute();			
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	

	public Status fetchLastCreatedBrand(BrandDetails brandDetails){
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.FETCH_LAST_CREATED_BRAND_DETAILS;
		
		try{
			conn = ConnectionFactory.getConnection();
			ps = conn.prepareStatement(query);
			
			rs = ps.executeQuery();			
			while(rs.next()){
				brandDetails.setCategoryId(rs.getInt("category_id"));
				brandDetails.setBrandId(rs.getInt("brand_id"));
				brandDetails.setBrandName(rs.getString("name"));						
				brandDetails.setDescription(rs.getString("description"));				
			}
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	
	
	public Status fetchBrandDetailsByBrandId(BrandDetails brandDetails){
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.FETCH_BRAND_DETAILS_BY_BRAND_ID;
		
		try{
			conn = ConnectionFactory.getConnection();
			ps = conn.prepareStatement(query);
			
			int i=1;
			ps.setInt(i++, brandDetails.getBrandId());	
			
			rs = ps.executeQuery();
			
			while(rs.next()){
				brandDetails.setBrandId(rs.getInt("brand_id"));
				brandDetails.setBrandName(rs.getString("name"));										
				brandDetails.setDescription(rs.getString("description"));
				brandDetails.setCategoryId(rs.getInt("category_id"));
			}
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}

	public Status fetchAllBrandDetails(List<BrandDetails> brandDetailsList, Integer categoryId){
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.FETCH_ALL_BRANDS_FOR_A_CATEGORY;
		
		try{
			conn = ConnectionFactory.getConnection();
			ps = conn.prepareStatement(query);			
			
			ps.setInt(1, categoryId);		
			
			rs = ps.executeQuery();
			BrandDetails brandDetails = null;
			while(rs.next()){
				brandDetails = new BrandDetails();				
				brandDetails.setBrandId(rs.getInt("brand_id"));
				brandDetails.setBrandName(rs.getString("name"));		
				brandDetails.setDescription(rs.getString("description"));
				brandDetails.setCategoryId(rs.getInt("category_id"));
				
				brandDetailsList.add(brandDetails);
			}
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	
	public Status updateBrandDetails(BrandDetails brandDetails){
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.UPDATE_BRAND_DETAILS;
		
		try{
			conn = ConnectionFactory.getConnection();
			ps = conn.prepareStatement(query);
			int i = 1;
			ps.setString(i++, brandDetails.getBrandName());
			ps.setString(i++, brandDetails.getDescription());
			ps.setInt(i++, brandDetails.getCategoryId());
			ps.setInt(i++, brandDetails.getBrandId());
			ps.execute();
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	
	public Status deleteBrand(BrandDetails brand){
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.DELETE_BRAND;
		
		try{
			conn = ConnectionFactory.getConnection();
			ps = conn.prepareStatement(query);
			int i = 1;		
			ps.setInt(i++, brand.getBrandId());
			ps.execute();
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	
	//==============================Category Feature Details persistance apis=============================//
	/*public Status createCategoryFeatureDetailsTable(String categoryName){
		String tableName = categoryName + "_feature_details";
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.CREATE_FEATURE_DETAILS;
		List<String> values = new ArrayList<String>();
		values.add(tableName);
		try{
			conn = ConnectionFactory.getConnection();
			query = replacePlaceHolders(query,values);
			ps = conn.prepareStatement(query);			
			ps.execute();			
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	*/
	public Status addFeatureDetails(FeatureDetails featureDetails){		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.INSERT_FEATURE_DETAILS;
		
		try{
			conn = ConnectionFactory.getConnection();			
			ps = conn.prepareStatement(query);
			
			int i = 1;
			ps.setString(i++, featureDetails.getFeatureId());
			ps.setString(i++, featureDetails.getFeatureName());
			ps.setInt(i++, featureDetails.getPriority());
			ps.setInt(i++, featureDetails.getMaxValue());
			ps.setInt(i++, featureDetails.getMinValue());
			ps.setString(i++, featureDetails.getDescription());
			ps.setString(i++, featureDetails.getUnit());
			ps.setString(i++, featureDetails.getGroupId());			
			ps.setInt(i++, featureDetails.getCategoryId());
			ps.setString(i++, featureDetails.getHide()==true?"true":"false");
			
			ps.execute();			
			
			//add a new column in the items details
			addFeatureColumn(featureDetails.getCategoryId(), featureDetails.getFeatureId());
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	
	public Status fetchAllFeatureDetails(Integer categoryId, List<FeatureDetails> featureDetailsList){
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.FETCH_ALL_FEATURE_DETAILS;
		
		try{
			conn = ConnectionFactory.getConnection();
			ps = conn.prepareStatement(query);
			ps.setInt(1, categoryId);
			rs = ps.executeQuery();
			FeatureDetails featureDetails = null;
			while(rs.next()){
				featureDetails = new FeatureDetails();
				
				featureDetails.setFeatureId(rs.getString("feature_id"));
				featureDetails.setFeatureName(rs.getString("feature_name"));
				featureDetails.setGroupId(rs.getString("group_id"));		
				featureDetails.setPriority(rs.getInt("priority"));
				featureDetails.setMinValue(rs.getInt("min_value"));
				featureDetails.setMaxValue(rs.getInt("max_value"));
				featureDetails.setDescription(rs.getString("description"));
				featureDetails.setUnit(rs.getString("unit"));
				featureDetails.setCategoryId(rs.getInt("category_id"));
				featureDetails.setHide(rs.getString("hide").equalsIgnoreCase("true")?true:false);
				featureDetailsList.add(featureDetails);
			}
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	
	public Status updateFeatureDetails(FeatureDetails featureDetails, Integer categoryId,String originalFeatureId){
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.UPDATE_FEATURE_DETAILS;
		
		try{
			conn = ConnectionFactory.getConnection();
			
			ps = conn.prepareStatement(query);
			
			int i = 1;
			ps.setString(i++, featureDetails.getFeatureId());
			ps.setString(i++, featureDetails.getFeatureName());
			ps.setInt(i++, featureDetails.getPriority());
			ps.setInt(i++, featureDetails.getMaxValue());
			ps.setInt(i++, featureDetails.getMinValue());
			ps.setString(i++, featureDetails.getDescription());
			ps.setString(i++, featureDetails.getUnit());
			ps.setString(i++, featureDetails.getGroupId());			
			ps.setInt(i++, categoryId);
			ps.setString(i++, featureDetails.getHide()==true?"true":"false");
			ps.setString(i++, originalFeatureId);
			ps.setInt(i++, categoryId);
			
			ps.execute();		
			
			//update the column name in the items table
			updateFeatureColumn(categoryId,originalFeatureId,featureDetails.getFeatureId());
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	
	public Status deleteFeature(FeatureDetails fd){
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.DELETE_FEATURE;
		
		try{
			conn = ConnectionFactory.getConnection();
			ps = conn.prepareStatement(query);
			int i = 1;		
			ps.setString(i++, fd.getFeatureId());
			ps.setInt(i++, fd.getCategoryId());
			ps.execute();
			
			//delete the column name in the items table
			try{
				deleteFeatureColumn(fd.getCategoryId(),fd.getFeatureId());
			}catch(Exception e){
				//log a severe error that the column in Items table is not deleted 
				//but the rest of the to modules will not be affected as the column is not present in the
				//featrue_details table
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	
//==============================Category Feature Group Details persistance apis=============================//
	/*public Status createCategoryFeatureGroupDetailsTable(String categoryName){
		String tableName = categoryName + "_feature_group_details";
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.CREATE_FEATURE_GROUP_DETAILS;
		List<String> values = new ArrayList<String>();
		values.add(tableName);
		try{
			conn = ConnectionFactory.getConnection();
			query = replacePlaceHolders(query,values);
			ps = conn.prepareStatement(query);			
			ps.execute();			
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}*/
	
	public Status addFeatureGroupDetails(GroupDetails groupDetails){
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.INSERT_FEATURE_GROUP_DETAILS;
		
		try{
			conn = ConnectionFactory.getConnection();			
			ps = conn.prepareStatement(query);
			
			int i = 1;
			ps.setString(i++, groupDetails.getGroupId());
			ps.setString(i++, groupDetails.getDescription());	
			ps.setInt(i++, groupDetails.getCategoryId());
			ps.execute();			
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	
	public Status fetchAllGroupDetails(Integer categoryId, List<GroupDetails> groupDetailsList){
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.FETCH_ALL_GROUP_DETAILS;
		
		try{
			conn = ConnectionFactory.getConnection();			
			ps = conn.prepareStatement(query);
			ps.setInt(1, categoryId);
			rs = ps.executeQuery();
			GroupDetails groupDetails = null;
			while(rs.next()){
				groupDetails = new GroupDetails();
				
				groupDetails.setGroupId(rs.getString("group_id"));		
				groupDetails.setDescription(rs.getString("description"));
				groupDetails.setCategoryId(rs.getInt("category_id"));
				groupDetailsList.add(groupDetails);
			}
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	
	public Status updateFeatureGroupDetails(GroupDetails groupDetails,String originalGroupId){
			
			Connection conn = null;
			PreparedStatement ps = null;
			ResultSet rs = null;
			String query = SQLList.UPDATE_FEATURE_GROUP_DETAILS;
			
			try{
				conn = ConnectionFactory.getConnection();
				ps = conn.prepareStatement(query);
				
				int i = 1;
				ps.setString(i++, groupDetails.getGroupId());
				ps.setString(i++, groupDetails.getDescription());
				ps.setInt(i++, groupDetails.getCategoryId());
				ps.setString(i++, originalGroupId);
				ps.setInt(i++, groupDetails.getCategoryId());
				
				ps.execute();			
				
			} catch(Exception e) {			
				e.printStackTrace();
				return Status.FALIUER;
			}
			finally {
				DBUtil.close(conn);
				DBUtil.close(ps);
				DBUtil.close(rs);
			}
			return Status.SUCCESS;
		}		
	
	public Status deleteFeatureGroup(GroupDetails gd){
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.DELETE_FEATURE_GROUP;
		
		try{
			conn = ConnectionFactory.getConnection();
			ps = conn.prepareStatement(query);
			int i = 1;		
			ps.setString(i++, gd.getGroupId());
			ps.setInt(i++, gd.getCategoryId());
			ps.execute();
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
		
//==============================Value Details persistance apis=============================//
	/*public Status createValueDetailsTable(Integer categoryId, String featureName){
		String tableName = categoryId + "_" + featureName + "_value_details";
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.CREATE_VALUE_DETAILS;
		List<String> values = new ArrayList<String>();
		values.add(tableName);
		try{
			conn = ConnectionFactory.getConnection();
			query = replacePlaceHolders(query,values);
			ps = conn.prepareStatement(query);			
			ps.execute();			
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}*/
	
	public Status addValueDetails(ValueDetails valueDetails){
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.INSERT_VALUE_DETAILS;
		
		try{
			conn = ConnectionFactory.getConnection();
			ps = conn.prepareStatement(query);
			
			int i = 1;
			ps.setString(i++, valueDetails.getValue());
			ps.setString(i++, valueDetails.getValueName());
			ps.setInt(i++, valueDetails.getWeight());
			ps.setString(i++, valueDetails.getDescription());	
			ps.setInt(i++, valueDetails.getCategoryId());
			ps.setString(i++, valueDetails.getFeatureId());
			ps.execute();			
			
		} catch(Exception e) {			
			//e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	
	public Status fetchAllValuesDetails(Integer categoryId, String featureName, List<ValueDetails> valueDetailsList){
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.FETCH_ALL_VALUE_DETAILS;
		
		try{
			conn = ConnectionFactory.getConnection();
			ps = conn.prepareStatement(query);
			int i = 1;
			
			ps.setInt(i++, categoryId);
			ps.setString(i++, featureName);
			
			rs = ps.executeQuery();
			ValueDetails valueDetails = null;
			while(rs.next()){
				valueDetails = new ValueDetails();
				
				valueDetails.setValue(rs.getString("value"));
				valueDetails.setValueName(rs.getString("value_name"));
				valueDetails.setWeight(rs.getInt("weight"));
				valueDetails.setDescription(rs.getString("description"));
				valueDetails.setCategoryId(rs.getInt("category_id"));
				valueDetails.setFeatureId(rs.getString("feature_id"));
				valueDetailsList.add(valueDetails);
			}
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	
	public Status updateValueDetails(Integer categoryId, String featureId,ValueDetails valueDetails, String originalValue){
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.UPDATE_VALUE_DETAILS;
		
		try{
			conn = ConnectionFactory.getConnection();
			ps = conn.prepareStatement(query);
			
			int i = 1;
			ps.setString(i++, valueDetails.getValue());
			ps.setString(i++, valueDetails.getValueName());
			ps.setInt(i++, valueDetails.getWeight());
			ps.setString(i++, valueDetails.getDescription());
			ps.setInt(i++, valueDetails.getCategoryId());
			ps.setString(i++, valueDetails.getFeatureId());
			ps.setString(i++, originalValue);
			ps.setInt(i++, categoryId);
			ps.setString(i++, featureId);
			
			ps.execute();
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}		

	public Status deleteValueDetails(ValueDetails valueD){
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.DELETE_VALUE;
		
		try{
			conn = ConnectionFactory.getConnection();
			ps = conn.prepareStatement(query);
			int i = 1;		
			ps.setString(i++, valueD.getValue());			
			ps.setInt(i++, valueD.getCategoryId());
			ps.setString(i++, valueD.getFeatureId());
			ps.execute();
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
		
				
	//==============================Category Item Details persistance apis=============================//
	public Status createItemDetailsTable(Integer categoryId){
		String tableName = categoryId + "_item_details";
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.CREATE_ITEM_DETATILS;
		List<String> values = new ArrayList<String>();
		values.add(tableName);
		try{
			conn = ConnectionFactory.getConnection();
			query = replacePlaceHolders(query,values);
			ps = conn.prepareStatement(query);			
			ps.execute();			
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	
	public Status addFeatureColumn(Integer categoryId, String featureName) {
		featureName = PersistUtils.removeWhiteSpaces(featureName);
		String tableName = categoryId + "_item_details";
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.ADD_FEATURE_TO_ITEM_DETAILS;
		List<String> values = new ArrayList<String>();
		
		values.add(tableName);	
		values.add(featureName);
		
		try{
			conn = ConnectionFactory.getConnection();
			query = replacePlaceHolders(query,values);
			ps = conn.prepareStatement(query);
			
			ps.execute();			
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	
	public Status updateFeatureColumn(Integer categoryName, String oldFeatureName,String newFeatureName)
	{ 
		oldFeatureName = PersistUtils.removeWhiteSpaces(oldFeatureName);
		newFeatureName = PersistUtils.removeWhiteSpaces(newFeatureName);
		String tableName = categoryName + "_item_details";
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.CHANGE_FEATURE_NAME;
		List<String> values = new ArrayList<String>();
		
		values.add(tableName);	
		values.add(oldFeatureName);
		values.add(newFeatureName);
		
		try{
			conn = ConnectionFactory.getConnection();
			query = replacePlaceHolders(query,values);
			ps = conn.prepareStatement(query);
			
			ps.execute();			
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
		 
	}
	
	public Status deleteFeatureColumn(Integer categoryName,String featureName)
	{ 
		String tableName = categoryName + "_item_details";
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.DELETE_FEATURE_COLUMN;
		List<String> values = new ArrayList<String>();
		
		values.add(tableName);	
		values.add(featureName);
		try{
			conn = ConnectionFactory.getConnection();
			query = replacePlaceHolders(query,values);
			ps = conn.prepareStatement(query);			
			ps.execute();			
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
		 
	}
	
	public Status addItemDetails(ItemDetails itemDetails){
		String tableName = itemDetails.getCategoryId() + "_item_details";
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.INSERT_ITEM_DETAILS;
		List<String> values = new ArrayList<String>();
		values.add(tableName);
		
		List<String> queryFormat = formColumnListForItemDetails(itemDetails.getFeatures());
		query = query + queryFormat.get(0) + " VALUES" + queryFormat.get(1);
				
		try{
			conn = ConnectionFactory.getConnection();
			query = replacePlaceHolders(query,values);
			ps = conn.prepareStatement(query);
						
			int i = 1;			
			ps.setInt(i++, itemDetails.getCategoryId());
			ps.setInt(i++, itemDetails.getBrandId());
			ps.setString(i++, itemDetails.getModelName());
			ps.setString(i++, itemDetails.getPrimaryImages());
			ps.setString(i++, itemDetails.getSecondaryImages());
			Map<String,String> features = itemDetails.getFeatures(); 
			for(String str: features.keySet()){
				ps.setString(i++, features.get(str));
			}
			
			ps.execute();			
			
			addFeatureValuesToValueDetails(itemDetails.getCategoryId(),itemDetails.getFeatures());
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	
	public Status fetchLastCreatedItem(ItemDetails itemDetails, Integer categoryId){
		String tableName = categoryId + "_item_details";
		
		//fetch all column details
		List<FeatureDetails> featureDetailsList = new ArrayList<FeatureDetails>();
		if(!(fetchAllFeatureDetails(categoryId, featureDetailsList) == Status.SUCCESS))
			return Status.FALIUER;
		
		//form column name for query
		String columns = formColumnListForItemDetails(featureDetailsList);
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.FETCH_LAST_CREATED_ITEM_DETAILS;
		List<String> values = new ArrayList<String>();
		values.add(columns);
		values.add(tableName);
		
		try{
			conn = ConnectionFactory.getConnection();
			query = replacePlaceHolders(query,values);
			ps = conn.prepareStatement(query);
			
			rs = ps.executeQuery();			
			while(rs.next()){				
				
				itemDetails.setItemId(rs.getInt("item_id"));
				itemDetails.setCategoryId(rs.getInt("category_id"));		
				itemDetails.setBrandId(rs.getInt("brand_id"));
				itemDetails.setModelName(rs.getString("model_name"));
				itemDetails.setPrimaryImages(rs.getString("primary_images"));
				itemDetails.setSecondaryImages(rs.getString("secondary_images"));
				for(FeatureDetails details : featureDetailsList){
					itemDetails.getFeatures().put(details.getFeatureId(), rs.getString(PersistUtils.removeWhiteSpaces(details.getFeatureId())));
				}
			}
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	
	public Status fetchItem(ItemDetails itemDetails){
		String tableName = itemDetails.getCategoryId() + "_item_details";
		
		//fetch all column details
		List<FeatureDetails> featureDetailsList = new ArrayList<FeatureDetails>();
		if(!(fetchAllFeatureDetails(itemDetails.getCategoryId(), featureDetailsList) == Status.SUCCESS))
			return Status.FALIUER;
		
		//form column name for query
		String columns = formColumnListForItemDetails(featureDetailsList);
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.FETCH_ITEM_DETAILS;
		List<String> values = new ArrayList<String>();
		values.add(columns);
		values.add(tableName);
		
		try{
			conn = ConnectionFactory.getConnection();
			query = replacePlaceHolders(query,values);
			ps = conn.prepareStatement(query);
			ps.setInt(1, itemDetails.getItemId());
			rs = ps.executeQuery();			
			while(rs.next()){				
				
				itemDetails.setItemId(rs.getInt("item_id"));
				itemDetails.setCategoryId(rs.getInt("category_id"));		
				itemDetails.setBrandId(rs.getInt("brand_id"));
				itemDetails.setModelName(rs.getString("model_name"));
				itemDetails.setPrimaryImages(rs.getString("primary_images"));
				itemDetails.setSecondaryImages(rs.getString("secondary_images"));
				for(FeatureDetails details : featureDetailsList){
					itemDetails.getFeatures().put(details.getFeatureId(), rs.getString(details.getFeatureId()));
				}
			}
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	
	
	
	public Status fetchAllItemDetails(Integer categoryId, List<ItemDetails> itemDetailsList, Integer offset, Integer numOfRows){
		String tableName = categoryId + "_item_details";

		//fetch all column details
		List<FeatureDetails> featureDetailsList = new ArrayList<FeatureDetails>();
		if(!(fetchAllFeatureDetails(categoryId, featureDetailsList) == Status.SUCCESS))
			return Status.FALIUER;
		
		String columns = formColumnListForItemDetails(featureDetailsList);
		String recordRange = "";
		
		if(offset !=null && offset != -1) {
			recordRange = " LIMIT " + offset; 
			if(numOfRows != null)
				recordRange = recordRange + ", " + numOfRows;
		}
		
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.FETCH_ALL_ITEM_DETAILS;
		List<String> values = new ArrayList<String>();
		values.add(columns);
		values.add(tableName);
		
		try{
			conn = ConnectionFactory.getConnection();
			query = replacePlaceHolders(query,values);
			query = query + recordRange;
			ps = conn.prepareStatement(query);
			
			rs = ps.executeQuery();
			ItemDetails itemDetails = null;
			while(rs.next()){
				itemDetails = new ItemDetails();
				
				itemDetails.setItemId(rs.getInt("item_id"));
				itemDetails.setCategoryId(rs.getInt("category_id"));		
				itemDetails.setBrandId(rs.getInt("brand_id"));
				itemDetails.setModelName(rs.getString("model_name"));
				itemDetails.setPrimaryImages(rs.getString("primary_images"));
				itemDetails.setSecondaryImages(rs.getString("secondary_images"));
				for(FeatureDetails details : featureDetailsList){
					itemDetails.getFeatures().put(details.getFeatureId(), rs.getString(details.getFeatureId()));
				}
				
				itemDetailsList.add(itemDetails);
			}
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	
	public Status updateItemDetails(ItemDetails itemDetails){
		String tableName = itemDetails.getCategoryId() + "_item_details";
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.UPDATE_ITEM_DETAILS;
		List<String> values = new ArrayList<String>();
		values.add(tableName);
		
		String setFeatureQueryPart = formColumnListForItemDetailsUpdate(itemDetails.getFeatures());
		query = query + setFeatureQueryPart + " WHERE item_id=?";
				
		try{
			conn = ConnectionFactory.getConnection();
			query = replacePlaceHolders(query,values);
			ps = conn.prepareStatement(query);
						
			int i = 1;			
			ps.setInt(i++, itemDetails.getCategoryId());
			ps.setInt(i++, itemDetails.getBrandId());
			ps.setString(i++, itemDetails.getModelName());
			ps.setString(i++, itemDetails.getPrimaryImages());
			ps.setString(i++, itemDetails.getSecondaryImages());
			Map<String,String> features = itemDetails.getFeatures(); 
			for(String str: features.keySet()){
				ps.setString(i++, features.get(str));
			}
			ps.setInt(i++, itemDetails.getItemId());
			ps.execute();			
			addFeatureValuesToValueDetails(itemDetails.getCategoryId(),itemDetails.getFeatures());
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	
	
	public Status deleteItem(ItemDetails itemDetails){
		String tableName = itemDetails.getCategoryId() + "_item_details";
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = SQLList.DELETE_ITEM;
		List<String> values = new ArrayList<String>();
		values.add(tableName);
				
		try{
			conn = ConnectionFactory.getConnection();
			query = replacePlaceHolders(query,values);
			ps = conn.prepareStatement(query);
						
			int i = 1;
			ps.setInt(i++, itemDetails.getItemId());
			ps.execute();
			
		} catch(Exception e) {			
			e.printStackTrace();
			return Status.FALIUER;
		}
		finally {
			DBUtil.close(conn);
			DBUtil.close(ps);
			DBUtil.close(rs);
		}
		return Status.SUCCESS;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//===========================private methods====================================//
	private String replacePlaceHolders(final String query, List<String> values){
		String result = query;		
		int i=0;
		for(String str: values){
			i++;			
			result = result.replace("[" + i + "]", str);
		}		
		return result;
	}	

	private List<String> formColumnListForItemDetails(Map<String,String> featureValues){
		List<String> result = new ArrayList<String>();
		String cols="(item_id, category_id, brand_id, model_name, primary_images, secondary_images";		
		String values="(0,?,?,?,?,?";
		for(String str: featureValues.keySet()){
			cols = cols + ", " + PersistUtils.removeWhiteSpaces(str);
			values = values + ", ?";
		}		
		cols = cols + ")";
		values = values + ")";
		
		result.add(cols);
		result.add(values);
		
		return result;
	}
	private void addFeatureValuesToValueDetails(Integer cateId,Map<String,String> featureValues){
		for(String key: featureValues.keySet()){
			ValueDetails vd = null;
			try{
			vd = new ValueDetails();
			vd.setCategoryId(cateId);
			vd.setFeatureId(key);
			vd.setValue(featureValues.get(key));
			vd.setValueName(featureValues.get(key).replaceAll("\\W+", " "));
			addValueDetails(vd);
			} catch (Exception e){
				System.out.println(vd.getValue() + " not added in value_details table. : " + e.getMessage());
			}
		}
	}
	
	private String formColumnListForItemDetails(List<FeatureDetails> features){
		
		String cols=" item_id, category_id, brand_id, model_name, primary_images, secondary_images";
		
		for(FeatureDetails details: features){
			cols = cols + ", " + PersistUtils.removeWhiteSpaces(details.getFeatureId());			
		}
		
		return cols;
	}

	private String formColumnListForItemDetailsUpdate(Map<String,String> featureValues){
		
		String result = " ";
		for(String key: featureValues.keySet()){
			result = result + ", " + PersistUtils.removeWhiteSpaces(key) + " = ? ";//'" + featureValues.get(key) + " ";
		}	
		return result;
	}
}
