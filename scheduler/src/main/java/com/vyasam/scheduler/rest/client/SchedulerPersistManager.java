package com.vyasam.scheduler.rest.client;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.ClientConfig;

import com.vyasam.constants.FetchStatus;
import com.vyasam.models.ItemDetails;
import javax.ws.rs.client.Entity;

public class SchedulerPersistManager {

	public List<ItemDetails> fetchNewJobs() throws Exception
	{
		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(PersistURLs.FETCH_NEW_JOBS);
		List<ItemDetails> list = target
									.queryParam("SQL", "Select * from compareo.ItemDetails where status='" + FetchStatus.NEW + "' ORDER BY RAND() limit 15")
									.request(MediaType.APPLICATION_XML)
									.get(new GenericType<List<ItemDetails>>(){});
		return list;
	}
	
	public List<ItemDetails> fetchNewJobsForFirstTime() throws Exception
	{
		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(PersistURLs.FETCH_NEW_JOBS_FOR_FIRST_TIME);
		List<ItemDetails> list = target
									.queryParam("SQL", "Select * from compareo.ItemDetails where status='" + FetchStatus.QUEUED + "'"
																	    + " OR status='" + FetchStatus.FETCHING + "' ORDER BY RAND()")
									.request(MediaType.APPLICATION_XML)
									.get(new GenericType<List<ItemDetails>>(){});
		return list;
	}
	
	public void saveItemDetails(ItemDetails itemDetails) throws Exception
    {
	    
        ClientConfig config = new ClientConfig();
        Client client = ClientBuilder.newClient(config);
        WebTarget target = client.target(PersistURLs.SAVE_ITEM_DETAILS);

        target.request(MediaType.APPLICATION_XML).
                                post(Entity.entity(itemDetails, MediaType.APPLICATION_XML),ItemDetails.class);       	 
    }	
}
