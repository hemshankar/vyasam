package com.vyasam.scheduler.rest.client;

import com.vyasam.scheduler.constants.SchedulerConstants;
import com.vyasam.scheduler.utility.SchedulerUtility;

public class PersistURLs {

	public static String FETCH_NEW_JOBS = SchedulerUtility.getConfig(SchedulerConstants.VSERVER_HOME)
										+ SchedulerUtility.getConfig(SchedulerConstants.RAW_QUERY);
			//"http://localhost:8080/vServer/persist/ItemDetailsPM/fetch/itemDetails/raw_query";
	public static String FETCH_NEW_JOBS_FOR_FIRST_TIME = SchedulerUtility.getConfig(SchedulerConstants.VSERVER_HOME)
										 + SchedulerUtility.getConfig(SchedulerConstants.RAW_QUERY);
	public static String SAVE_ITEM_DETAILS = SchedulerUtility.getConfig(SchedulerConstants.VSERVER_HOME)
							 + SchedulerUtility.getConfig(SchedulerConstants.SAVE_ITEM_DETAILS);
}
