package com.vyasam.scheduler.rest.server;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.vyasam.client.models.HTMLFetchTask;
import com.vyasam.common_utilities.CommonUtilities;
import com.vyasam.constants.FetchConstants;
import com.vyasam.scheduler.utility.SchedulerUtility;

@Path("scheduler")
public class Scheduler {

    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        return "Got it!";
    }
    
    @GET
    @Path("getTask")
    @Produces(MediaType.APPLICATION_XML)
    public HTMLFetchTask getTask()
    {
		try {
			return SchedulerUtility.getScheduler().getNextTask();
		} catch (Exception e) {
			e.printStackTrace();
		}    
		return null;
    }
    
    @POST
    @Path("updateHtmlContent")
    @Consumes({MediaType.APPLICATION_XML})
    public void updateHtmlContent(HTMLFetchTask task) throws Exception
    {
    	task.getMore().put(FetchConstants.FETCH_END_TIME, CommonUtilities.getCurrentTime());
    	SchedulerUtility.getScheduler().saveTask(task);
    }
}
