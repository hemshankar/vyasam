package com.vyasam.scheduler.utility;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import com.vyasam.scheduler.manager.ScheduleManager;
import com.vyasam.scheduler.rest.client.SchedulerPersistManager;

public class SchedulerUtility {

	private static ScheduleManager scheduler = new ScheduleManager();
	private static SchedulerPersistManager schedulerPersistmanager = new SchedulerPersistManager();
	private static PropertyValues propValues = new PropertyValues();
	
	public static ScheduleManager getScheduler()
	{
		return scheduler;
	}
	
	public static SchedulerPersistManager getPersistManager()
	{
		return schedulerPersistmanager;
	}
	public static class PropertyValues {
		String result = "";
		InputStream inputStream;
		Properties prop = null;
		
		public void populateProps() throws IOException{
			try{
				String propFileName = "config.properties";
				prop = new Properties(); 
				inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
	 
				if (inputStream != null) {
					prop.load(inputStream);
				} else {
					throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
				}
			}catch(Exception e){
				e.printStackTrace();
			}finally {
				inputStream.close();
			}
		}
		
		public PropertyValues(){			
			if(prop == null){
				try {
					populateProps();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		public String getPropValues(String key) {
 			try {
				result = prop.getProperty(key);
			} catch (Exception e) {
				System.out.println("Exception: " + e);
			} 
			return result;
		}
	}
	
	public static String getConfig(String key){
		//return config.get(key);
		return propValues.getPropValues(key);
	}
}
