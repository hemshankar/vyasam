package com.vyasam.scheduler.constants;

public class SchedulerConstants {
	public final static String VSERVER_HOSTNAME = "VSERVER_HOSTNAME";
	public final static String VSERVER_PORT = "VSERVER_PORT";
	public final static String VSERVER_HOME = "VSERVER_HOME";

	public final static String RAW_QUERY = "RAW_QUERY";	
	public final static String SAVE_ITEM_DETAILS = "SAVE_ITEM_DETAILS";
//	
//	String FETCH_NEW_JOBS = "http://localhost:8080/vServer/persist/ItemDetailsPM/fetch/itemDetails/raw_query";
//	String FETCH_NEW_JOBS_FOR_FIRST_TIME = "http://localhost:8080/vServer/persist/ItemDetailsPM/fetch/itemDetails/raw_query";
//	String SAVE_ITEM_DETAILS = "http://localhost:8080/vServer/persist/ItemDetailsPM/save_update/itemsDetails";
}
