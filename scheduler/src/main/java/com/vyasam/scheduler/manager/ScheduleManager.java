package com.vyasam.scheduler.manager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeSet;

import javax.transaction.Synchronization;

import com.vyasam.client.models.HTMLFetchTask;
import com.vyasam.common_utilities.CommonUtilities;
import com.vyasam.constants.FetchConstants;
import com.vyasam.constants.FetchStatus;
import com.vyasam.constants.TaskType;
import com.vyasam.extractor.ExtractionManager;
import com.vyasam.jsoup.models.exceptions.VyasamModelsException;
import com.vyasam.models.ItemDetails;
import com.vyasam.scheduler.rest.client.SchedulerPersistManager;
import com.vyasam.utility.logger.Logger;

public class ScheduleManager {

	private ExtractionManager extractionManager = new ExtractionManager();
	private SchedulerPersistManager schedulerPersistManager = new SchedulerPersistManager();

	private List<ItemDetails> newTaskQueue = new ArrayList<ItemDetails>();
	private Map<Long, String> beingFetchedQueue = new LinkedHashMap<>();
	private Map<String, ItemDetails> fetchingTaskQueue = new LinkedHashMap<String, ItemDetails>();

	private Map<String, ItemFetcher> itemSources = new LinkedHashMap<String, ItemFetcher>();
	private Timer fetchScheduler = new Timer();
	private Timer fetchedStatusTimer = new Timer();
	private Timer fetchStatusLogTimer = new Timer();
	private boolean CRASH_RECOVERED = false;
	private boolean LOOGING_ON = false;
	private long lastFetchTime = 0;

	class PeriodicTaskFetcher extends TimerTask {
		@Override
		public void run() {
			try {
				if(newTaskQueue.size() + fetchingTaskQueue.size() < 40){
					fetchNewTasks();				
				}
				lastFetchTime = System.currentTimeMillis();
				if(newTaskQueue.size()>0 || fetchingTaskQueue.size()>0){
					fetchScheduler.schedule(new PeriodicTaskFetcher(),2 * 60 * 1000);
				} else{
					LOOGING_ON = false;
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("==================================Error in PeriodicTaskFetcher");
			}
			System.out.println("===================Scheduled and returned==============");
		}
	}

	class StatusCheckerTask extends TimerTask {
		@Override
		public void run() {
			synchronized ("QUEUE_OPERATION") {
				try {
					Set<Long> keySet = new TreeSet<Long>();
					keySet.addAll(beingFetchedQueue.keySet());
					for(Long key: keySet) {						
						if (System.currentTimeMillis() - key > 10000) {
							ItemDetails iDetails = fetchingTaskQueue
									.remove(beingFetchedQueue.get(key));
							beingFetchedQueue.remove(key);
							iDetails.setStatus(FetchStatus.FAILED);
							schedulerPersistManager.saveItemDetails(iDetails);
							System.out.println("Fetch timed out for ItemId: " + iDetails.getId() + ". Removing from pending tasks.");							
						}
					}
					if(newTaskQueue.size()>0 || fetchingTaskQueue.size()>0) {
						fetchedStatusTimer.schedule(new StatusCheckerTask(), 10000);
					} else{
						LOOGING_ON = false;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	class FetchStatusLogger extends TimerTask{

		@Override
		public void run() {
			System.out.println("========Fetching: " + fetchingTaskQueue.size() + "; Pending: " + newTaskQueue.size() + "===========");
			if(newTaskQueue.size()>0 || fetchingTaskQueue.size()>0) {
				fetchStatusLogTimer.schedule(new FetchStatusLogger(), 2000);
			} else{
				LOOGING_ON = false;
			}
		}
		
	}
	
	public void addTask(ItemDetails task) {
		synchronized ("QUEUE_OPERATION") {			
			newTaskQueue.add(task);
			System.out.println("Added new task: " + task.getId() + ".");
		}
	}

	public ScheduleManager() {		
	}

	public void fetchNewTasks() throws Exception {		
			
			List<ItemDetails> tasks = null; 
					if(!CRASH_RECOVERED) //considers all the Queued and Fetching jobs along with New jobs
					{
						tasks = schedulerPersistManager.fetchNewJobsForFirstTime(); 
						CRASH_RECOVERED = true;
						if(tasks == null || tasks.size()< 1)
							tasks = schedulerPersistManager.fetchNewJobs(); 
					}
					else //considers only new jobs
						tasks = schedulerPersistManager.fetchNewJobs(); 
			for (ItemDetails task : tasks) {
				ItemFetcher fetcher = itemSources.get(task.getSourceName());
				if (fetcher == null) {
					fetcher = new ItemFetcher(this, task.getSourceName());
					itemSources.put(task.getSourceName(), fetcher);
				}
				task.generateFromXML();
				task.setStatus(FetchStatus.QUEUED);
				schedulerPersistManager.saveItemDetails(task);
				fetcher.addTask(task);
			}
			lastFetchTime = System.currentTimeMillis();
	}

	public HTMLFetchTask getNextTask() throws Exception {
		synchronized ("QUEUE_OPERATION") {
			HTMLFetchTask task = new HTMLFetchTask();
			try {
				// fetch the list of new entries
				if (newTaskQueue == null || newTaskQueue.isEmpty()) {
					if ((System.currentTimeMillis() - lastFetchTime > 60000)
							&& (itemSources.isEmpty() || itemSources.size() < 2)) {
						fetchNewTasks();
						Map.Entry<String, ItemFetcher> src1 = itemSources
								.entrySet().iterator().next();
						src1.getValue().forceFetchTask();
						lastFetchTime = System.currentTimeMillis();
					}
				}

				// there are no jobs even after fetching from database return
				// NOOP
				if (newTaskQueue == null || newTaskQueue.isEmpty()) {
					task.setTaskType(TaskType.NOTING);
					return task;
				}

				// update the status to fetching
				ItemDetails newItem = newTaskQueue.remove(0);
				newItem.setStatus(FetchStatus.FETCHING);
				fetchingTaskQueue.put(newItem.getId().toString(), newItem);
				beingFetchedQueue.put(System.currentTimeMillis(), newItem
						.getId().toString());
				schedulerPersistManager.saveItemDetails(newItem);

				// create task for item
				task.setId(newItem.getId().toString());
				task.setUrl(newItem.getSourceLink());
				task.setTaskType(TaskType.GET_ITEM_HTML_CONTENT);
				task.getMore().put(FetchConstants.FETCH_START_TIME,CommonUtilities.getCurrentTime());
				System.out.println("Fetching task " + task.getId());
				if(!LOOGING_ON)
				{
					fetchScheduler.schedule(new PeriodicTaskFetcher(), 2 * 60 * 1000);
					fetchedStatusTimer.schedule(new StatusCheckerTask(), 10 * 1000);
					fetchStatusLogTimer.schedule(new FetchStatusLogger(), 2000);
					LOOGING_ON = true;
				}
				
			} catch (Exception e) {
				Logger.log(
						"error",
						"error while fetching new item list: "
								+ e.getLocalizedMessage());
				e.printStackTrace();
				throw new Exception(e.getMessage());
			}
			return task;
		}
	}

	public void saveTask(HTMLFetchTask taskDetails) throws Exception {
		synchronized ("QUEUE_OPERATION") {
			String key = taskDetails.getId();
			ItemDetails itemDetails = fetchingTaskQueue.get(key);
			try {
				// update the item details
				itemDetails.setHtmlContent(taskDetails.getHtmlContent());
				itemDetails.getMoreValues().put(
						FetchConstants.FETCH_START_TIME,
						taskDetails.getMore().get(
								FetchConstants.FETCH_START_TIME));
				itemDetails.getMoreValues().put(
						FetchConstants.FETCH_END_TIME,
						taskDetails.getMore()
								.get(FetchConstants.FETCH_END_TIME));
				itemDetails.generateFromXML();
				itemDetails.setItemFeatures(extractionManager
						.fetchItemDetailsProvidedHTMLContent(itemDetails));
				itemDetails.generateXMLs();
				itemDetails.setStatus(FetchStatus.FETCHED);
				itemDetails.setFetchedTime(System.currentTimeMillis() + "");
				// save the code
				schedulerPersistManager.saveItemDetails(itemDetails);
				System.out.println("Fetch for Item id: " + itemDetails.getId() + " SUCCESSFUL.");				
			} catch (Exception e) {
				itemDetails.setStatus(FetchStatus.FAILED);
				schedulerPersistManager.saveItemDetails(itemDetails);
				throw new VyasamModelsException("Fetching of features failed for item id: " + itemDetails.getId());
			}
			finally {
				fetchingTaskQueue.remove(key);
				for (Long time : beingFetchedQueue.keySet()) {
					if (beingFetchedQueue.get(time).equals(key)) {
						beingFetchedQueue.remove(time);
						break;
					}
				}
				System.out.println("Removed item id: " + itemDetails.getId() + " from fetch list.");
			}
		}
	}

	public Map<String, ItemFetcher> getItemSources() {
		return itemSources;
	}

	public void setItemSources(Map<String, ItemFetcher> itemSources) {
		this.itemSources = itemSources;
	}

	protected void stopFetching() throws Exception {		
		synchronized ("QUEUE_OPERATION") {
			for (String key : fetchingTaskQueue.keySet()) {
				ItemDetails itemDetails = fetchingTaskQueue.get(key);
				itemDetails.setStatus(FetchStatus.NEW);
				schedulerPersistManager.saveItemDetails(itemDetails);
			}
			fetchingTaskQueue.clear();
			beingFetchedQueue.clear();		
			
			for(String key: itemSources.keySet())
			{
				itemSources.get(key).saveRemainigTaskAsNew(schedulerPersistManager);
			}
		}
	}
}
