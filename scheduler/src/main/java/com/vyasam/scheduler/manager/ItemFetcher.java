package com.vyasam.scheduler.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import com.vyasam.constants.FetchStatus;
import com.vyasam.models.ItemDetails;
import com.vyasam.scheduler.rest.client.SchedulerPersistManager;

public class ItemFetcher {

	private ScheduleManager manager = null;
	List<ItemDetails> jobList = new ArrayList<ItemDetails>();
	Random rand = new Random(32452342);

	private Timer timer = new Timer();
	private String sourceName = "";

	public void addTask(ItemDetails task) {
		synchronized (this) {
			if (!jobList.contains(task)) {
				jobList.add(task);
				/*System.out.println("Added new task to : " + sourceName
													+ ". Total tasks: " + jobList.size());*/
			}
		}
	}

	public void forceFetchTask() {
		synchronized (this) {
			if (jobList.size() > 0) {
				System.out.println(sourceName + ": Adding task id: " + jobList.get(0).getId());
				manager.addTask(jobList.remove(0));
				/*System.out.println(sourceName + ": Removed task from source: " + sourceName
						+ ". Total remaining task: " + jobList.size());*/
			}
		}
	}

	public ItemFetcher(ScheduleManager sm, String srcName) {
		manager = sm;
		sourceName = srcName;
		timer.schedule(new TaskAdder(), 3000);
		//System.out.println("Created new fetch for source " + srcName);
	}

	class TaskAdder extends TimerTask {
		@Override
		public void run() {
			if (jobList.size() > 0) {
				synchronized (this) {
					//System.out.println(sourceName + ": Adding task id: " + jobList.get(0).getId());
					manager.addTask(jobList.remove(rand.nextInt(jobList.size())));
					/*System.out.println(sourceName + ": Removed task from source: "
							+ sourceName + ". Total remaining task: "
							+ jobList.size());*/
				}
				if (jobList.size() == 0) {
					manager.getItemSources().remove(sourceName);
					return;
				}
			}
			Integer randNo = rand.nextInt(5);
			timer.schedule(new TaskAdder(), (5 + randNo) * 1000);
		}
	}
	
	public void saveRemainigTaskAsNew(SchedulerPersistManager schedulerPersistManager){		
		for(ItemDetails item: jobList)
		{
			item.setStatus(FetchStatus.NEW);
			try {
				schedulerPersistManager.saveItemDetails(item);
			} catch (Exception e) {
				e.printStackTrace();
			}
			jobList.clear();
		}
	}
}
