package scheduler;

import java.io.IOException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.ClientConfig;
import org.jsoup.Jsoup;

import com.vyasam.client.models.HTMLFetchTask;
import com.vyasam.models.ItemDetails;

public class TestScheduler {

	HTMLFetchTask fetchTask = null;
	
	public void getTask()
	{
		String URL2 = "http://localhost:8080/scheduler/vyasam/scheduler/getTask"; 
				//"http://192.168.91.138:8080/scheduler/vyasam/scheduler/getTask";
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL2);		
		fetchTask = target.request(MediaType.APPLICATION_XML).get(HTMLFetchTask.class);
		try {
			try{Thread.sleep(3000);}catch(Exception e){}
			updateItemDetails();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(fetchTask.getId());		
	}
	
	public void updateItemDetails() throws IOException
	{
		fetchTask.setHtmlContent(Jsoup.connect(fetchTask.getUrl()).get().html());
		
		String URL2 = "http://localhost:8080/scheduler/vyasam/scheduler/updateHtmlContent";
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL2);
		
		target.request().
					post(Entity.entity(fetchTask, MediaType.APPLICATION_XML));
		
	}
	
	public static void main(String [] args)
	{
		for(int i=0;i<50;i++){
			try{Thread.sleep(5000);}catch(Exception e){}
		TestScheduler t = new TestScheduler();
		try{Thread.sleep(3000);}catch(Exception e){}
		t.getTask();
		}
	}
}
