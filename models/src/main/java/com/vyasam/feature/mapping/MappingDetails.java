package com.vyasam.feature.mapping;


import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
@Entity
public class MappingDetails implements Serializable{

	
	private static final long serialVersionUID = 1L;
	@Id @GeneratedValue
	private Integer mappingId;
	private String mappingName = "Un_Named_Mapping";
	
	
	@ElementCollection(fetch = FetchType.EAGER)
	private Map<String,String> columnMapping = new HashMap<String, String>();
	private String CategoryId;
	
	public String getMappingName() {
		return mappingName;
	}
	public void setMappingName(String mappingId) {
		this.mappingName = mappingId;
	}
	public Map<String, String> getColumnMapping() {
		return columnMapping;
	}
	public void setColumnMapping(Map<String, String> columnMapping) {
		this.columnMapping = columnMapping;
	}
	public String getCategoryId() {
		return CategoryId;
	}
	public void setCategoryId(String categoryId) {
		CategoryId = categoryId;
	}
	public Integer getMappingId() {
		return mappingId;
	}
	public void setMappingId(Integer mappingId) {
		this.mappingId = mappingId;
	}
}
