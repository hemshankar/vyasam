package com.vyasam.classSpecific.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DefaultFeature implements Serializable{

	Map<String,DefaultValue> values = new HashMap<String,DefaultValue>();
	Integer defaultPriority = 0;
	String name = "";
	String targetFeature = "";
	Boolean relevent = true;
	String groupName = "";

	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public Boolean getRelevent() {
		return relevent;
	}
	public void setRelevent(Boolean relevent) {
		this.relevent = relevent;
	}
	public Map<String,DefaultValue> getValues() {
		return values;
	}
	public void setValues(Map<String,DefaultValue> values) {
		this.values = values;
	}
	public Integer getDefaultPriority() {
		return defaultPriority;
	}
	public void setDefaultPriority(Integer defaultPriofity) {
		this.defaultPriority = defaultPriofity;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getTargetFeature() {
		return targetFeature;
	}
	public void setTargetFeature(String targetFeature) {
		this.targetFeature = targetFeature;
	}
}
