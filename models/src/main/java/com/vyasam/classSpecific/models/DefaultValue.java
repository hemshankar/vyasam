package com.vyasam.classSpecific.models;

import java.io.Serializable;

public class DefaultValue implements Serializable{

	Integer defaultPriofity = 0;
	String name = "";

	public Integer getDefaultPriofity() {
		return defaultPriofity;
	}

	public void setDefaultPriofity(Integer defaultPriofity) {
		this.defaultPriofity = defaultPriofity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
