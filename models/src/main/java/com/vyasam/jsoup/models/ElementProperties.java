package com.vyasam.jsoup.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

@XmlRootElement
@XmlSeeAlso(ElementProperty.class)
public class ElementProperties implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8580821162045186535L;
	
	private List<ElementProperty> properties = null;
	private Integer depth = 0;
	
	public static ElementProperties newElementProperties()
	{
		return new ElementProperties();
	}
	
	public ElementProperties updateProperties()
	{
		for(ElementProperty props:properties)
		{
			props.updateFromList(null);
		}
		return this;
	}
	
	public List<ElementProperty> getProperties() {
		return properties;
	}
	public ElementProperties setProperties(List<ElementProperty> properties) {
		this.properties = properties;
		return this;
	}
	public Integer getDepth() {
		return depth;
	}
	public ElementProperties setDepth(Integer depth) {
		this.depth = depth;
		return this;
	}
	
	public ElementProperties addProperty(ElementProperty prop)
	{
		properties.add(prop);
		depth++;
		return this;
	}
	public ElementProperties()
	{
		properties = new ArrayList<ElementProperty>();
	}
	public ElementProperties clone()
	{
		ElementProperties props = new ElementProperties();
		props.setDepth(depth);
		List<ElementProperty> newProps = new ArrayList<ElementProperty>();
		newProps.addAll(properties);
		props.setProperties(newProps);		
		return props;
	}
}
