package com.vyasam.jsoup.models.exceptions;

public class VyasamModelsException extends Exception{

	private static final long serialVersionUID = 5942293239927036742L;
	
	public VyasamModelsException(String msg)
	{
		super(msg);
	}
}
