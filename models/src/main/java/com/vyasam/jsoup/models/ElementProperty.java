package com.vyasam.jsoup.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement

public class ElementProperty implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5838027079757755703L;
		
	private Map<String,String> propMap = new LinkedHashMap<String, String>();

	/*
	 * required for UI
	 */
	private List<Property> propList = new ArrayList<Property>();
	
	public List<Property> getPropList() {
		return propList;
	}

	public void setPropList(List<Property> propList) {
		this.propList = propList;
	}

	public Map<String, String> getPropMap() {
		return propMap;
	}

	public ElementProperty setPropMap(Map<String, String> elementProperty) {
		this.propMap = elementProperty;
		return this;
	}
	
	public ElementProperty add(String key,String value)
	{
		propMap.put(key, value);
		Property prop = new Property();
		prop.setKey(key);
		prop.setValue(value);		
		propList.add(prop);
		return this;
	}
	
	public ElementProperty updateFromList(List<Property> pList)
	{
		if(pList != null)
		{
			propList = pList;
		}
		
		propMap.clear();
		
		for(Property p : propList)
		{
			propMap.put(p.getKey(), p.getValue());
		}
		
		return this;
	}
}
