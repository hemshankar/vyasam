/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vyasam.jsoup.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 *
 * @author hsahu
 */

@XmlRootElement
@XmlSeeAlso(ElementProperties.class)
public class DetailsExtractionSteps implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -6160282739028832379L;
	ElementProperties rootProps = null;
	ElementProperties keyProps = null; 
	ElementProperties valueProps = null; 
    
    public ElementProperties getRootProps() {
        return rootProps;
    }

    public void setRootProps(ElementProperties rootProps) {
        this.rootProps = rootProps;
    }

    public ElementProperties getKeyProps() {
        return keyProps;
    }

    public void setKeyProps(ElementProperties keyProps) {
        this.keyProps = keyProps;
    }

    public ElementProperties getValueProps() {
        return valueProps;
    }

    public void setValueProps(ElementProperties valueProps) {
        this.valueProps = valueProps;
    }    
}
