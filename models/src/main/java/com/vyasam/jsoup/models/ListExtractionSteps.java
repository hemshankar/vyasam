/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vyasam.jsoup.models;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hsahu
 */
@XmlRootElement
public class ListExtractionSteps implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 5340587955702976033L;
	ElementProperties rootProps = null;
	ElementProperties linkProps = null;   

    public ElementProperties getRootProps() {
        return rootProps;
    }

    public void setRootProps(ElementProperties rootProps) {
        this.rootProps = rootProps;
    }

    public ElementProperties getLinkProps() {
        return linkProps;
    }

    public void setLinkProps(ElementProperties linkProps) {
        this.linkProps = linkProps;
    }
    
}
