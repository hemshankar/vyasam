package com.vyasam.models;

import java.io.Serializable;

public class Feature  implements Serializable{
	
	private static final long serialVersionUID = -4557492087048104413L;
	private String key="";
	private String value = "";
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	public String toString()
	{
		return key + "=" + value + "\n";
	}	
}
