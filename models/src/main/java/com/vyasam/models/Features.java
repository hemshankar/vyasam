/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vyasam.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hsahu
 */
@XmlRootElement
public class Features implements Serializable{
   
	private static final long serialVersionUID = 226531067918087585L;
	private List<Feature> featuresList = new ArrayList<Feature>(); 
	
	public void addFeature(Feature feature)
	{
		featuresList.add(feature);
	}

	public List<Feature> getFeaturesList() {
		return featuresList;
	}

	public void setFeaturesList(List<Feature> featuresList) {
		this.featuresList = featuresList;
	}
			
}
