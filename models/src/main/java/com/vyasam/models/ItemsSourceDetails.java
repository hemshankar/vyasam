/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vyasam.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;

import com.vyasam.jsoup.models.DetailsExtractionSteps;
import com.vyasam.jsoup.models.ListExtractionSteps;
import com.vyasam.models.interfaces.IVyasamModel;
import com.vyasam.utilities.VyasamModelsUtility;
import com.vyasam.utility.models.More;


/**
 *
 * @author hsahu
 */


@XmlRootElement
@XmlSeeAlso(ListExtractionSteps.class)
@Entity
public class ItemsSourceDetails implements Serializable,IVyasamModel {

	private static final long serialVersionUID = -4741489783772413829L;
	
	@Id @GeneratedValue
	private Integer id;
    private String title;
    private String SourceName = "GSM_ARENA";
    private Integer classId;    
	private String sourceLink;
    private String fetchedTime;
    
    //@Lob
    @Transient
    private String htmlContent;
        
    @Transient
    private ListExtractionSteps extractionSteps;
    @Lob
    private String extractionStepsXML;
    
    @OneToMany (cascade=CascadeType.ALL)//,fetch=FetchType.EAGER)//(mappedBy="sourceDetails")
    private List<ItemDetails> itemDetailsList = new ArrayList<ItemDetails>();
    
    @Transient
    @XmlTransient 
    private More moreDetails = null;
    @Lob
    private String moreXML;
    
    public ItemsSourceDetails generateXMLs()
    {
    	try
    	{
    		if(extractionSteps!=null)
    			extractionStepsXML = VyasamModelsUtility.serializationUtility.objectToXML(ListExtractionSteps.class, extractionSteps);
    		if(moreXML!=null)
    			moreXML = VyasamModelsUtility.serializationUtility.objectToXML(More.class, moreDetails);
    		
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	return this;
    }
    
    //+=============================GETTERS & SETTERS===================================
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSourceName() {
		return SourceName;
	}
	public void setSourceName(String sourceName) {
		SourceName = sourceName;
	}
	public String getSourceLink() {
		return sourceLink;
	}
	public void setSourceLink(String sourceLink) {
		this.sourceLink = sourceLink;
	}
	public String getFetchedTime() {
		return fetchedTime;
	}
	public void setFetchedTime(String fetchedTime) {
		this.fetchedTime = fetchedTime;
	}
	public String getHtmlContent() {
		return htmlContent;
	}
	public void setHtmlContent(String htmlContent) {
		this.htmlContent = htmlContent;
	}
	public ListExtractionSteps getExtractionSteps() {
		return extractionSteps;
	}
	public void setExtractionSteps(ListExtractionSteps extractionSteps) {
		this.extractionSteps = extractionSteps;
	}
	public String getExtractionStepsXML() {
		return extractionStepsXML;
	}
	public void setExtractionStepsXML(String extractionStepsXML) {
		this.extractionStepsXML = extractionStepsXML;
	}
	public List<ItemDetails> getItemDetailsList() {
		return itemDetailsList;
	}
	public void setItemDetailsList(List<ItemDetails> itemDetailsList) {
		this.itemDetailsList = itemDetailsList;
	}
	
	public More getMoreDetails() {
		return moreDetails;
	}

	public void setMoreDetails(More moreDetails) {
		this.moreDetails = moreDetails;
	}

	public String getMoreXML() {
		return moreXML;
	}
	public void setMoreXML(String moreXML) {
		this.moreXML = moreXML;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
    public Integer getClassId() {
		return classId;
	}

	public void setClassId(Integer classId) {
		this.classId = classId;
	}
}
