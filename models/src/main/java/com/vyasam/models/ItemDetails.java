package com.vyasam.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;

import com.vyasam.common_utilities.CommonUtilities;
import com.vyasam.jsoup.models.DetailsExtractionSteps;
import com.vyasam.models.interfaces.IVyasamModel;
import com.vyasam.utilities.VyasamModelsUtility;
import com.vyasam.utility.exceptions.CommonUtilitesException;
import com.vyasam.utility.models.More;

@XmlRootElement
@XmlSeeAlso({DetailsExtractionSteps.class,Features.class,Feature.class,More.class})
@Entity
public class ItemDetails implements Serializable,IVyasamModel {
	
	private static final long serialVersionUID = -7322646994749885193L;
	
	@Id @GeneratedValue
	private Integer id;
	private String title;
	
	private String sourceLink;
	private String sourceName;
    private Integer classId;
    private String fetchedTime;
    @Transient
	private Boolean valid = true;

	//@Lob
	@Transient
	private String htmlContent;
	
	@Transient
	private Features itemFeatures;
	@Lob
	private String itemFeaturesXML;
	
	@Transient
	@XmlTransient
	private DetailsExtractionSteps extractionSteps;
	@Lob
	private String extractionStepsXML;
	
	/*@ManyToOne
	@JoinColumn (name="SOURCE_ID")
	private ItemsSourceDetails sourceDetails = null;*/
	
	@Transient
	@XmlTransient
	private More moreValues = new More();	
	@Lob
	private String moreXML;
	
	private String Status = "NEW";	

	public ItemDetails generateXMLs()
    {
    	try
    	{
    		if(extractionSteps!=null)
    			extractionStepsXML = VyasamModelsUtility.serializationUtility.objectToXML(DetailsExtractionSteps.class, extractionSteps);
    		
    		if(moreValues!=null)
    			moreXML = VyasamModelsUtility.serializationUtility.objectToXML(More.class, moreValues);
    		
    		if(itemFeatures!=null)
    			itemFeaturesXML =  VyasamModelsUtility.serializationUtility.objectToXML(Features.class,itemFeatures);
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	return this;
    }
	
	public ItemDetails generateFromXML() throws CommonUtilitesException
	{
		if(!CommonUtilities.isEmptyString(extractionStepsXML))
			extractionSteps = VyasamModelsUtility.serializationUtility.XMLToObjetc(extractionStepsXML, DetailsExtractionSteps.class);
		if(!CommonUtilities.isEmptyString(moreXML))
			moreValues = VyasamModelsUtility.serializationUtility.XMLToObjetc(moreXML,More.class);
		if(!CommonUtilities.isEmptyString(itemFeaturesXML))
			itemFeatures = VyasamModelsUtility.serializationUtility.XMLToObjetc(itemFeaturesXML,Features.class);
		return this;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemDetails other = (ItemDetails) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	//===============================GETTERS and SETTERS=============================== 

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSourceLink() {
		return sourceLink;
	}
	public void setSourceLink(String sourceLink) {
		this.sourceLink = sourceLink;
	}
	public String getSourceName() {
		return sourceName;
	}
	public void setSourceName(String soruceName) {
		this.sourceName = soruceName;
	}
	public String getFetchedTime() {
		return fetchedTime;
	}
	public void setFetchedTime(String fetchedTime) {
		this.fetchedTime = fetchedTime;
	}
	public String getHtmlContent() {
		return htmlContent;
	}
	public void setHtmlContent(String htmlContent) {
		this.htmlContent = htmlContent;
	}
	public Features getItemFeatures() {
		return itemFeatures;
	}
	public void setItemFeatures(Features itemFeatures) {
		this.itemFeatures = itemFeatures;
	}
	public String getItemFeaturesXML() {
		return itemFeaturesXML;
	}
	public void setItemFeaturesXML(String itemFeaturesXML) {
		this.itemFeaturesXML = itemFeaturesXML;
	}
	public DetailsExtractionSteps getExtractionSteps() {
		return extractionSteps;
	}
	public void setExtractionSteps(DetailsExtractionSteps extractionSteps) {
		this.extractionSteps = extractionSteps;
	}
	public String getExtractionStepsXML() {
		return extractionStepsXML;
	}
	public void setExtractionStepsXML(String extractionStepsXML) {
		this.extractionStepsXML = extractionStepsXML;
	}
	/*public ItemsSourceDetails getSourceDetails() {
		return sourceDetails;
	}
	public void setSourceDetails(ItemsSourceDetails sourceDetails) {
		this.sourceDetails = sourceDetails;
	}*/
	public More getMoreValues() {
		return moreValues;
	}
	public void setMoreValues(More more) {
		this.moreValues = more;
	}
	public String getMoreXML() {
		return moreXML;
	}
	public void setMoreXML(String moreXML) {
		this.moreXML = moreXML;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public Integer getClassId() {
		return classId;
	}

	public void setClassId(Integer classId) {
		this.classId = classId;
	}

	public Boolean getValid() {
		return valid;
	}

	public void setValid(Boolean valid) {
		this.valid = valid;
	}
}
