package com.vyasam.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
public class ExtractionSteps implements Serializable{

	@Id @GeneratedValue
	Integer stepsId;
	String name;
	
	@Lob
	String listExtrectionStepsXML;
	
	@Lob
	String detailsExtrectionStepsXML;
	
	
	//===========================Getter and Setter
	public String getListExtrectionStepsXML() {
		return listExtrectionStepsXML;
	}

	public void setListExtrectionStepsXML(String listExtrectionStepsXML) {
		this.listExtrectionStepsXML = listExtrectionStepsXML;
	}

	public String getDetailsExtrectionStepsXML() {
		return detailsExtrectionStepsXML;
	}

	public void setDetailsExtrectionStepsXML(String detailsExtrectionStepsXML) {
		this.detailsExtrectionStepsXML = detailsExtrectionStepsXML;
	}

	public Integer getStepsId() {
		return stepsId;
	}

	public void setStepsId(Integer stepsId) {
		this.stepsId = stepsId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
