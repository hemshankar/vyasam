package com.vyasam.query.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

import com.vyasam.models.interfaces.IVyasamModel;

@XmlRootElement
@Entity
public class Query implements Serializable,IVyasamModel{

	private static final long serialVersionUID = 5307738590628324934L;
	@Id @GeneratedValue
	private Integer id;
	private String query;	
	
	@Column(unique=true)
	private String queryName;
	@ElementCollection
	private List<String> parameters; 
	@ElementCollection
	private List<DataType> parametersTypes;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public String getQueryName() {
		return queryName;
	}
	public void setQueryName(String queryName) {
		this.queryName = queryName;
	}
	public List<String> getParameters() {
		return parameters;
	}
	public void setParameters(List<String> parameters) {
		this.parameters = parameters;
	}
	public List<DataType> getParametersTypes() {
		return parametersTypes;
	}
	public void setParametersTypes(List<DataType> parametersTypes) {
		this.parametersTypes = parametersTypes;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
