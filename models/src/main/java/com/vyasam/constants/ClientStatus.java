package com.vyasam.constants;

public class ClientStatus {	
	public static final String FREE = "free";
	public static final String BUSY = "busy";
	public static final String BLOCKED = "blocked";
}
