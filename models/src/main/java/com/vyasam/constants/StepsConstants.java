package com.vyasam.constants;

import java.io.Serializable;

public class StepsConstants implements Serializable{

	private static final long serialVersionUID = -1794651789653112293L;

	//Jsoup related
	public static final String HAS_ID = "HAS_ID";
	public static final String HAS_CLASS = "HAS_CLASS";	
	public static final String HAS_TAG = "HAS_TAG";
	public static final String HAS_ATTRIBUTE = "HAS_ATTRIBUTE";	
	public static final String HAS_ATTRIBUTE_WITH_VALUE = "HAS_ATTRIBUTE_WITH_VALUE";
	
	//Non Jsoup
	public static final String HAS_NO_TEXT_LIKE = "HAS_NO_TEXT_LIKE";
	public static final String HAS_TEXT_LIKE = "HAS_TEST_LIKE"; 
	public static final String HAS_EMPTY_TEXT = "HAS_EMPTY_TEXT";
	public static final String HAS_NO_EMPTY_TEXT = "HAS_NO_EMPTY_TEXT";
	public static final String CONTAINS_NO_TEXT_LIKE = "CONTAINS_NO_TEXT_LIKE";
	public static final String CONTAINS_TEXT_LIKE = "CONTAINS_TEXT_LIKE";
	
	//spliters
	public static final String ATTRIBUTE_VALUE_SPLLITER = "=";
	
	//othes
	public static final String TRUE = "TRUE";
	public static final String FALSE = "FALSE";
	
	
}
