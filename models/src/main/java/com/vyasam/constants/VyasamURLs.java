package com.vyasam.constants;

import com.vyasam.utilities.VyasamModelsUtility;

public class VyasamURLs {

	public static final String SAVE_VCLASS = VyasamModelsUtility.getConfig(GenericConstants.VSERVER_HOME) 
			+ VyasamModelsUtility.getConfig(GenericConstants.SAVE_VCLASS);
    /*public static String SAVE_VCLASS = "http://" + GenericConstants.VSERVER_HOST + ":"
			+ GenericConstants.VSERVER_PORT
			+ "/vServer/persist/vclassPM/save_update/VClassPersist";*/
    
    public static final String FETCH_ALL_CLASSES_HQL = VyasamModelsUtility.getConfig(GenericConstants.VSERVER_HOME) 
			+ VyasamModelsUtility.getConfig(GenericConstants.FETCH_ALL_CLASSES_HQL);
    /*public static String FETCH_ALL_CLASSES_HQL = "http://" + GenericConstants.VSERVER_HOST + ":"
			+ GenericConstants.VSERVER_PORT
			+ "/vServer/persist/vclassPM/fetch/allClasses/HQL";*/
    
    public static final String DELETE_VCLASS = VyasamModelsUtility.getConfig(GenericConstants.VSERVER_HOME) 
			+ VyasamModelsUtility.getConfig(GenericConstants.DELETE_VCLASS);
    /*public static String DELETE_VCLASS = "http://" + GenericConstants.VSERVER_HOST + ":"
			+ GenericConstants.VSERVER_PORT
			+ "/vServer/persist/vclassPM/delete/VClassPersist";*/
	
}
