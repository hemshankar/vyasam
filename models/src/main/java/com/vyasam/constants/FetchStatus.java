package com.vyasam.constants;

public class FetchStatus {

	public final static String NEW = "NEW"; 
	public final static String FETCHING = "FETCHING";
	public final static String FETCHED = "FETCHED";
	public final static String FAILED = "FAILED";
	public final static String QUEUED = "QUEUED";
	
}
