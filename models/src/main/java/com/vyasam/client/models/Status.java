package com.vyasam.client.models;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Status {

	private String m_status;

	public Status()
	{
		
	}
	
	public Status(String status)
	{
		m_status = status; 
	}
	
	public String getStatus() {
		return m_status;
	}

	public void setStatus(String status) {
		this.m_status = status;
	}
	
}
