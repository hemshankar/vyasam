package com.vyasam.client.models;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import com.vyasam.constants.TaskType;

@XmlRootElement
public class HTMLFetchTask implements Serializable {

	private static final long serialVersionUID = -5710791859097990548L;
	private String id;
	private String url;
	private String htmlContent;
	private String taskType = TaskType.NOTING;
	private Map<String,Object> more = new HashMap<String, Object>();
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getHtmlContent() {
		return htmlContent;
	}
	public void setHtmlContent(String htmlContent) {
		this.htmlContent = htmlContent;
	}
	public Map<String, Object> getMore() {
		return more;
	}
	public void setMore(Map<String, Object> more) {
		this.more = more;
	}
	public String getTaskType() {
		return taskType;
	}
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
}
