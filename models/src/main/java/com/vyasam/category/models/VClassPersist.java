package com.vyasam.category.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

import com.vyasam.models.interfaces.IVyasamModel;

@XmlRootElement
@Entity
public class VClassPersist implements Serializable,IVyasamModel {
	private String className = "";
	
	@Id @GeneratedValue
	private Integer classId;
	private Integer parentId;
	

	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public Integer getClassId() {
		return classId;
	}
	public void setClassId(Integer classId) {
		this.classId = classId;
	}
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	
	public Integer getId(){
		return classId;
	}
}
