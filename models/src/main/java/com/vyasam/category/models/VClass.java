package com.vyasam.category.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.vyasam.jsoup.models.exceptions.VyasamModelsException;
import com.vyasam.models.interfaces.IVyasamModel;
import com.vyasam.utilities.VyasamModelsUtility;

@XmlRootElement
@Entity 
public class VClass implements Serializable,IVyasamModel{

	private static final long serialVersionUID = -8048691887921821113L;

	@Id @GeneratedValue
	private Integer id;
	//root is the class of all the top level classes.
	
	@Transient
	@XmlTransient
	private static Map<Integer,VClass> mapOfRootClasses = new HashMap<Integer,VClass>();	

	private String name;
	
	private Integer parentId=-1;
	
	//@ManyToOne
    //@JoinColumn(name="parent_id")
	@Transient
	@XmlTransient
	private VClass parent;
	
	@Transient
	@XmlTransient
	//@OneToMany(mappedBy="parent",cascade={CascadeType.PERSIST, CascadeType.MERGE},orphanRemoval = true)
	private List<VClass> children = new ArrayList<VClass>();
	
	public static void deleteClass(VClass vc) throws VyasamModelsException
	{
		if(vc==null)
			return;
		
		VClassPersist vPersist = null;
		for(VClass child: vc.getChildren())
		{
			deleteClass(child);			
		}
		
		//clear my entry from database
		vPersist = createVClassPersistFromVClass(vc);
		VyasamModelsUtility.DeleteVClass(vPersist);		
		
		updateClasses();
		
	}
	
	public static VClass addClass(VClass vc,String className) throws VyasamModelsException{
		if(className==null || "".equals(className))
			throw new VyasamModelsException("Class name cannot be empty");
		VClass newClass = new VClass();
		VClassPersist persistClass = new VClassPersist();
		
		newClass.setName(className);
		persistClass.setClassName(className);
		
		if(vc==null)
		{
			newClass.setParentId(-1);
			persistClass.setParentId(-1);
		}
		else
		{					
			newClass.setParentId(vc.getId());
			persistClass.setParentId(vc.getId());
		}
		
		persistClass = VyasamModelsUtility.saveVClass(persistClass);
		newClass.setId(persistClass.getClassId());		
		
		if(vc==null)
		{
			mapOfRootClasses.put(newClass.getId(), newClass);			
		}
		else
		{
			vc.getChildren().add(newClass);
			newClass.setParent(vc);
		}
		
		return newClass;
	}
	
	public static void updateClasses() throws VyasamModelsException
	{
		/*
		 * 1. fetch all the class entries from the table.
		 * 2. Form the hierarchical tree from those entries
		 */
		Map<Integer, List<VClass>> classParentMap = new HashMap<Integer, List<VClass>>();
		List<VClassPersist> vClassListPersist = VyasamModelsUtility.fetchAllClasses();
		
		for(VClassPersist c: vClassListPersist)
		{
			List<VClass> classList = classParentMap.get(c.getParentId());
			VClass classObj = createVClassFromVClassPersist(c);
			if(classList==null)
			{
				classList = new ArrayList<VClass>();
				classParentMap.put(c.getParentId(), classList);
			}
			classList.add(classObj);		
		}
		
		for(VClass c: classParentMap.get(-1))
		{
			mapOfRootClasses.put(c.getId(), c);
			updateChildren(c,classParentMap);			
		}
	}
	
	public static VClass createVClassFromVClassPersist(VClassPersist vPersist) throws VyasamModelsException
	{
		if(vPersist==null)
			throw new VyasamModelsException("Argument cannot be null: ");
		VClass vClass = new VClass();
		
		vClass.setId(vPersist.getClassId());
		vClass.setName(vPersist.getClassName());
		vClass.setParentId(vPersist.getParentId());
		return vClass;
	}
	
	public static VClassPersist createVClassPersistFromVClass(VClass vClass) throws VyasamModelsException
	{
		if(vClass==null)
			throw new VyasamModelsException("Argument cannot be null: ");
		VClassPersist vPersist = new VClassPersist();
		
		vPersist.setClassId(vClass.getId());
		vPersist.setClassName(vClass.getName());
		vPersist.setParentId(vClass.getParentId());
		return vPersist;
	}
	
	private static void updateChildren(VClass c,Map<Integer, List<VClass>> classParentMap)
	{
		List<VClass> childList = classParentMap.get(c.getId());
		if(childList !=null)
		{
			c.setChildren(childList);
			for(VClass childClass:childList)
			{
				childClass.setParent(c);
				updateChildren(childClass,classParentMap);
			}
		}
	}
	
	
	public static void updateParents(VClass vClass)
	{		
		for(VClass child:  vClass.getChildren())
		{
			child.setParent(vClass);
			updateParents(child);
		}
	}	
	
	public static void detachParents()
	{		
		for(VClass rootElements:  mapOfRootClasses.values())
		{
			detachParentsOfEachRoot(rootElements);
		}
	}	
	
	private static void detachParentsOfEachRoot(VClass vClass)
	{		
		for(VClass child:  vClass.getChildren())
		{
			child.setParent(null);
			detachParentsOfEachRoot(child);
		}
	}	
	public static void printClassHeirarchy(VClass vClass, Integer level)
	{
		String print = "";
		for(int i=0;i<level;i++)
		{
			print = print + "\t";
		}
		print = print + vClass.getName();
		
		System.out.println(print);
		
		for(VClass v: vClass.getChildren())
		{
			printClassHeirarchy(v,level+1);
		}
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public VClass getParent() {
		return parent;
	}
	public void setParent(VClass parent) {
		this.parent = parent;
	}
	public List<VClass> getChildren() {
		return children;
	}
	public void setChildren(List<VClass> children) {
		this.children = children;
	}	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Map<Integer,VClass> getListOfRootClasses() {
		return mapOfRootClasses;
	}

	public void setListOfRootClasses(Map<Integer,VClass> listOfRootClasses) {
		this.mapOfRootClasses = listOfRootClasses;
	}
}
