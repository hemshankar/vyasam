package com.vyasam.utilities;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.ClientConfig;

import com.common.exceptions.UtilityException;
import com.common.utilities.Utility;
import com.vyasam.category.models.VClassPersist;
import com.vyasam.classSpecific.models.DefaultFeature;
import com.vyasam.common.serialization.SerializationUtility;
import com.vyasam.constants.GenericConstants;
import com.vyasam.constants.VyasamURLs;

public class VyasamModelsUtility implements Serializable {

	public static String configFileLinux = "/usr/share/tomcat/webapps/vyasamUI/config.txt";
	private static String configFileWindows = "C:/Users/hsahu/cleanview/cleanview_ui/config.txt";
	private static PropertyValues propValues = new PropertyValues();
	
	static {
		
	}

	public static class PropertyValues {
		String result = "";
		InputStream inputStream;
		Properties prop = null;
		
		public void populateProps() throws IOException{
			try{
				String propFileName = "config.properties";
				prop = new Properties(); 
				inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
	 
				if (inputStream != null) {
					prop.load(inputStream);
				} else {
					throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
				}
			}catch(Exception e){
				e.printStackTrace();
			}finally {
				inputStream.close();
			}
		}
		
		public PropertyValues(){			
			if(prop == null){
				try {
					populateProps();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		public String getPropValues(String key) {
 			try {
				result = prop.getProperty(key);
			} catch (Exception e) {
				System.out.println("Exception: " + e);
			} 
			return result;
		}
	}
	
	public static String getConfig(String key) {
		return propValues.getPropValues(key);
	}

	public static SerializationUtility serializationUtility = new SerializationUtility();
	public static Integer THRESHOLD = 10;

	public static VClassPersist saveVClass(VClassPersist vClassPersist) {

		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(VyasamURLs.SAVE_VCLASS);

		// VClass.detachParents();
		vClassPersist = target.request(MediaType.APPLICATION_XML).post(
				Entity.entity(vClassPersist, MediaType.APPLICATION_XML),
				VClassPersist.class);
		return vClassPersist;
	}

	public static Map<String, Group> groupsMap = null;

	public static List<VClassPersist> fetchAllClasses() {

		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(VyasamURLs.FETCH_ALL_CLASSES_HQL);

		List<VClassPersist> list = target
				.queryParam("HQL", "from " + VClassPersist.class.getName())
				.request(MediaType.APPLICATION_XML)
				.get(new GenericType<List<VClassPersist>>() {
				});

		return list;
	}

	public static void DeleteVClass(VClassPersist vPersist) {

		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(VyasamURLs.DELETE_VCLASS);

		// VClass.detachParents();
		target.request(MediaType.APPLICATION_XML).post(
				Entity.entity(vPersist, MediaType.APPLICATION_XML));
	}

	public static void groupFeaturesWithSimilarValues(
			Map<String, DefaultFeature> map) {
		groupsMap = new HashMap<String, Group>();

		Integer matchedValues = 0;
		Integer maxMatchedValues = 0;
		String maxMatchGroupId = "";
		for (String featureName : map.keySet()) {
			maxMatchedValues = 0;
			if (groupsMap.size() == 0) {
				groupsMap.put(featureName, new Group(map.get(featureName)));
				map.get(featureName).setGroupName(featureName);
			} else {
				for (String groupId : groupsMap.keySet()) {
					matchedValues = groupsMap.get(groupId).matchValues(
							map.get(featureName));
					if (matchedValues > maxMatchedValues) {
						maxMatchGroupId = groupId;
						maxMatchedValues = matchedValues;
					}
				}

				if (maxMatchedValues > THRESHOLD) {
					groupsMap.get(maxMatchGroupId).addFeature(
							map.get(featureName));
					map.get(featureName).setGroupName(maxMatchGroupId);
				} else {
					groupsMap.put(featureName, new Group(map.get(featureName)));
					map.get(featureName).setGroupName(featureName);
				}
			}
		}
	}
}
