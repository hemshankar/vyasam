package com.vyasam.utilities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vyasam.classSpecific.models.DefaultFeature;
import com.vyasam.classSpecific.models.DefaultValue;

public class Group implements Serializable{
	public Map<String, Boolean> values = new HashMap<String, Boolean>();
	public String groupId;
	
	static final String [] stopWords = {"is"," ",",",";","the"};

	public Group() {
	}

	public String valuesToString()
	{
		String result = "";
		for(String str:values.keySet())
		{
			result = result + "," + str;
		}
		return result;
	}
	
	public  boolean isStopWord(String str){
				
		for(int i=0;i<stopWords.length;i++)
		{
			if(str.equalsIgnoreCase(stopWords[i]))
				return true;
		}
		return false;
	}
	
	public  List<String> splitUsingNormalDelimiter(List<String> strList,String delimiter)
	{
		List<String> result = new ArrayList<String>();
		
		for(String s: strList)
		{
			String [] strArr = s.split(delimiter);
			for(String finalValue: strArr)
			{
				result.add(finalValue);
			}
		}
		
		return result;
	}
	
	public  List<String> splitUsingNormalDelimiters(String str)
	{
		List<String> input = new ArrayList<String>();
		input.add(str);
		List<String> result = splitUsingNormalDelimiter(input," ");
		result = splitUsingNormalDelimiter(result,",");
		result = splitUsingNormalDelimiter(result,";");
		return result;
	}
	
	public Group(DefaultFeature feature) {
		addFeature(feature);
	}
	
	public void addFeature(DefaultFeature feature){
		try {
			for (DefaultValue dValue : feature.getValues().values()) {
				List<String> valueStr = splitUsingNormalDelimiters(dValue.getName());
				for(String value: valueStr)
				{
					if(!isStopWord(value))
						values.put(value, true);
				}						
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Integer matchValues(DefaultFeature feature)
	{
		Integer matchedValues=0;
		try {								
				List<String> valueStr = getUniqueValues(feature);
				for(String value: valueStr)
				{
					if(!isStopWord(value))
					{
						if(values.get(value)!=null)
							matchedValues++;
					}
				}						
							
		} catch (Exception e) {
			e.printStackTrace();
		}
		return matchedValues;
	}
	
	public  List<String> getUniqueValues(DefaultFeature feature)
	{
		Map<String,Boolean> valuesMap = new HashMap<String, Boolean>(); 
	
		for (DefaultValue dValue : feature.getValues().values()) {
			List<String> valueStr = splitUsingNormalDelimiters(dValue.getName());
			for(String str : valueStr){
				if(!isStopWord(str))
				valuesMap.put(str, true);
			}
		}
		
		List<String> result = new ArrayList<String>();			
		result.addAll(valuesMap.keySet());
		return result;
	}
}