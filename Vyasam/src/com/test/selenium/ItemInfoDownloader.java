package com.test.selenium;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
//
//import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.firefox.FirefoxDriver;

public class ItemInfoDownloader {
	
	/*public static void main(String [] args)
	{
		String url = "http://www.flipkart.com/mobiles/pr?p%5B%5D=facets.brand%255B%255D%3DLenovo&sid=tyy%2C4io&pincode=560057&filterNone=true";
		ItemInfoDownloader downloader = new ItemInfoDownloader(url);
		try
		{
			System.out.println(downloader.fetchLinkList());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		downloader.done();
	}
	
	public static String START_URL="";
	
	private WebDriver driver = null;
	private Random rand = new Random(3000);
	public ItemInfoDownloader(String url)
	{
		START_URL = url;		
		driver = new FirefoxDriver();
		driver.get(START_URL);
	}
	
	public void setStartURL(String startUrl)
	{
		START_URL = startUrl;
	}
	
	public void startFetchingItems()
	{
		List<String> itemLinkList = fetchLinkList();
		for(String link: itemLinkList)
		{
			waitFor(3000);
			downLoadItemFeature(link);
		}
	}
	
	public List<String>	fetchLinkList()
	{
		List<String> linksList = new ArrayList<String>();
		waitFor(3000);
		WebElement select = driver.findElement(By.id("products"));
		List<WebElement> allLinks = select.findElements(By.tagName("a"));
		for (WebElement link : allLinks) {
			if(!"".equals(link.getText().trim()))
			{	
				try
				{
					waitFor(getNextRandNumberForPageLoad());
					
					WebElement elem =  allLinks.get(getNextRandNumberForPageSelect());
					
				    System.out.println(elem.getText());
				    
				    String link_url = elem.getAttribute("href");			    
				    
				    downLoadItemFeature(link_url);
				}
				catch(Exception e)
				{
					System.out.println("Error: "  + e.getMessage());
				}
			}
		}
		return linksList;
	}
	
	public void downLoadItemFeature(String link)
	{
		driver.get(link);
		waitFor(getNextRandNumberForPageLoad());
		WebElement specDetails = driver.findElement(By.className("productSpecs"));
		
		List<WebElement> specKey = specDetails.findElements(By.className("specsKey"));
		List<WebElement> specValue = specDetails.findElements(By.className("specsValue"));
		
		Map<String,String> specs = new HashMap<>();
		
		for(int i=0;i<specKey.size();i++)
		{
			specs.put(specKey.get(i).getText(), specValue.get(i).getText());
		}
		
		System.out.println(specs);
	}
	
	public void waitFor(long time)
	{
		try
		{
			Thread.sleep(time);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void done()
	{
		try{
			driver.quit();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public int getNextRandNumberForPageLoad()
	{
		return 2000 + rand.nextInt(3000);
	}
	
	public int getNextRandNumberForPageSelect()
	{
		return rand.nextInt(50);
	}*/
}
