package com.vyasam.extractor;

import java.io.Serializable;



import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.vyasam.jsoup.models.ElementProperties;
import com.vyasam.models.Feature;
import com.vyasam.models.Features;
import com.vyasam.utility.Utility;

public class ItemDetailsExtractor implements Serializable{

	private static final long serialVersionUID = 5962652519339523125L;

	public Features extractItmes(Element doc,
			ElementProperties rootIdentifier,
			ElementProperties keyIdentifier,
			ElementProperties valueIdentifier) {

		Features features = new Features();
		try {
			Element rootElement = Utility.getElement(doc, rootIdentifier);// doc.getElementById("specs-list");
			if (rootElement != null) {
				Elements elems = rootElement.getAllElements();				
				Element keyElem = null;
				Element valueElem = null;
				Feature feature = null;
				
				for (Element e : elems) {					
					if ((keyElem = Utility.getElement(e, keyIdentifier))!=null) {
						if (feature != null 
								&& !Utility.isEmptyString(feature.getKey())//!=null 
								&& !Utility.isEmptyString(feature.getValue()))// !=null )
							features.addFeature(feature);
						feature = new Feature();
						String key = keyElem.text();
						if (!Utility.isEmptyString(key))
						{
							key = keyElem.text();
							feature.setKey(key);
						}
												
					} else if ((valueElem = Utility.getElement(e, valueIdentifier))!=null) {
						if (feature != null) {
							String value = valueElem.text();
							if (!Utility.isEmptyString(value))
							{
								value = valueElem.text();
								feature.setValue(value);
							}							
						}
					}					
				}
				if (feature != null) { //for last element
					features.addFeature(feature);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return features;
	}

	
}
