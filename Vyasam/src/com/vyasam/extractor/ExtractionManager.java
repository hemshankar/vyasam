package com.vyasam.extractor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.vyasam.models.Features;
import com.vyasam.models.ItemDetails;
import com.vyasam.models.ItemsSourceDetails;;

public class ExtractionManager implements Serializable {

	private static final long serialVersionUID = -4741698208929989872L;
	
	ItemListExtractor listExtractor = new ItemListExtractor();
	ItemDetailsExtractor detailsExtractor = new ItemDetailsExtractor();
		
	public List<ItemDetails> getItemList(ItemsSourceDetails source)
	{
		List<ItemDetails> itemsList = new ArrayList<ItemDetails>();
		
		Document doc = Jsoup.parse(source.getHtmlContent());
		
		List<Element> elemList = listExtractor.getItemsList(doc, 
						source.getExtractionSteps().getRootProps(),
						source.getExtractionSteps().getLinkProps()); 
		
		ItemDetails item = null;
		for(Element e: elemList)
		{
			item = new ItemDetails();
			if(e.ownText() !=null && !"".equals(e.ownText()))
				item.setTitle(e.ownText().toString());
			else
				item.setTitle(e.text());
			
			item.setSourceLink(e.attr("abs:href"));	
			item.setItemFeatures(new Features());
			itemsList.add(item);
		}
		source.setItemDetailsList(itemsList);
		return itemsList;
	}
	
	public Features fetchItemDetails(ItemDetails itemDetails) 
	{
		try
		{
			Features features = null;
			String url = itemDetails.getSourceLink();
			Document doc = Jsoup.connect(url).ignoreHttpErrors(true).get();
			
			features = detailsExtractor.extractItmes(doc, 
							itemDetails.getExtractionSteps().getRootProps(), 
							itemDetails.getExtractionSteps().getKeyProps(), 
							itemDetails.getExtractionSteps().getValueProps());  
			
			return features;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	public Features fetchItemDetailsProvidedHTMLContent(ItemDetails itemDetails) 
	{
		try
		{
			Features features = null;
			
			Document doc = Jsoup.parse(itemDetails.getHtmlContent());
			
			features = detailsExtractor.extractItmes(doc, 
							itemDetails.getExtractionSteps().getRootProps(), 
							itemDetails.getExtractionSteps().getKeyProps(), 
							itemDetails.getExtractionSteps().getValueProps());  
			
			return features;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
}
