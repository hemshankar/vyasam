package com.vyasam.extractor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.vyasam.constants.StepsConstants;
import com.vyasam.jsoup.models.ElementProperties;
import com.vyasam.utility.Utility;

public class ItemListExtractor implements Serializable {

	
	private static final long serialVersionUID = -4087913734929284189L;

	/*
	 * fetch the rootElement
	 */
	@Deprecated
	public List<Element> getItemsList(Document doc,
			Map<String, String> rootElement, Map<String, String> itemProps) {

		List<Element> result = new ArrayList<Element>();
		try {
			Element rootElem = null;
			// search with ID
			String rootElemId = rootElement.get(StepsConstants.HAS_ID);
			String rootElemClass = rootElement.get(StepsConstants.HAS_CLASS);
			if (rootElemId != null)
				rootElem = Utility.getElementById(doc.getAllElements(),
						rootElemId, 0);
			else if (rootElemClass != null)
				rootElem = Utility.getElementByClass(doc.getAllElements(),
						rootElemClass, 0);
			List<Element> elems = rootElem.getElementsByTag("a");

			for (Element e : elems) {
				if (Utility.validElement(e, itemProps)) {
					result.add(e);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public List<Element> getItemsList(Element doc, ElementProperties rootProps,
			ElementProperties itemProps) {

		List<Element> result = new ArrayList<Element>();
		try {
			Element rootElem = Utility.getElement(doc, rootProps);			
			Utility.getElements(rootElem, itemProps, result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
