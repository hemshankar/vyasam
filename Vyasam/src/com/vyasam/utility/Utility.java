package com.vyasam.utility;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.vyasam.common_utilities.CommonUtilities;
import com.vyasam.constants.StepsConstants;
import com.vyasam.jsoup.models.ElementProperties;
import com.vyasam.jsoup.models.ElementProperty;
import com.vyasam.jsoup.models.exceptions.VyasamModelsException;

public class Utility implements Serializable{

	private static final long serialVersionUID = 3628103692942475177L;

	public static CommonUtilities commonUtilities = new CommonUtilities();
	
	public static boolean isEmptyString(String str)
	{
		return commonUtilities.isEmptyString(str);
	}
	public static Element getElement(Element root,ElementProperties elemProps) throws VyasamModelsException
	{
		if(elemProps==null 
				|| root ==null 
				|| elemProps.getProperties() == null
				|| elemProps.getProperties().size()==0)
			return null;
		
		List<ElementProperty> elemPList = elemProps.getProperties();
		Integer depth = elemProps.getDepth();
		
		List<Element> thisLevelElements = getElementsFromOneProperty(root,elemPList.get(0)); 
		
		if(depth == 1)
		{
			if(thisLevelElements !=null && thisLevelElements.size()!=0)
				return thisLevelElements.get(0);
			return null;
		}			
		else
		{
			ElementProperties nonFirstElementProps = removeTopElementProperties(elemProps);
			for(Element e: thisLevelElements)
			{	
				Element elem = getElement(e,nonFirstElementProps);
				if(elem !=null)
					return elem;
			}
		}
		return null;
	}
	
	public static void getElements(Element root,ElementProperties elemProps,List<Element> result) throws VyasamModelsException
	{
		if(elemProps==null 
				|| root ==null 
				|| elemProps.getProperties() == null
				|| elemProps.getProperties().size()==0)
			throw new VyasamModelsException("Invalid input while fetching the element.");
		
		List<ElementProperty> elemPList = elemProps.getProperties();
		Integer depth = elemProps.getDepth();
		
		List<Element> firstLevelElements = getElementsFromOneProperty(root,elemPList.get(0)); 
		
		if(depth == 1)
		{
			if(firstLevelElements !=null && firstLevelElements.size()!=0)
				result.addAll(firstLevelElements);			
		}			
		else
		{
			ElementProperties nonFirstElementProps = removeTopElementProperties(elemProps);
			for(Element e: firstLevelElements)
			{	
				getElements(e,nonFirstElementProps,result);
			}
		}
	}
	
	public static List<Element> getElementsFromOneProperty(Element root, ElementProperty propMap) throws VyasamModelsException
	{
		Map<String, String> elementDetails = propMap.getPropMap();
		List<Element> elems = new ArrayList<Element>();
		
		//if the Id is set we just have to use it as there will not be more than one ID
		String attr = elementDetails.get(StepsConstants.HAS_ID);
		if (attr != null)
		{
			elems.add(root.getElementById(attr));
			return elems;
		}
		if((attr = elementDetails.get(StepsConstants.HAS_TAG))!=null)
		{				
			elems = root.getElementsByTag(attr);
		}
		if((attr = elementDetails.get(StepsConstants.HAS_CLASS))!=null)
		{	
			String [] classNames = attr.split(",");
			if(elems.size()>0)
			{
				List<Element> tmp = new ArrayList<Element>();
				for(Element e: elems)
				{				
					tmp.addAll(getElementsWithClasses(e,classNames));
				}
				elems = tmp;
			}
			else
			{
				elems = getElementsWithClasses(root,classNames);
			}
		}
		if((attr = elementDetails.get(StepsConstants.HAS_ATTRIBUTE))!=null)
		{
			String [] attrNames = attr.split(",");
			if(elems.size()>0)
			{
				List<Element> tmp = new ArrayList<Element>();
				for(Element e: elems)
				{				
					tmp.addAll(getElementsWithAttributes(e,attrNames));
				}
				elems = tmp;
			}
			else
			{
				elems = getElementsWithAttributes(root,attrNames);
			}
		}
		if((attr = elementDetails.get(StepsConstants.HAS_ATTRIBUTE_WITH_VALUE))!=null)
		{
			String [] attrKVs = attr.split(",");
			if(elems.size()>0)
			{
				List<Element> tmp = new ArrayList<Element>();
				for(Element e: elems)
				{				
					tmp.addAll(getElementsWithAttributesAndValues(e,attrKVs));
				}
				elems = tmp;
			}
			else
			{
				elems = getElementsWithAttributesAndValues(root,attrKVs);
			}
		}
		if((attr = elementDetails.get(StepsConstants.HAS_NO_TEXT_LIKE))!=null)
		{
			String [] invalidTexts = attr.split(",");
			if(elems.size()==0)
			{elems = root.getAllElements();}
			List<Element> tmp = new ArrayList<Element>();
			Boolean validElement = true;
			for(Element e: elems)
			{		
				validElement = true;	
				for(String str:invalidTexts)
				{
					if(e.text().equalsIgnoreCase(str))
					{
						validElement = false;
						break;
					}
				}	
				if(validElement)
					tmp.add(e);
			}
			elems = tmp;			
		}
		if((attr = elementDetails.get(StepsConstants.HAS_TEXT_LIKE))!=null)
		{
			String [] invalidTexts = attr.split(",");
			if(elems.size()==0)
			{elems = root.getAllElements();}
			List<Element> tmp = new ArrayList<Element>();
			
			Boolean validElement = true;
			for(Element e: elems)
			{		
				validElement = true;		
				for(String str:invalidTexts)
				{
					if(!e.text().equalsIgnoreCase(str))
					{
						validElement = false;
						break;
					}
				}	
				if(validElement)
					tmp.add(e);
			}
			elems = tmp;
			
		}
		if((attr = elementDetails.get(StepsConstants.CONTAINS_NO_TEXT_LIKE))!=null)
		{
			String [] invalidTexts = attr.split(",");
			if(elems.size()==0)
			{elems = root.getAllElements();}
			List<Element> tmp = new ArrayList<Element>();
			Boolean validElement = true;
			for(Element e: elems)
			{		
				validElement = true;
				for(String str:invalidTexts)
				{
					if(e.text().contains(str))
					{
						validElement = false;
						break;
					}
				}
				if(validElement)
					tmp.add(e);
			}
			elems = tmp;			
		}
		//can be used as pattern match
		if((attr = elementDetails.get(StepsConstants.CONTAINS_TEXT_LIKE))!=null)
		{
			String [] validTexts = attr.split(",");
			
			if(elems.size()==0)
			{elems = root.getAllElements();}
			
			List<Element> tmp = new ArrayList<Element>();
			Boolean validElement = true;
			for(Element e: elems)
			{		
				validElement = true;	
				for(String str:validTexts)
				{
					if(!e.text().contains(str))
					{
						validElement = false;
						break;
					}
				}
				if(validElement)
					tmp.add(e);
			}
			elems = tmp;		
		}
		if((attr = elementDetails.get(StepsConstants.HAS_EMPTY_TEXT))!=null)
		{			
			if(elems.size()==0)
			{elems = root.getAllElements();}
			List<Element> tmp = new ArrayList<Element>();
			for(Element e: elems)
			{			
				if(e.text()== null || e.text().equals(""))
					tmp.add(e);
			}
			elems = tmp;			
		}
		if((attr = elementDetails.get(StepsConstants.HAS_NO_EMPTY_TEXT))!=null)
		{			
			if(elems.size()==0)
			{elems = root.getAllElements();}
			List<Element> tmp = new ArrayList<Element>();
			for(Element e: elems)
			{			
				if(e.text()!= null && !e.text().trim().equals(""))
					tmp.add(e);
			}
			elems = tmp;			
		}
		return elems;
	}
	
	public static List<Element> 
	getElementsWithClasses(Element root,String []classNames) throws VyasamModelsException
	{
		if(classNames==null || classNames.length==0)
			throw new VyasamModelsException("Class names empty");
		
		List<Element> tmp1 = root.getElementsByClass(classNames[0]);
		List<Element> tmp2 = null; 
		int count =1;
		
		for(;count<classNames.length;count++)
		{
			tmp2 = root.getElementsByClass(classNames[count]);			
			tmp1 = intersection(tmp1,tmp2);						
		}
		
		return tmp1;
	}
	
	public static List<Element> 
	getElementsWithAttributes(Element root,String []attrNames) throws VyasamModelsException
	{
		if(attrNames==null || attrNames.length==0)
			throw new VyasamModelsException("Class names empty");
		
		List<Element> tmp1 = root.getElementsByAttribute(attrNames[0]);
		List<Element> tmp2 = null; 
		int count =1;
		
		for(;count<attrNames.length;count++)
		{
			tmp2 = root.getElementsByAttribute(attrNames[count]);			
			tmp1 = intersection(tmp1,tmp2);						
		}
		
		return tmp1;
	}
	
	public static List<Element> 
	getElementsWithAttributesAndValues(Element root,String []attrKVs) throws VyasamModelsException
	{
		if(attrKVs==null || attrKVs.length==0)
			throw new VyasamModelsException("Class names empty");
		String[] kvs = attrKVs[0].split(StepsConstants.ATTRIBUTE_VALUE_SPLLITER);
		if(kvs.length<2)
		{
			throw new VyasamModelsException("attrybute key value not proper");
		}
		List<Element> tmp1 = root.getElementsByAttributeValueMatching(kvs[0],kvs[1]);
		List<Element> tmp2 = null; 
		int count =1;
		
		for(;count<attrKVs.length;count++)
		{
			kvs = attrKVs[count].split(StepsConstants.ATTRIBUTE_VALUE_SPLLITER);
			if(kvs.length<2)
			{
				throw new VyasamModelsException("attrybute key value not proper");
			}
			tmp2 = root.getElementsByAttributeValueMatching(kvs[0],kvs[1]);			
			tmp1 = intersection(tmp1,tmp2);						
		}		
		return tmp1;
	}
	
	 public static <T> List<T> intersection(List<T> list1, List<T> list2) {
	        List<T> list = new ArrayList<T>();

	        for (T t : list1) {
	            if(list2.contains(t)) {
	                list.add(t);
	            }
	        }

	        return list;
	    }
	
	public static ElementProperties removeTopElementProperties(ElementProperties props) throws VyasamModelsException
	{
		if(props==null || props.getProperties().size()==0)
			throw new VyasamModelsException("Error while removing the first element. Properties either nulll or size is 0");
		ElementProperties newProperties = props.clone();		
		newProperties.getProperties().remove(0);	
		newProperties.setDepth(props.getDepth()-1);
		return newProperties;
	}
	
	public static Element getElementById(Elements elems, String id, int level) {
		// Document doc = Jsoup.parse(html);
		// Elements elems = doc.getAllElements();
		Element result = null;
		if (elems.size() == 0)
			return null;
		for (Element e : elems) {
			if (id.equals(e.id()))
				return e;
			result = getElementById(e.children(), id, level + 1);
			if (result != null)
				return result;

		}
		return null;
	}

	public static Element getElementByClass(Elements elems, String className, int level) {
		Element result = null;
		if (elems.size() == 0)
			return null;
		for (Element e : elems) {
			if (e.classNames().contains(className))
				return e;
			result = getElementByClass(e.children(), className, level + 1);
			if (result != null)
				return result;
		}
		return null;
	}
/*
	public static Element getElement(Element rootElement,
			Map<String, String> elementDetails) {
		String id = elementDetails.get(StepsConstants.HAS_ID);
		String className = elementDetails.get(StepsConstants.HAS_CLASS);

		Element elem = null;
		if (id != null)
			elem = rootElement.getElementById(id);// getElementById(rootElement.getAllElements(), id, 0);
		else if (className != null)
			elem = getElementByClass(rootElement.getAllElements(), className, 0);

		return elem;
	}
*/
	public static boolean validElement(Element e, Map<String, String> props) {
		/*
		 * Following conditions are checked 1. tag 2. class 3. Text do not
		 * contain
		 */
		for (String key : props.keySet()) {
			switch (key) {
			case StepsConstants.HAS_TAG:
				if (!e.tag().toString().equals(props.get(key)))
					return false;
				break;
			case StepsConstants.HAS_CLASS:
				if (!e.classNames().contains(props.get(key)))
					return false;
				break;
			case StepsConstants.HAS_NO_TEXT_LIKE:
				String text = props.get(key);
				for (String str : text.split(",")) {
					if (e.text().equals(str))
						return false;
				}

				break;
			case StepsConstants.HAS_EMPTY_TEXT:
				if (e.text().equals("")
						&& props.get(key).equals(StepsConstants.FALSE))
					return false;
				if (!e.text().equals("")
						&& props.get(key).equals(StepsConstants.TRUE))
					return false;
				break;
			case StepsConstants.HAS_ATTRIBUTE:
				if (!e.hasAttr(props.get(key)))
					return false;
					break;
					
			case StepsConstants.CONTAINS_NO_TEXT_LIKE:
				text = props.get(key);
				for (String str : text.split(",")) {
					if (e.text().contains(str))
						return false;
				}
				break;
			}
		}
		return true;
	}

	public static String printPageSummary(Elements elems, int level) {
		if (elems.size() == 0)
			return "";
		String str = "";
		for (Element e : elems) 
		{
			for (int i = 0; i < level; i++)
				str = str + "\t";
			str = str + "\n<" + e.tagName() + " id=" + e.id() + " class="
					+ e.classNames() + " href=" + e.attr("abs:href") + ">"
					+ e.ownText();
			str = str + printPageSummary(e.children(), level + 1);
		}	
		return str;
	}

	public static void printSummary(Elements elems, int level) throws FileNotFoundException, UnsupportedEncodingException
	{		
		writeToFile("summeryFile.txt",printPageSummary(elems,level));
	}
	
	public static void writeToFile(String fileName,String content) throws FileNotFoundException, UnsupportedEncodingException
	{
		PrintWriter writer = new PrintWriter(fileName, "UTF-8");
		writer.println(content);
		writer.close();
	}
}
