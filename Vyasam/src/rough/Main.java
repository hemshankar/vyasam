package rough;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.swing.text.StringContent;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.vyasam.constants.StepsConstants;
import com.vyasam.extractor.ExtractionManager;
import com.vyasam.extractor.ItemDetailsExtractor;
import com.vyasam.extractor.ItemListExtractor;
import com.vyasam.jsoup.models.ElementProperties;
import com.vyasam.jsoup.models.ElementProperty;
import com.vyasam.jsoup.models.ListExtractionSteps;
import com.vyasam.models.Features;
import com.vyasam.models.ItemsSourceDetails;
import com.vyasam.persistance.PersistanceManager;
import com.vyasam.utility.Utility;

public class Main {

	public static void main(String [] args)
	{
		try
		{
			String file = 
					"C:/Users/hsahu/Desktop/temp/compareO/"
					+"Mobiles - Buy Mobiles Online at Best Prices in India.html";
					//+ "All Vodafone phones.html";
		
			List<Element> list  = fetchItemList2(file);
			
			
			String file2 = "C:/Users/hsahu/Desktop/temp/compareO/Dell Inspiron.html";
					//"C:/D Drive/Projects/robocode/robocode-master/robocode-master/robocode.distribution/target/archive-tmp/fileSetFormatter.159761914.tmp/robocode/_Robot.html";
			//Features features = fetchItemDetails(file2);
			//System.out.println(features.getFeaturesList().toString());
			
			
			
			
			/*Random rand = new Random();
			rand.setSeed(303);
			int count = 1;
			for(Element e: list)
			{
				Long waitTime = (long) (2000 + rand.nextInt(3000));
				System.out.println("Wating for " + waitTime);
				Thread.sleep(waitTime);
				List<Feature> featureList = fetchItemDetails(e.attr("abs:href"));
				Utility.writeToFile
				("C:/Users/hsahu/Desktop/temp/compareO/featurelist/htc" + count++ +".txt",
						featureList.toString());
			}*/
		}
		catch(Exception e){e.printStackTrace();}
	}
	
	public static List<Element> fetchItemList(String file) throws IOException {
		
		Document doc = Jsoup.parse(new File(file), "utf-8");
				
		Map<String, String> rootProps = new LinkedHashMap<String, String>();
		rootProps.put(StepsConstants.HAS_ID, "products");
		//map.put(StepsConstants.HAS_CLASS, "list-content");

		Map<String, String> itemProps = new LinkedHashMap<>();		
		itemProps.put(StepsConstants.HAS_TAG, "a");
		itemProps.put(StepsConstants.CONTAINS_NO_TEXT_LIKE,
				"Shortlist,Sellers");
		itemProps.put(StepsConstants.HAS_NO_TEXT_LIKE,
				"");
				//"See Prices,compare,See Full Specifications,Upcoming,See Details");
		 
		
		
		List<Element> items = new ItemListExtractor().getItemsList(doc, rootProps, itemProps);
		String output = "";
		String item_ = "";
		for (Element item : items) {
			item_ = item.text() + "-->" + item.attr("abs:href");
			System.out.println(item_);
			output = "\n" + output + " " + item.text() + "-->" + item.attr("abs:href");
		}
		Utility.writeToFile("C:/Users/hsahu/Desktop/temp/compareO/featurelist/ItemList.txt",output);
		return items;
	}
	
	public static List<Element> fetchItemList2(String file) throws IOException {
		
		Document doc = Jsoup.parse(new File(file), "utf-8");
				
		Map<String, String> rootProps = new LinkedHashMap<String, String>();
		rootProps.put(StepsConstants.HAS_ID, "products");
		//map.put(StepsConstants.HAS_CLASS, "list-content");

		ElementProperties props = new ElementProperties();
		ElementProperty property = new ElementProperty();
		property.setPropMap(rootProps);
		props.addProperty(property);
		
		Map<String, String> itemProps = new LinkedHashMap<>();		
		itemProps.put(StepsConstants.HAS_TAG, "a");
		itemProps.put(StepsConstants.CONTAINS_NO_TEXT_LIKE,
				"Shortlist,Sellers,Out ");
		itemProps.put(StepsConstants.HAS_NO_EMPTY_TEXT,
				"");
		itemProps.put(StepsConstants.HAS_NO_TEXT_LIKE,
				"Out of Stock");
		ElementProperties props2 = new ElementProperties();
		ElementProperty property2 = new ElementProperty();
		property2.setPropMap(itemProps);
		props2.addProperty(property2);
		
		ItemsSourceDetails sourceDetails = new ItemsSourceDetails();
		sourceDetails.setHtmlContent(doc.html());
		ListExtractionSteps listExtractionSteps = new ListExtractionSteps();
		listExtractionSteps.setRootProps(props);
		listExtractionSteps.setLinkProps(props2);
		sourceDetails.setExtractionSteps(listExtractionSteps);
		
		ExtractionManager manager = new ExtractionManager();
		manager.getItemList(sourceDetails);
		
		PersistanceManager perManager = new PersistanceManager();
		
		perManager.save(sourceDetails);
		
		//"See Prices,compare,See Full Specifications,Upcoming,See Details");
		
		List<Element> items = new ItemListExtractor().getItemsList(doc, props, props2);
		String output = "";
		String item_ = "";
		for (Element item : items) {
			item_ = item.text() + "-->" + item.attr("abs:href");
			System.out.println(item_);
			output = "\n" + output + " " + item.text() + "-->" + item.attr("abs:href");
		}
		Utility.writeToFile("C:/Users/hsahu/Desktop/temp/compareO/featurelist/ItemList.txt",output);
		return items;
	}
	
	public static Features fetchItemDetails(String file) throws IOException
	{
		Document doc = Jsoup.parse(new File(file), "utf-8");
		
		ElementProperties rootProps = new ElementProperties();
		ElementProperty property = new ElementProperty();
		property.add(StepsConstants.HAS_CLASS, "detailssubbox");		
		rootProps.addProperty(property);
		
		ElementProperties keyProps = new ElementProperties();
		
		ElementProperty keyProperty1 = new ElementProperty();
		keyProperty1.add(StepsConstants.HAS_TAG, "td");
		
		ElementProperty keyProperty2 = new ElementProperty();
		keyProperty2.add(StepsConstants.HAS_ATTRIBUTE_WITH_VALUE, "width=20%");
		/*
		ElementProperty keyproperty2 = new ElementProperty();
		keyproperty2.add(key, value);
		
		ElementProperty keyproperty3 = new ElementProperty();
		keyproperty3.add(key, value);
		*/
		keyProps.addProperty(keyProperty1);
		keyProps.addProperty(keyProperty2);				
		
		ElementProperties valueProps = new ElementProperties();
		
		ElementProperty valueProperty1 = new ElementProperty();
		valueProperty1.add(StepsConstants.HAS_TAG, "td");
		
		/*ElementProperty ValueProperty2 = new ElementProperty();
		ValueProperty2.add(StepsConstants.HAS_TAG, "code");
		
		ElementProperty ValueProperty3 = new ElementProperty();
		ValueProperty3.add(StepsConstants.HAS_TAG, "strong");		

		ElementProperty ValueProperty4 = new ElementProperty();
		ValueProperty4.add(StepsConstants.HAS_TAG, "a");		*/
		//ValueProperty4.add(StepsConstants.HAS_NO_EMPTY_TEXT,"");
		
		valueProps.addProperty(valueProperty1);
		/*valueProps.addProperty(ValueProperty2);
		valueProps.addProperty(ValueProperty3);
		valueProps.addProperty(ValueProperty4);*/
		
		Features features = new ItemDetailsExtractor().extractItmes(doc, rootProps, keyProps, valueProps);
		System.out.println("Fetched " + features.getFeaturesList().size() + " features from URL " + file);
		return features;
	}
	
}
