package com.vyasam.server;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.vyasam.category.models.VClass;
import com.vyasam.category.models.VClassPersist;
import com.vyasam.models.ItemDetails;
import com.vyasam.models.ItemsSourceDetails;
import com.vyasam.persistance.PersistanceManager;
import com.vyasam.utility.models.ListOfList;

@Path("manager")
public class PersistManager {

	PersistanceManager  persistManager = new PersistanceManager();
	
	
	@Path("fetch/HQL")
	@GET
	@Produces ({MediaType.APPLICATION_XML})
	public ListOfList fetchDataFromHQL(@Context UriInfo ui)
	{
		try
		{
			MultivaluedMap<String, String> queryParams = ui.getQueryParameters();
			String sql = queryParams.getFirst("HQL");			
			List result = (List) persistManager.fetch(sql);
			
			ListOfList listClass = new ListOfList();
			listClass.addListOfList(result);			
			return listClass;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	@Path("fetch/itemDetails")
	@GET	
	@Produces ({MediaType.APPLICATION_XML})
	public List<ItemDetails> fetchItemDetails(@Context UriInfo ui)
	{
		try
		{
			MultivaluedMap<String, String> queryParams = ui.getQueryParameters();
			String sql = queryParams.getFirst("SQL");
			//String entityName = queryParams.getFirst("ENTITY_NAME");
			String entityName = "com.vyasam.models.ItemDetails";
			return persistManager.fetch(sql, entityName);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	@Path("fetch")
	@GET
	@Produces ({MediaType.APPLICATION_XML})
	public Response fetch(@Context UriInfo ui)
	{
		try
		{
			MultivaluedMap<String, String> queryParams = ui.getQueryParameters();
			String sql = queryParams.getFirst("SQL");
			//String entityName = queryParams.getFirst("ENTITY_NAME");
			//Class class_ = Class.forName(entityName);
			//List<Class.forName(entityName)> 
			List<String> result = new ArrayList<String>();
			List fetchedResult = persistManager.fetch(sql);
			for(Object obj : fetchedResult)
			{
				result.add(obj.toString());
			}

			GenericEntity<List<String>> entity = new GenericEntity<List<String>>(result) {};
			Response response = Response.ok(entity).build();
			return response;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	
	@Path("save/VClass")
	@POST
	@Consumes({MediaType.APPLICATION_XML})
	public void saveVClass(VClass vClass)
	{
		try
		{
			persistManager.save(vClass);			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Path("hello")
	@GET	
	public String hello()
	{
		return "Hello";
	}
}
