package com.vyasam.server;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import com.vyasam.models.ExtractionSteps;

@Path("extrationStepsPM")
public class ExtractionStepsPersistManager extends PersistManager{
	@Path("save/steps")
	@POST
	@Consumes({ MediaType.APPLICATION_XML })
	public void saveObject(ExtractionSteps ExtractionSteps) {
		try {
			persistManager.save(ExtractionSteps);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Path("save_update/steps")
	@POST
	@Consumes({ MediaType.APPLICATION_XML })
	public void saveOrUpdateObject(ExtractionSteps ExtractionSteps) {
		try {
			persistManager.saveOrUpdate(ExtractionSteps);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Path("delete/steps")
	@POST
	@Consumes({ MediaType.APPLICATION_XML })
	public void deleteObject(ExtractionSteps ExtractionSteps) {
		try {
			persistManager.Delete(ExtractionSteps);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Path("fetch/steps/HQL")
	@GET
	@Produces({ MediaType.APPLICATION_XML })
	public List<ExtractionSteps> fetchItemsSourceFromHQL(@Context UriInfo ui) {
		try {
			MultivaluedMap<String, String> queryParams = ui
					.getQueryParameters();
			String sql = queryParams.getFirst("HQL");
			List<ExtractionSteps> result = (List<ExtractionSteps>) persistManager.fetch(sql);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
