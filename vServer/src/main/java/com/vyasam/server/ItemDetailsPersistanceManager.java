package com.vyasam.server;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import com.vyasam.jsoup.models.exceptions.VyasamModelsException;
import com.vyasam.models.ItemDetails;
import com.vyasam.models.ItemsSourceDetails;
import com.vyasam.query.models.Query;
import com.vyasam.utility.db.ConnectionFactory;
import com.vyasam.utility.db.DBType;

@Path("ItemDetailsPM")
public class ItemDetailsPersistanceManager extends PersistManager {

	@Path("save/itemsDetails")
	@POST
	@Consumes({ MediaType.APPLICATION_XML })
	public void saveObject(ItemDetails itemDetails) {
		try {
			persistManager.save(itemDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Path("save_update/itemsDetails")
	@POST
	@Consumes({ MediaType.APPLICATION_XML })
	public void saveOrUpdateObject(ItemDetails itemDetails) {
		try {
			persistManager.saveOrUpdate(itemDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Path("delete/itemsDetails")
	@POST
	@Consumes({ MediaType.APPLICATION_XML })
	public void deleteObject(ItemDetails itemDetails) {
		try {
			persistManager.Delete(itemDetails);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Path("fetch/ItemDetails/HQL")
	@GET
	@Produces({ MediaType.APPLICATION_XML })
	public List<ItemDetails> fetchItemsSourceFromHQL(@Context UriInfo ui) {
		try {
			MultivaluedMap<String, String> queryParams = ui
					.getQueryParameters();
			String sql = queryParams.getFirst("HQL");
			List<ItemDetails> result = (List<ItemDetails>) persistManager.fetch(sql);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Path("fetch/itemDetails")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public ItemDetails fetchItemDetails(ItemDetails source) {
		try {
			return persistManager.fetch(source, ItemDetails.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Path("fetch/itemDetails/query")
	@POST
	@Produces({ MediaType.APPLICATION_XML })
	@Consumes({ MediaType.APPLICATION_XML })
	public List<ItemDetails> fetchItemDetailsFromQuery(Query query)
			throws VyasamModelsException {
		List<ItemDetails> list = new ArrayList<ItemDetails>();
		try {
			ResultSet rs = persistManager.executeSQL(query);
			ItemDetails ids = null;
			while (rs.next()) {
				ids = new ItemDetails();
				ids.setClassId(rs.getInt("classId"));
				ids.setId(rs.getInt("id"));
				ids.setStatus(rs.getString("Status"));
				ids.setItemFeaturesXML(rs.getString("itemFeaturesXML"));
				ids.setTitle(rs.getString("title"));
				ids.setSourceLink(rs.getString("sourceLink"));
				ids.setSourceName(rs.getString("sourceName"));
				ids.setExtractionStepsXML(rs.getString("extractionStepsXML"));
				ids.setMoreXML(rs.getString("moreXML"));
				ids.setFetchedTime("fetchedTime");
				list.add(ids);			
			}			
			return list;
		} catch (Exception e) {
			throw new VyasamModelsException(e.getMessage());
		}
	}
	
	@Path("fetch/itemDetails/raw_query")
	@GET
	@Produces({ MediaType.APPLICATION_XML })
	public List<ItemDetails> fetchItemDetailsFromQuery(@Context UriInfo ui) throws VyasamModelsException {
		
		try {
			
			MultivaluedMap<String, String> queryParams = ui
					.getQueryParameters();
			String rawQuery = queryParams.getFirst("SQL");
			
			Query query = new Query();
			query.setQuery(rawQuery);
			
			return fetchItemDetailsFromQuery(query);
		} catch (Exception e) {
			throw new VyasamModelsException(e.getMessage());
		}
	}
}
