package com.vyasam.server;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import com.vyasam.models.ItemsSourceDetails;

@Path("ItemSourceDetailsPM")
public class ItemSourceDetailsPersistManager extends PersistManager{

	@Path("save/itemsSource")
	@POST
	@Consumes({MediaType.APPLICATION_XML})
	public void saveObject(ItemsSourceDetails itemSourceDetails)
	{
		try
		{
			persistManager.save(itemSourceDetails);
			/*itemSourceDetails = (ItemsSourceDetails)persistManager.save(itemSourceDetails);
			for(ItemDetails itemDetails: itemSourceDetails.getItemDetailsList())
			{
				itemDetails.setSourceDetails(itemSourceDetails);
				itemDetails = (ItemDetails)persistManager.save(itemDetails);
			}*/
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Path("save_update/itemsSource")
	@POST
	@Consumes({MediaType.APPLICATION_XML})
	public void saveOrUpdateObject(ItemsSourceDetails itemSourceDetails)
	{
		try
		{
			persistManager.saveOrUpdate(itemSourceDetails);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Path("delete/itemsSource")
	@POST
	@Consumes({MediaType.APPLICATION_XML})
	public void deleteObject(ItemsSourceDetails itemSourceDetails)
	{
		try
		{
			persistManager.Delete(itemSourceDetails);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}	

	@Path("fetch/itemsSourceDetails/HQL")
	@GET
	@Produces ({MediaType.APPLICATION_XML})
	public List<ItemsSourceDetails> fetchItemsDetailsFromHQL(@Context UriInfo ui)
	{
		try
		{
			MultivaluedMap<String, String> queryParams = ui.getQueryParameters();
			String sql = queryParams.getFirst("HQL");
			Integer limit = Integer.parseInt(queryParams.getFirst("Limit"));
			List<ItemsSourceDetails> result = (List<ItemsSourceDetails>) persistManager.fetch(sql,limit);
			return result;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
}
