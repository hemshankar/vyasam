package com.vyasam.server;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import com.vyasam.category.models.VClass;
import com.vyasam.category.models.VClassPersist;

@Path("vclassPM")
public class VClassPersistManager extends PersistManager{

	@Path("save_update/VClass")
	@POST
	@Consumes({MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_XML})
	public VClass saveOrUpdateVClass(VClass vClass)
	{
		try
		{
			//VClass.updateParents(vClass); 
			vClass = (VClass)persistManager.saveOrUpdate(vClass); //bidirectional consistency problem http://meri-stuff.blogspot.in/2012/03/jpa-tutorial.html#RelationshipsBidirectionalOneToManyManyToOneConsistency	
			System.out.println("Added " + vClass.getId() + ":" + vClass.getName());
			for(VClass child: vClass.getChildren())
			{
				child.setParentId(vClass.getId());
				saveOrUpdateVClass(child);
				System.out.println("Parent ===== " + vClass.getId() + " Child: " + child.getId());
			}
			
			//VClass.detachParents(vClass);
			//System.out.println("----------" + vClass.getId());			
			//VClass.printClassHeirarchy(vClass,0);
			return vClass;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return vClass;
	}
	
	@Path("save_update/VClassPersist")
	@POST
	@Consumes({MediaType.APPLICATION_XML})
	@Produces({MediaType.APPLICATION_XML})
	public VClassPersist saveOrUpdateVClass(VClassPersist vClassPersist)
	{
		try
		{			
			vClassPersist = (VClassPersist)persistManager.saveOrUpdate(vClassPersist); //bidirectional consistency problem http://meri-stuff.blogspot.in/2012/03/jpa-tutorial.html#RelationshipsBidirectionalOneToManyManyToOneConsistency	
			System.out.println("Added " + vClassPersist.getClassId() + ":" + vClassPersist.getClassName());			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return vClassPersist;
	}
	
	@Path("delete/VClassPersist")
	@POST
	@Consumes({MediaType.APPLICATION_XML})
	public void deleteVClass(VClassPersist vClassPersist)
	{
		try
		{
			persistManager.Delete(vClassPersist);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Path("fetch/allClasses/HQL")
	@GET
	@Produces ({MediaType.APPLICATION_XML})
	public List<VClassPersist> fetchAllVClassesFromHQL(@Context UriInfo ui)
	{
		try
		{
			MultivaluedMap<String, String> queryParams = ui.getQueryParameters();
			String sql = queryParams.getFirst("HQL");			
			List<VClassPersist> result = (List<VClassPersist>) persistManager.fetch(sql);
			return result;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
}
