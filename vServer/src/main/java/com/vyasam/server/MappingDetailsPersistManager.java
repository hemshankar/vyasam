package com.vyasam.server;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import com.vyasam.feature.mapping.MappingDetails;

@Path("feature_mapping")
public class MappingDetailsPersistManager extends PersistManager{
	@Path("save/mapping")
	@POST
	@Consumes({MediaType.APPLICATION_XML})
	public void saveObject(MappingDetails mappingDetails)
	{
		try
		{
			persistManager.save(mappingDetails);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Path("save_update/mapping")
	@POST
	@Consumes({MediaType.APPLICATION_XML})
	public void saveOrUpdateObject(MappingDetails mappingDetails)
	{
		try
		{
			persistManager.saveOrUpdate(mappingDetails);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Path("delete/mapping")
	@POST
	@Consumes({MediaType.APPLICATION_XML})
	public void deleteObject(MappingDetails mappingDetails)
	{
		try
		{
			persistManager.Delete(mappingDetails);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}	

	@Path("fetch/mapping/HQL")
	@GET
	@Produces ({MediaType.APPLICATION_XML})
	public List<MappingDetails> fetchItemsDetailsFromHQL(@Context UriInfo ui)
	{
		try
		{
			MultivaluedMap<String, String> queryParams = ui.getQueryParameters();
			String sql = queryParams.getFirst("HQL");
			Integer limit;
			try{
			limit = Integer.parseInt(queryParams.getFirst("Limit"));
			}catch(Exception e){
				limit = 999999;
			}
			List<MappingDetails> result = (List<MappingDetails>) persistManager.fetch(sql,limit);
			return result;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
}
