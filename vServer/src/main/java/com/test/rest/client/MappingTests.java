package com.test.rest.client;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.ClientConfig;

import com.vyasam.category.models.VClass;
import com.vyasam.feature.mapping.MappingDetails;
import com.vyasam.models.ItemDetails;

public class MappingTests {

	
	public static void main(String [] args){
		
		//==================ADD Mapping=====================
		MappingDetails details = new MappingDetails();
		details.setCategoryId("1");
		details.getColumnMapping().put("s2", "t2");
		details.setMappingName("TestMapping3");
		testAdd(details);
		
		//==================Fetch Mapping=====================
		List<MappingDetails> detailsList = fetchMapping();
		
	}
	
	public static void testAdd(Object obj1)
	{
		String URL2 = "http://localhost:8080/vServer/persist/feature_mapping/save/mapping";
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL2);
		
		target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(obj1, MediaType.APPLICATION_XML),ItemDetails.class);		
	}
	
	public static List<MappingDetails> fetchMapping()
	{
		String URL2 = "http://localhost:8080/vServer/persist/feature_mapping/fetch/mapping/HQL";
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL2);
		
		List<MappingDetails> list = target.queryParam("HQL", "from MappingDetails").request(MediaType.APPLICATION_XML).get(new GenericType<List<MappingDetails>>(){});
		
		for(MappingDetails d: list)
		{
			System.out.println(d.getMappingName());
		}
		return list;	
	}
}
