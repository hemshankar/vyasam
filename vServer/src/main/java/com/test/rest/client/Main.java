package com.test.rest.client;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.ClientConfig;

import com.vyasam.category.models.VClass;
import com.vyasam.models.ItemDetails;
import com.vyasam.models.ItemsSourceDetails;
import com.vyasam.utility.models.ListOfList;

public class Main {

	public static void main(String [] args)
	{
		//ExtractionManager manager = new ExtractionManager();
		
		ItemDetails iDetails1 = new ItemDetails();

		
		iDetails1.setSourceLink("link1");
		ItemDetails iDetails2 = new ItemDetails();

		
		iDetails2.setSourceLink("link2");
		
		ItemsSourceDetails source = new ItemsSourceDetails();
		source.setItemDetailsList(new ArrayList<ItemDetails>());
		source.getItemDetailsList().add(iDetails1);
		//source.getItemDetailsList().add(iDetails2);
				
		
		testRest(source);
		//fetchGeneralData();
		addVClass();
		//fetchClasses();
	}		

	public static void testRest(Object obj1)
	{
		String URL2 = "http://localhost:8080/vServer/persist/manager/save/itemsSource";
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL2);
		
		target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(obj1, MediaType.APPLICATION_XML),ItemDetails.class);
		
		//String obj = target.request(MediaType.TEXT_PLAIN).get(String.class);
		//TestObject obj = target.request(MediaType.APPLICATION_XML).get(TestObject.class);
		//System.out.println(obj);//.toString();
	}
	
	public static void fetchItemSource()
	{
		
		String URL2 = "http://localhost:8080/vServer/persist/manager/fetch/HQL";
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL2);
		
		List<ItemDetails> list = target.queryParam("HQL", "from ItemDetails").request(MediaType.APPLICATION_XML).get(new GenericType<List<ItemDetails>>(){});
		
		for(ItemDetails d: list)
		{
			System.out.println(d.getId());
		}

	}
	
	public static void fetchClasses()
	{
		String URL2 = "http://localhost:8080/vServer/persist/manager/fetch/allClasses/HQL";
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL2);
		
		List<VClass> list = target.queryParam("HQL", "from VClass").request(MediaType.APPLICATION_XML).get(new GenericType<List<VClass>>(){});
		
		for(VClass d: list)
		{
			System.out.println(d.getId());
		}
	}
	

	public static void fetchGeneralData()
	{
		
		String URL2 = "http://192.168.91.140:8080/vServer/vclassPM/manager/fetch/HQL";
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL2);
		
		ListOfList list = target.queryParam("HQL", "Select id,classList from ItemDetails").request(MediaType.APPLICATION_XML).get(ListOfList.class);
		
		System.out.println(list);
		/*for(ItemDetails d: list)
		{
			System.out.println(d.getId());
		}*/

	}
	
	public static void addVClass(){
		
		VClass p = new VClass();
		p.setName("Root");
		
		VClass c1 = new VClass();
		c1.setName("Level_1_Child_1");
		//c1.setParent(p);
		VClass c2 = new VClass();
		c2.setName("Level_1_Child_2");
		//c2.setParent(p);
		
		VClass cc1 = new VClass();
		cc1.setName("Level_2_Sub_1_Child_1");
		//cc1.setParent(c1);

		VClass cc2 = new VClass();
		cc2.setName("Level_2_Sub_1_Child_2");
		//cc2.setParent(c1);

		p.getChildren().add(c1);
		p.getChildren().add(c2);
		c1.getChildren().add(cc1);
		c1.getChildren().add(cc2);
		
		String URL2 = "http://192.168.91.140:8080/vServer/persist/vclassPM/save_update/VClass";
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL2);
		
		/*target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(p, MediaType.APPLICATION_XML),VClass.class);
		*/
		
		VClass.detachParents();
		VClass.printClassHeirarchy(p, 0);
		p = target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(p, MediaType.APPLICATION_XML),VClass.class);
		
		VClass.updateParents(p);
		VClass.printClassHeirarchy(p, 0);
		
		c1 = p.getChildren().get(1);
		
		VClass cc3 = new VClass();
		cc3.setName("Level_2_Sub_2_Child_1");
		System.out.println("=================" + c1.getId());
		c1.getChildren().add(cc3);
		
		VClass cc4 = new VClass();
		cc4.setName("Level_2_Sub_2_Child_1");
		c2.getChildren().add(cc4);
		//p.getChildren().add(cc);
	
		//VClass.updateParents(p);
		VClass.detachParents();
		VClass.printClassHeirarchy(p, 0);
		p = target.request(MediaType.APPLICATION_XML).
				post(Entity.entity(p, MediaType.APPLICATION_XML),VClass.class);

		VClass.updateParents(p);
		VClass.printClassHeirarchy(p, 0);
		/*target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(c1, MediaType.APPLICATION_XML),VClass.class);
		*/
		
	}
	
	public static void updateVClass(){
		
	}
	
	public static void fetchVClass(){
		
	}
	
	public static void deleteVClass(){
		
	}
	
}
