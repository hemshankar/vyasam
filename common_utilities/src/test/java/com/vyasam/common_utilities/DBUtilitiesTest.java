package com.vyasam.common_utilities;


import java.sql.Connection;

import com.vyasam.utility.db.ConnectionFactory;
import com.vyasam.utility.db.DBType;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class DBUtilitiesTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public DBUtilitiesTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( DBUtilitiesTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
    	String url = "jdbc:mysql://localhost:3306/mysql";
    	String username = "root";
    	String password = "infa";
    	
    	try {
			Connection conn = ConnectionFactory.getConnection(DBType.MYSQL, url, username, password);
			if(conn!=null)
			{
	        	 conn.close();
			}	        	 
			else
				assertTrue( false );
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
    }
}
