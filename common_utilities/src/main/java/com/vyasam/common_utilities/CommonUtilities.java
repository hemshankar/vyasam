package com.vyasam.common_utilities;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

public class CommonUtilities {
	
	static Calendar calendar = Calendar.getInstance();
	public static String getTime()	{

		calendar.setTimeInMillis(System.currentTimeMillis());
		int mYear = calendar.get(Calendar.YEAR);
		int mMonth = calendar.get(Calendar.MONTH);
		int mDay = calendar.get(Calendar.DAY_OF_MONTH);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int min = calendar.get(Calendar.MINUTE);
		int sec = calendar.get(Calendar.SECOND);
		
		return mYear + "/" + mMonth + "/" + mDay + " " + hour + ":" + min + ":" + sec;
	}
	public static boolean isEmptyString(String str)
	{
		if(str==null || str.trim().equals(""))
			return true;
		return false;
	}
	
	public static String getCurrentTime()
	{
		Date date= new java.util.Date();
		return (new Timestamp(date.getTime())).toString();
	}
}
