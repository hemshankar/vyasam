package com.vyasam.common.serialization;

import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.vyasam.utility.exceptions.CommonUtilitesException;

/**
 * 
 * @author hsahu
 */
public class SerializationUtility {

	public <T> String objectToXML(Class className, T obj)
			throws CommonUtilitesException {
		if(obj==null)
			throw (new CommonUtilitesException("NullPointerException"));
		
		try {

			StringWriter sw = new StringWriter();
			// File file = new File("C:\\file.xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(className);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			// jaxbMarshaller.marshal(item.getExtractionSteps(), file);
			// jaxbMarshaller.marshal(item.getExtractionSteps(), System.out);
			jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			jaxbMarshaller.marshal(obj, sw);
			return sw.toString();

		} catch (JAXBException e) {
			throw (new CommonUtilitesException("error at ObjectToXML" + e.getMessage()));
		}
	}

	public <T> void objectToFile(Class className, Class<T> obj, String fileName)
			throws CommonUtilitesException {
		if(obj==null || fileName==null || fileName.equals(""))
			throw (new CommonUtilitesException("NullPointerException"));
		
		try {

			File file = new File("C:\\file.xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(className);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			jaxbMarshaller.marshal(obj, file);

			// jaxbMarshaller.marshal(obj, System.out);

		} catch (JAXBException e) {
			throw (new CommonUtilitesException("error at objectToFile" + e.getMessage()));
		}
	}

	public <T> T XMLToObjetc(String xml, Class className) 
			throws CommonUtilitesException {
		if(xml==null || className==null || xml.equals(""))
			throw (new CommonUtilitesException("NullPointerException"));
		
		try {
			StringReader reader = new StringReader(xml);
			JAXBContext jaxbContext = JAXBContext.newInstance(className);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			T obj = (T) jaxbUnmarshaller.unmarshal(reader);
			return obj;

		} catch (JAXBException e) {
			throw new CommonUtilitesException("error at XMLToObjetc" + e.getMessage());
		}
	}

	public <T> T XMLFileToObjetc(String fileName, Class className) 
			throws CommonUtilitesException {
		if(fileName==null || className==null || fileName.equals(""))
			throw (new CommonUtilitesException("NullPointerException"));
		try {

			File file = new File(fileName);
			JAXBContext jaxbContext = JAXBContext.newInstance(className);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			T obj = (T) jaxbUnmarshaller.unmarshal(file);
			return obj;

		} catch (JAXBException e) {
			throw new CommonUtilitesException("error at XMLFileToObjetc" + e.getMessage());
		}
	}

}
