package com.vyasam.utility.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ListOfStrings implements Serializable{

	private static final long serialVersionUID = -2039729911645511680L;
	private List<String> strList = new ArrayList<String>();

	public List<String> getStrList() {
		return strList;
	}

	public void setStrList(List<String> strList) {
		this.strList = strList;
	}
	public void add(String listStr)
	{
		strList.add(listStr);
	}
}
