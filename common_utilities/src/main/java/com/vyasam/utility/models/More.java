package com.vyasam.utility.models;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
public class More implements Serializable{

	private static final long serialVersionUID = -7728606368005733562L;
	private Map<String,Object> moreProps = new LinkedHashMap<String, Object>();

	public Map<String, Object> getMoreProps() {
		return moreProps;
	}

	public void setMoreProps(Map<String, Object> moreProps) {
		this.moreProps = moreProps;
	}
	
	public void put(String key,Object value)
	{
		moreProps.put(key, value);
	}
	
	public Object get(String key)
	{
		return moreProps.get(key);
	}
}
