package com.vyasam.utility.models;

import java.util.ArrayList;
import java.util.List;

import javax.swing.event.ListSelectionEvent;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ListOfList {


	private List<ListOfStrings> result = null;
	
	public void addListOfList(List lst)
	{
		result = new ArrayList<ListOfStrings>();
		
		for(Object arr : lst)
		{
			ListOfStrings strList = new ListOfStrings();			
			for(Object str: (Object[])arr)
			{
				strList.getStrList().add(str.toString());
				System.out.println(str);
			}
			result.add(strList);
		}
	}
	
	public List<ListOfStrings> getResult() {
		return result;
	}

	public void setResult(List<ListOfStrings> result) {
		this.result = result;
	}
	
	public void add(ListOfStrings listStr)
	{
		result.add(listStr);
	}
}
