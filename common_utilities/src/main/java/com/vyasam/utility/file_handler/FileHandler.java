package com.vyasam.utility.file_handler;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.vyasam.utility.logger.Logger;

/**
 *
 * @author hsahu
 */
public class FileHandler implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 9047229628773406322L;

	public static String readFile(String fileName)
    {
        String inputFiles = "";
        try
        {
           //Create object of FileReader
            FileReader inputFile = new FileReader(fileName);
            
            //Instantiate the BufferedReader Class
            BufferedReader bufferReader = new BufferedReader(inputFile);

            //Variable to hold the one line data
            String line;

            // Read file line by line and print on the console            
            while((line = bufferReader.readLine()) !=null)                
            {
                if(!("".equals(line)))
                {                    
                    inputFiles = inputFiles + "\n" + line;
                }
            }
           
            //Close the buffer reader
            bufferReader.close();
        }
        catch(Exception e)
        {
            Logger.log("error","Error while reading file line by line:" + e.toString());                      
        } 
        return inputFiles;
    }
    
    public static void appendToFile(String fileName,String content)
    {        
        try
        {
            File file =new File(fileName);

            //if file doesnt exists, then create it
            if(!file.exists())
            {
                file.createNewFile();
            }
            
            FileWriter fileWritter = new FileWriter(file,true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write("\n" + content);
            bufferWritter.close();
            fileWritter.close();            
    	}
        catch(Exception e)
        {
            System.out.println("Error while appending to the file: " + fileName + ". " +  e.toString());
    	}
    }
    
    public static List<String> listFileInFolder(String folder)
    {
        List<String> files = new ArrayList<String>();

        File f = new File(folder);
        if(f.isDirectory())
        {
            for(String fileName: f.list())
            {
                try
                {               
                    File ff = new File(fileName);
                    if(!ff.isDirectory())
                    {
                        files.add(fileName);
                    }
                }
                catch(Exception e)
                {

                }
            }
        }
        else
        {
            Logger.log("error","provided string is not a directory");
        }
        
        return files;
    } 
}
