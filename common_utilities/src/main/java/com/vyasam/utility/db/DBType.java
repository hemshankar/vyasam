package com.vyasam.utility.db;

public enum DBType {
	MYSQL ("com.mysql.jdbc.Driver");
	
	private final String driverClass;
	
	DBType(String driverName)
	{
		driverClass = driverName;
	}
	
	public String getDriverClassName()
	{
		return driverClass;
	}
}
