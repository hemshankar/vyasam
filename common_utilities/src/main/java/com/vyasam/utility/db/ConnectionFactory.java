package com.vyasam.utility.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class ConnectionFactory {
	// static reference to itself
	private static ConnectionFactory instance = new ConnectionFactory();
//	public static final String URL = "jdbc:mysql://localhost/jdbcdb";
//	public static final String USER = "YOUR_DATABASE_USERNAME";
//	public static final String PASSWORD = " YOUR_DATABASE_PASSWORD";
//	public static final String DRIVER_CLASS = "com.mysql.jdbc.Driver";

	private class ConnectionObj
	{
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((URL == null) ? 0 : URL.hashCode());
			result = prime * result
					+ ((USERNAME == null) ? 0 : USERNAME.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			ConnectionObj other = (ConnectionObj) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (URL == null) {
				if (other.URL != null)
					return false;
			} else if (!URL.equals(other.URL))
				return false;
			if (USERNAME == null) {
				if (other.USERNAME != null)
					return false;
			} else if (!USERNAME.equals(other.USERNAME))
				return false;
			return true;
		}
		public String URL = "";
		public String USERNAME = "";
		public String PASSWORD = "";
		public Connection conn = null;
		public DBType dbType = null;
		private ConnectionFactory getOuterType() {
			return ConnectionFactory.this;
		}		
	}
	
	private Map<ConnectionObj, Connection> connectionCache = new HashMap<ConnectionObj,Connection>();

	// private constructor
	private ConnectionFactory() {
		try {
			//Class.forName(DRIVER_CLASS);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Connection createConnection(DBType dbType,String url,String username,String password) throws Exception {
		
		ConnectionObj connectionObj = new ConnectionObj();
		connectionObj.PASSWORD = password;
		connectionObj.USERNAME = username;
		connectionObj.URL = url;
		connectionObj.dbType = dbType;
        
		Connection conn = connectionCache.get(connectionObj);
    	if(conn==null)
    	{
    		try {
    			Class.forName(connectionObj.dbType.getDriverClassName());
    			conn = DriverManager.getConnection(url, username, password);
    			connectionObj.conn = conn;
	            connectionCache.put(connectionObj,conn);
	        } catch (SQLException e) {
	        	throw new Exception("SQLException: ERROR: Unable to Connect to Database: " + e.getMessage());
	        } catch(Exception e){
	        	throw new Exception("Error while connection to database: "+e.getMessage());
	        }
    		
    	}
        return conn;
    }	
	
	/*private Connection createConnection() {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(URL, USER, PASSWORD);
		} catch (SQLException e) {
			System.out.println("ERROR: Unable to Connect to Database.");
		}
		return connection;
	}*/

	public static Connection getConnection(DBType dbType,String url,String username,String password) throws Exception {
		return instance.createConnection(dbType,url,username,password);
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		for(ConnectionObj obj: connectionCache.keySet())
		{
			try {
			obj.conn.close();
			}catch(Exception e){}
		}
	}
}