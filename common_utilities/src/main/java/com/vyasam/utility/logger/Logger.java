package com.vyasam.utility.logger;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;
import java.io.Serializable;
import java.sql.Date;
import java.text.SimpleDateFormat;

import com.vyasam.common.constants.CommonConstants;
import com.vyasam.utility.file_handler.FileHandler;
/**
 *
 * @author hsahu
 * 
 * Implements the logging feature at various levels.
 */
public class Logger implements Serializable
{

	private static final long serialVersionUID = 1348287743815691452L;
	private static String m_mode = "info";     
    
    /**
     * returns the filename with current date.
     * @param type
     * @param comment 
     */
    
    public static String getLogFileName()
    {
        synchronized("GENERATE_LOG_FILE_NAME")
        {
            //add date to the filename
           Date dNow = new Date(System.currentTimeMillis()); 
           SimpleDateFormat ft = new SimpleDateFormat ("yyyy_MM_dd");
           String currentDate =  "_" + ft.format(dNow) + ".txt";
           //Create the directory
           File dirs = new File(CommonConstants.LOG_LOCATION);
           if(!dirs.exists())
           {
               if(dirs.mkdirs())
               {

               }
               else
               {
                   System.out.println("Error while creating log directories");                       
               }
           }
           return CommonConstants.LOG_LOCATION + CommonConstants.LOG_FILENAME + currentDate;
        }
    }
    
     /*
     * loggs to a log file
     * type - info, error
     */
    
    public static void log(String type, String comment)
    {       
              
       //add date to the filename    
       String logFileName = getLogFileName();
       Date dNow = new Date(System.currentTimeMillis()); 
       SimpleDateFormat ft = new SimpleDateFormat ("yyyy_MM_dd");
       
        if(getMode().equals("info"))
        {    
             //add time type to the comment
             ft = new SimpleDateFormat ("hh:mm:ss a zzz");       
             comment = "(" + ft.format(dNow) + ")"+ type + ":" + comment;
             FileHandler.appendToFile(logFileName, comment);
        }            
        else if(getMode().equals("error") && type != null && type.equals("error"))
        {
             //add time and type to the comment
             ft = new SimpleDateFormat ("hh:mm:ss a zzz");       
             comment = "(" + ft.format(dNow) + ")"+ type + ":" + comment;
             FileHandler.appendToFile(logFileName, comment);
        }
        else
        {
            //add time and type to the comment
             ft = new SimpleDateFormat ("hh:mm:ss a zzz");       
             comment = "(" + ft.format(dNow) + ")UndefinedMode: " + comment;
             FileHandler.appendToFile(logFileName, comment);
        }
    }
    
    public static void logExecuteQueryException(String query, Exception e)
    {
          Logger.log("error", "ERROR while executing query: '" + query + "' --- '" + e.toString() + "'");          
    }
    
    //----------------Getter and setter--------------
    public static String getMode() {
        return m_mode;
    }

    public static void setMode(String mode) {
        m_mode = mode;
    }
    
}

