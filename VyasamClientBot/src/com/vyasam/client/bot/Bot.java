package com.vyasam.client.bot;

import java.io.IOException;
import java.util.Map;
import java.util.Random;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.ClientConfig;

import org.jsoup.Jsoup;

import com.common.exceptions.UtilityException;
import com.common.utilities.Utility;
import com.vyasam.client.models.HTMLFetchTask;
import com.vyasam.common_utilities.CommonUtilities;

public class Bot {
	
	HTMLFetchTask fetchTask = null;
	static Map<String, String> config = null;
	static
	{
		try {
			config = Utility.populateConfigurationsFromConfigFile("resources/VyasamClientConfig.txt");
			System.out.println(config);
		} catch (UtilityException e) {
			if(e.getLocalizedMessage().contains("Error while getting the buffered writer"))
			{
				System.out.println("Make sure that " + System.getProperty("user.dir") + "/resources/VyasamClientConfig.txt exist");				
			}
			else
			{
				e.printStackTrace();
			}
			System.exit(0);
		}
	}
	
	public void getTask()
	{
		String URL2 = config.get("GET_TASK_URL");//"http://localhost:8080/scheduler/vyasam/scheduler/getTask"; 
				//"http://192.168.91.138:8080/scheduler/vyasam/scheduler/getTask";
		//System.out.println("Fetiching from " + URL2);
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL2);		
		fetchTask = target.request(MediaType.APPLICATION_XML).get(HTMLFetchTask.class);
		try {
			if(!fetchTask.getTaskType().equals("NOTHING"))
				updateItemDetails();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("\n\n================================\nID: " + fetchTask.getId() + "\n Source: "  + fetchTask.getUrl()  );		
	}
	
	public void updateItemDetails() throws IOException
	{
		//System.out.println("fetchTask.getUrl(): " + fetchTask.getUrl());
		fetchTask.setHtmlContent(Jsoup.connect(fetchTask.getUrl()).get().html());
		
		String URL2 = config.get("UPDATE_TASK_URL");//"http://localhost:8080/scheduler/vyasam/scheduler/updateHtmlContent";
		//System.out.println("Updating to " + URL2);
		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(URL2);
		
		target.request().
					post(Entity.entity(fetchTask, MediaType.APPLICATION_XML));
		System.out.println("Successfully updated id: " + fetchTask.getId());
	}
	
	
	
	public static void main(String [] args)
	{
		Bot t = new Bot();	 
		
		try
		{
			Random rand = new Random(32452342);			
			while(true)
			{
				Integer RNo = rand.nextInt(3);				
				try
				{
					t.getTask();
				}
				catch(Exception e)
				{
					if(e.getLocalizedMessage().contains("Connection refused: connect"))
					{ 
						System.out.println(CommonUtilities.getTime() + ": No server in the specified URL, sleeping for 100 Secs");
						Thread.sleep(10000);
						continue;						
					}
					
					e.printStackTrace();
					break;
				}
				//System.out.println(CommonUtilities.getTime() + " :Fetching a new task");
				System.out.println("Sleeping...(" + (RNo + 1)*10 + " secs)");
				Thread.sleep((RNo + 1)*10*1000);
			}
		}
		catch(Exception e)
		{
			System.out.println("=========" + e.getLocalizedMessage());
			if(e.getLocalizedMessage().contains("Connection refused: connect"))
				main(null);
			e.printStackTrace();
		}
	}
}
