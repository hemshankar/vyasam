package com.cleanview.brand;

import java.io.Serializable;

import com.cleanview.ui.constants.CommonUIConstants;
import com.cleanview.utilities.UIUtil;

public class BrandServiceURLS implements Serializable{
	
	private static final long serialVersionUID = 1L;
	public final static String UPDATE_BRAND =  UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("UPDATE_BRAND");
	public final static String ADD_BRAND =  UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("ADD_BRAND");
	public final static String FETCH_ALL_BRANDS =  UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("FETCH_ALL_BRANDS");
	public final static String DELETE_BRAND =  UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("DELETE_BRAND");	
}
