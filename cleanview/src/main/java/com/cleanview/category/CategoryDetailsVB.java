package com.cleanview.category;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.cleanview.brand.BrandPM;
import com.cleanview.brand.BrandService;
import com.cleanview.feature.groups.FeatureGroupPM;
import com.cleanview.features.FeaturePM;
import com.cleanview.models.BrandDetails;
import com.cleanview.models.CategoryDetailsForPersistance;
import com.cleanview.persist.common.Status;
import com.cleanview.session.CleanViewSession;
import com.cleanview.ui.constants.SessionKeys;
import com.cleanview.ui.interfaces.CategoryChangeListners;
import com.cleanview.ui.models.CategoryDetails;
import com.cleanview.utilities.UIUtil;


@ManagedBean
@RequestScoped
public class CategoryDetailsVB implements Serializable{
	
	private static final long serialVersionUID = 1L;
	CategoryDetailsPM pm = null;
	CleanViewSession session = null;
	BrandPM bpm = null;
	FeatureGroupPM gpm = null;
	FeaturePM fpm = null;
	//ItemsPM ipm = null;
	public CategoryDetailsVB(){
        pm = (CategoryDetailsPM) UIUtil.getPageModel("#{categoryDetailsPM}",CategoryDetailsPM.class);
        bpm = (BrandPM) UIUtil.getPageModel("#{brandPM}",BrandPM.class);
        gpm = (FeatureGroupPM) UIUtil.getPageModel("#{featureGroupPM}",FeatureGroupPM.class);
        fpm = (FeaturePM) UIUtil.getPageModel("#{featurePM}",FeaturePM.class);
       // ipm = (ItemsPM) UIUtil.getPageModel("#{itemsPM}",ItemsPM.class);
       
        session = UIUtil.getSession();
    }
	
	public void selectSubClass(Integer index) {
         
		List<CategoryDetails> selectedCategoryList = pm.getCategorySelectedList();        
        CategoryDetails rootCategory = selectedCategoryList.get(index);        
        Integer selectedClassId = rootCategory.getSelectedCategoryId();
        CategoryDetails selectedChild = rootCategory.getChildren().get(selectedClassId);
        
      //remove rest of the category details
        int size = selectedCategoryList.size();
        for (int i = index + 1; i < size; i++) {
        	selectedCategoryList.remove(selectedCategoryList.size() - 1);
        }
        
        if(selectedChild==null){
        	return;
        } else{        
	        pm.setSelectedCategory(selectedChild);
	        selectedChild.setSelectedCategoryId(0);
	        //add the selected class from this in the hierarchy
	        selectedCategoryList.add(selectedChild);
        }
        session.put(SessionKeys.SELECTED_CATEGORY, UIUtil.getCategoryFromId(selectedChild.getCategoryId() + ""));
        session.put(SessionKeys.SELECTED_CATEGOTY_LIST, selectedCategoryList);
      
        updateCategoryListener();
    }

	public void addCategory(){
		CategoryDetails parentCategory = (CategoryDetails)session.getValueAt(SessionKeys.SELECTED_CATEGORY);		
		CategoryDetailsForPersistance newCategory = pm.getNewCategoryDetails(); 
		if(parentCategory==null){
			parentCategory = pm.getRootCategory();//new CategoryDetails();
			//parentCategory.setCategoryId(1);
		}
		newCategory.setParentId(parentCategory.getCategoryId());
		try{
			newCategory = CategoryDetailsService.addCategory(newCategory);
			CategoryDetails newCateDetails = UIUtil.getCategoryDetailsFromPersistantObject(newCategory);
			newCateDetails.setParent(parentCategory);
			parentCategory.getChildren().put(newCateDetails.getCategoryId(), newCateDetails);
			
			//UIUtil.redirect("categories.xhtml?category="+ newCateDetails.getCategoryId());
			pm.setCategorySelectedList(UIUtil.getClassHirarchyforCategory(""+ newCateDetails.getCategoryId(), null));
			pm.setSelectedCategory(newCateDetails);
			
			session.put(SessionKeys.SELECTED_CATEGORY, newCateDetails);
	        session.put(SessionKeys.SELECTED_CATEGOTY_LIST, pm.getCategorySelectedList());
			
		} catch(Exception e) {
			System.out.println("===================Error while adding the category" + e.getMessage());
		}
	}
	
	public void updateCategoryListener(){
		for(CategoryChangeListners listners: pm.getCategoryListners().values()){
			listners.onChange();
		}
	}
	
	public void deleteCategory(){
		try{			
			if(CategoryDetailsService.deleteCategoryDetails(pm.getSelectedCategory().getDetails())==Status.SUCCESS){
				pm.setSelectedCategory(null);
				pm.setCategorySelectedList(null);
				UIUtil.getSession().put(SessionKeys.SELECTED_CATEGORY, null);
				UIUtil.getSession().put(SessionKeys.SELECTED_CATEGOTY_LIST, null);
				pm.setCategorySelectedList(UIUtil.getClassHirarchyforCategory(null, null));
				System.out.println("===================Category deleted Success");
			}else{
				System.out.println("===================Error while deleting the Category");
			}
		} catch(Exception e) {
			System.out.println("===================Error while deleting the Category" + e.getMessage());
		}
	}
}
