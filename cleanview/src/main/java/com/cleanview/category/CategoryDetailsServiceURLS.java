package com.cleanview.category;

import java.io.Serializable;

import com.cleanview.ui.constants.CommonUIConstants;
import com.cleanview.utilities.UIUtil;

public class CategoryDetailsServiceURLS implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public final static String ADD_CATEGORY = UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("ADD_CATEGORY");
	public final static String FETCH_CATEGORY_BY_ID = UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("FETCH_CATEGORY_BY_ID");
	public final static String FETCH_ALLCATEGORY = UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("FETCH_ALLCATEGORY");
	public final static String UPDATE_CATEGORY = UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("UPDATE_CATEGORY");
	public final static String DELETE_CATEGORY = UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("DELETE_CATEGORY");
}
