package com.cleanview.category;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.cleanview.models.CategoryDetailsForPersistance;
import com.cleanview.ui.constants.SessionKeys;
import com.cleanview.ui.interfaces.CategoryChangeListners;
import com.cleanview.ui.models.CategoryDetails;
import com.cleanview.utilities.UIUtil;

@ViewScoped
@ManagedBean
public class CategoryDetailsPM implements Serializable{

	private static final long serialVersionUID = 1L;

	//list of categories that are selected hierarchally
	private List<CategoryDetails> categorySelectedList = null;
	private String selectedCategoryParam = null;
	
	private CategoryDetails rootCategory = new CategoryDetails();
	
	private CategoryDetails selectedCategory = null;
	
	private CategoryDetailsForPersistance newCategoryDetails = new CategoryDetailsForPersistance();
	
	private String newCatename  = "";
	private String newDesc = "";
	private boolean initDone = false;
	
	private Map<String, CategoryChangeListners> categoryListners = new HashMap<String,CategoryChangeListners>();
	
	
	
	public CategoryDetailsPM(){
		//super();
		categorySelectedList = (List<CategoryDetails>) UIUtil.getSession().getValueAt(SessionKeys.SELECTED_CATEGOTY_LIST);
		if(categorySelectedList==null){
			categorySelectedList = new ArrayList<CategoryDetails>();
			rootCategory = UIUtil.getRootCategories();
			
			if(rootCategory==null){
				UIUtil.getRootCategory(CategoryDetailsService.fetchAllCategories());
				rootCategory = UIUtil.getRootCategories();			
			}
			categorySelectedList.add(rootCategory);
			UIUtil.getSession().put(SessionKeys.SELECTED_CATEGOTY_LIST, categorySelectedList);
		}
		
	}
	public void init(){
		
		if(initDone)
			return;
		
		initDone = true;
		if(selectedCategoryParam==null && selectedCategory == null){
			categorySelectedList = (List<CategoryDetails>) UIUtil.getSession().getValueAt(SessionKeys.SELECTED_CATEGOTY_LIST);
			if(categorySelectedList==null){
				UIUtil.getSession().put(SessionKeys.SELECTED_CATEGOTY_LIST, categorySelectedList);
				categorySelectedList = UIUtil.getClassHirarchyforCategory(null, null);	
			}
			
		}else{
			if(selectedCategoryParam == null){
				selectedCategoryParam = selectedCategory.getCategoryId() + "";
			}
			
			updateFromCategoryId(selectedCategoryParam);
			
			//update all the category change listener
			CategoryDetailsVB cvb = new CategoryDetailsVB();
			cvb.updateCategoryListener();
		}
	}
	public void updateFromCategoryId(String categoryId){
		//create the class hierarchy to be selected in dropdown
		categorySelectedList = UIUtil.getClassHirarchyforCategory(categoryId, null);
		
		//fetch the selected category details
		CategoryDetailsForPersistance cate = new CategoryDetailsForPersistance();
		cate.setCategoryId(Integer.parseInt(categoryId));
		selectedCategory = UIUtil.getCategoryDetailsFromPersistantObject(CategoryDetailsService.fetchCategoryById(cate));
		
		//put the selected category in session
		UIUtil.getSession().put(SessionKeys.SELECTED_CATEGORY, selectedCategory);
	}
	
	public String getClassHirarchy(){
		String result = "";
		for(CategoryDetails cate: categorySelectedList){
			if(cate.getSelectedCategoryId() != -2 && cate.getSelectedCategoryId() != 0)
			result = result + "/" + cate.getSelectedCategoryId();
		}
		return result;
	}
	
	public CategoryDetails getSelectedCategory() {
		return selectedCategory;
	}

	public void setSelectedCategory(CategoryDetails selectedCategory) {
		this.selectedCategory = selectedCategory;
	}

	public CategoryDetailsForPersistance getNewCategoryDetails() {
		return newCategoryDetails;
	}

	public void setNewCategoryDetails(CategoryDetailsForPersistance newCategoryDetails) {
		this.newCategoryDetails = newCategoryDetails;
	}

	public List<CategoryDetails> getCategorySelectedList() {
		return categorySelectedList;
	}

	public void setCategorySelectedList(List<CategoryDetails> categorySelectedList) {
		this.categorySelectedList = categorySelectedList;
	}

	public CategoryDetails getRootCategory() {
		return rootCategory;
	}

	public void setRootCategory(CategoryDetails rootCategory) {
		this.rootCategory = rootCategory;
	}

	public String getNewCatename() {
		return newCatename;
	}

	public void setNewCatename(String newCatename) {
		this.newCatename = newCatename;
	}

	public String getNewDesc() {
		return newDesc;
	}

	public void setNewDesc(String newDesc) {
		this.newDesc = newDesc;
	}

	public String getSelectedCategoryParam() {
		return selectedCategoryParam;
	}

	public void setSelectedCategoryParam(String selectedCategoryParam) {
		this.selectedCategoryParam = selectedCategoryParam;
	}
	public Map<String, CategoryChangeListners> getCategoryListners() {
		return categoryListners;
	}
	public void setCategoryListners(
			Map<String, CategoryChangeListners> categoryListners) {
		this.categoryListners = categoryListners;
	}
}
