package com.cleanview.category;

import java.io.Serializable;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import com.cleanview.models.CategoryDetailsForPersistance;
import com.cleanview.persist.common.Status;

public class CategoryDetailsService implements Serializable{
	
	private static final long serialVersionUID = 1L;

	public static CategoryDetailsForPersistance addCategory(CategoryDetailsForPersistance obj1){
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(CategoryDetailsServiceURLS.ADD_CATEGORY);
		Response res = target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(obj1, MediaType.APPLICATION_XML));
		return res.readEntity(CategoryDetailsForPersistance.class);
	}
	
	public static CategoryDetailsForPersistance fetchCategoryById(CategoryDetailsForPersistance cate){
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(CategoryDetailsServiceURLS.FETCH_CATEGORY_BY_ID);
		Response res = target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(cate, MediaType.APPLICATION_XML));
		return res.readEntity(CategoryDetailsForPersistance.class);
		
	}
	
	public static List<CategoryDetailsForPersistance> fetchAllCategories(){
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(CategoryDetailsServiceURLS.FETCH_ALLCATEGORY);
		
		Response res = target.request(MediaType.APPLICATION_XML).get();
		return res.readEntity(new GenericType<List<CategoryDetailsForPersistance>>(){});
	}
	
	public static Status updateCategoryDetails(CategoryDetailsForPersistance categoryDetails){
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(CategoryDetailsServiceURLS.UPDATE_CATEGORY);	
		Response response = target.request().post(Entity.entity(categoryDetails, MediaType.APPLICATION_XML));
		if(response.getStatus()==200)
			return Status.SUCCESS;
		return Status.FALIUER;
	}
	
	public static Status deleteCategoryDetails(CategoryDetailsForPersistance categoryDetails){
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(CategoryDetailsServiceURLS.DELETE_CATEGORY);	
		Response response = target.request().post(Entity.entity(categoryDetails, MediaType.APPLICATION_XML));
		if(response.getStatus()==200)
			return Status.SUCCESS;
		return Status.FALIUER;
	}
}
