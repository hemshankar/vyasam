package com.cleanview.session;


import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.cleanview.ui.constants.SessionKeys;

@ManagedBean
@SessionScoped

public class CleanViewSession implements Serializable {

	private static final long serialVersionUID = 1L;
	private Map<SessionKeys, Object> m_values;

    public CleanViewSession(){
        m_values = new HashMap<SessionKeys,Object>();        
    }
    
    public void put(SessionKeys key, Object value){
        m_values.put(key, value);
    }
    
    public Map<SessionKeys,Object> getValues() {
        return m_values;
    }
    
    public Object getValueAt(SessionKeys key){
        return m_values.get(key);
    }
    
    public void clear(){
    	m_values.clear();
    }
}
