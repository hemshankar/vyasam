package com.cleanview.compare;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.cleanview.category.CategoryDetailsService;
import com.cleanview.items.ItemsService;
import com.cleanview.models.CategoryDetailsForPersistance;
import com.cleanview.models.FeatureDetails;
import com.cleanview.models.ItemDetails;
import com.cleanview.models.ValueDetails;
import com.cleanview.ui.abstractImpls.AbstractChangeViewListner;
import com.cleanview.ui.constants.SessionKeys;
import com.cleanview.ui.models.CategoryDetails;
import com.cleanview.utilities.UIUtil;
import com.mysql.jdbc.StringUtils;

@ViewScoped
@ManagedBean
public class ComparePM extends AbstractChangeViewListner implements Serializable{

	private static final long serialVersionUID = 1L;

	private Map<String,FeatureDetails> featureDetailsMap = null;
	private String item1input = "";
	private String item2input = "";
	
	private String selectedItemId1= "";
	private String selectedItemId2= "";
	
	private PMIDetails item1 = null;
	private PMIDetails item2 = null;
	private List<FeatureDetails> features = null;
	private CategoryDetails selectedCategory = null;
	private String categoryIdInput = null;
	private boolean initDone = false;
	private List<ItemDetails> itemsList = null;
	private boolean showRandomWeights = false;
	Random r = new Random();
	
	public ComparePM(){
		features = new ArrayList<FeatureDetails>();
		Object randWeight = UIUtil.getSession().getValueAt(SessionKeys.SHOW_RANDOM_WEIGHTS);
		if(randWeight!=null)
			showRandomWeights = (Boolean) randWeight;
	}
	
	public void init(){
	
		if(initDone)
			return;
		initDone = true;
		
		if(categoryIdInput!=null){
			CategoryDetailsForPersistance cate = new CategoryDetailsForPersistance();
			cate.setCategoryId(Integer.parseInt(categoryIdInput));
			selectedCategory = UIUtil.getCategoryDetailsFromPersistantObject(CategoryDetailsService.fetchCategoryById(cate));
			cpm.updateFromCategoryId(categoryIdInput);
		}else{
			selectedCategory = (CategoryDetails) UIUtil.getSession().getValueAt(SessionKeys.SELECTED_CATEGORY);
		}
		if(selectedCategory!=null){
			ItemDetails item = new ItemDetails();
			item.setCategoryId(selectedCategory.getCategoryId());
			itemsList = ItemsService.featchAllItemDetails(item, 0, 1000);

			featureDetailsMap = selectedCategory.getDetails().getFeatureDetails();
			for(String key: featureDetailsMap.keySet()){
				if(!featureDetailsMap.get(key).getHide())
				features.add(featureDetailsMap.get(key));
			}

			ItemDetails i1 = new ItemDetails();
			ItemDetails i2 = new ItemDetails();
			
			if(!StringUtils.isNullOrEmpty(item1input)){
				selectedItemId1 = item1input;
				i1.setItemId(Integer.parseInt(item1input));
				i1.setCategoryId(selectedCategory.getCategoryId());
				i1 = ItemsService.featchItem(i1);
				item1 = new PMIDetails(i1,features,featureDetailsMap);
			}
			if(!StringUtils.isNullOrEmpty(item2input)){
				selectedItemId2 = item2input;
				i2.setItemId(Integer.parseInt(item2input));
				i2.setCategoryId(selectedCategory.getCategoryId());
				i2 = ItemsService.featchItem(i2);
				item2 = new PMIDetails(i2,features,featureDetailsMap);
			}			
		}
	}
	
	
	public static class PMIDetails implements Serializable{
		public class IDData implements Serializable{
			String value;
			Integer weight;
			Integer weightPercent;
			String color = "";
			public IDData(String v, Integer w, Integer wp){
				value = v; weight = w; weightPercent = wp;
				color = UIUtil.getColor(wp);
			}

			public String getValue() {
				return value;
			}

			public void setValue(String value) {
				this.value = value;
			}

			public Integer getWeight() {
				return weight;
			}

			public void setWeight(Integer weight) {
				this.weight = weight;
			}

			public Integer getWeightPercent() {
				return weightPercent;
			}

			public void setWeightPercent(Integer weightPercent) {
				this.weightPercent = weightPercent;
			}

			public String getColor() {
				return color;
			}

			public void setColor(String color) {
				this.color = color;
			}
		}
		private List<IDData> dataList;
		private ItemDetails item = null;
		List<String> images = new ArrayList<String>();
		private List<FeatureDetails> featuresList = null;
		private Map<String,FeatureDetails> featuresMap = null;
		public PMIDetails(ItemDetails iDetails, List<FeatureDetails> fList, Map<String,FeatureDetails> map) {
			featuresMap = map;
			featuresList = fList;
			dataList = new ArrayList<IDData>();	
			item = iDetails;
			for(FeatureDetails key: featuresList){
				
				String value = iDetails.getFeatures().get(key.getFeatureId());	
				ValueDetails vd = featuresMap.get(key.getFeatureId()).getValueDetails().get(value);
				//System.out.println("Feature: " + key.getFeatureId() + " value:" + value + " --" + vd);
				Integer weight = 1;
				Integer percent = 1;
				String valueName = value;
				if(vd!=null){
					weight = vd.getWeight();
					valueName = vd.getValueName();
					Integer max = featuresMap.get(key.getFeatureId()).getMaxValue();
					Integer min = featuresMap.get(key.getFeatureId()).getMinValue();
					if(weight!=null && max!=null && min!=null){
						Integer denom = (max - min);
						if(denom == 0)
							denom = 1;
						percent = ((weight - min) * 100)/ denom;
					}
				}
				
				dataList.add(new IDData(valueName, weight, percent));
				
				images = UIUtil.getImages(iDetails);
			}
		}

		public List<IDData> getDataList() {
			return dataList;
		}

		public void setDataList(List<IDData> dataList) {
			this.dataList = dataList;
		}

		public ItemDetails getItem() {
			return item;
		}

		public void setItem(ItemDetails item) {
			this.item = item;
		}

		public List<String> getImages() {
			return images;
		}

		public void setImages(List<String> images) {
			this.images = images;
		}
	}

	public Map<String, FeatureDetails> getFeatureDetailsMap() {
		return featureDetailsMap;
	}

	public void setFeatureDetailsMap(Map<String, FeatureDetails> featureDetailsMap) {
		this.featureDetailsMap = featureDetailsMap;
	}

	public PMIDetails getItem1() {
		return item1;
	}

	public void setItem1(PMIDetails item1) {
		this.item1 = item1;
	}

	public PMIDetails getItem2() {
		return item2;
	}

	public void setItem2(PMIDetails item2) {
		this.item2 = item2;
	}

	public List<FeatureDetails> getFeatures() {
		return features;
	}

	public void setFeatures(List<FeatureDetails> features) {
		this.features = features;
	}

	public CategoryDetails getSelectedCategory() {
		return selectedCategory;
	}

	public void setSelectedCategory(CategoryDetails selectedCategory) {
		this.selectedCategory = selectedCategory;
	}

	public String getItem1input() {
		return item1input;
	}

	public void setItem1input(String item1input) {
		this.item1input = item1input;
	}

	public String getItem2input() {
		return item2input;
	}

	public void setItem2input(String item2input) {
		this.item2input = item2input;
	}

	public String getCategoryIdInput() {
		return categoryIdInput;
	}

	public void setCategoryIdInput(String categoryIdInput) {
		this.categoryIdInput = categoryIdInput;
	}

	public List<ItemDetails> getItemsList() {
		return itemsList;
	}

	public void setItemsList(List<ItemDetails> itemsList) {
		this.itemsList = itemsList;
	}

	public String getSelectedItemId1() {
		return selectedItemId1;
	}

	public void setSelectedItemId1(String selectedItemId1) {
		this.selectedItemId1 = selectedItemId1;
	}

	public String getSelectedItemId2() {
		return selectedItemId2;
	}

	public void setSelectedItemId2(String selectedItemId2) {
		this.selectedItemId2 = selectedItemId2;
	}

	@Override
	public void onChange() {
		item1input = null;
		item2input = null;
		item1 = null;
		item2 = null;
		categoryIdInput = null;
		selectedItemId1 = null;
		selectedItemId2 = null;
		if(features!=null)
				features.clear();
		if(featureDetailsMap!=null)
			featureDetailsMap.clear();
		initDone=false;
		init();
	}

	@Override
	public String getId() {
		// TODO Auto-generated method stub
		return null;
	}

	public Integer getRandomWeight(){
		int Low = 10;
		int High = 100;
		return r.nextInt(High-Low) + Low;
	}

	public boolean isShowRandomWeights() {
		return showRandomWeights;
	}

	public void setShowRandomWeights(boolean showRandomWeights) {
		this.showRandomWeights = showRandomWeights;
	}
	
}
