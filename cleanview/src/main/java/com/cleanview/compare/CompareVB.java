package com.cleanview.compare;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.cleanview.category.CategoryDetailsPM;
import com.cleanview.session.CleanViewSession;
import com.cleanview.ui.constants.SessionKeys;
import com.cleanview.utilities.UIUtil;


@RequestScoped
@ManagedBean
public class CompareVB {
	
	ComparePM pm = null;
	CleanViewSession session = null;
	CategoryDetailsPM cpm = null;
	public CompareVB(){
		pm = (ComparePM) UIUtil.getPageModel("#{comparePM}",ComparePM.class);
		cpm = (CategoryDetailsPM) UIUtil.getPageModel("#{categoryDetailsPM}",CategoryDetailsPM.class);
	    session = UIUtil.getSession();
	}
	
	public void updateItem1(){
//		ItemDetails i1 = new ItemDetails();
//		String item1Id = pm.getSelectedItemId1();
		String page = "compare.xhtml?category=" +  pm.getSelectedCategory().getCategoryId() 
						+ "&item1=" + pm.getSelectedItemId1()
						+ "&item2=" + pm.getSelectedItemId2();
//		if(!StringUtils.isNullOrEmpty(item1Id)){
//			
//			i1.setItemId(Integer.parseInt(item1Id));
//			i1.setCategoryId(pm.getSelectedCategory().getCategoryId());
//			i1 = ItemsService.featchItem(i1);
//			pm.setItem1(new ComparePM.PMIDetails(i1,pm.getFeatures(),pm.getFeatureDetailsMap()));
			
		
		UIUtil.redirect(page);
			
		//}
	}
	public void updateItem2(){
		/*ItemDetails i2 = new ItemDetails();
		String item2Id = pm.getSelectedItemId2();
		if(!StringUtils.isNullOrEmpty(item2Id)){
			
			i2.setItemId(Integer.parseInt(item2Id));
			i2.setCategoryId(pm.getSelectedCategory().getCategoryId());
			i2 = ItemsService.featchItem(i2);
			pm.setItem2(new PMIDetails(i2,pm.getFeatures(),pm.getFeatureDetailsMap()));
		}*/
		String page = "compare.xhtml?category=" +  pm.getSelectedCategory().getCategoryId() 
				+ "&item1=" + pm.getSelectedItemId1()
				+ "&item2=" + pm.getSelectedItemId2();
		UIUtil.redirect(page);
	}
	
	public void refreshCategories(){
		String page = "compare.xhtml?category=" +  cpm.getSelectedCategory().getCategoryId();
		UIUtil.redirect(page);
	}
	
	public void showHidePage(){
		String page = "compare.xhtml?category=" +  pm.getSelectedCategory().getCategoryId() 
				+ "&item1=" + pm.getSelectedItemId1()
				+ "&item2=" + pm.getSelectedItemId2();
		Object randWeight = UIUtil.getSession().getValueAt(SessionKeys.SHOW_RANDOM_WEIGHTS);
		Boolean showRandomWeight = false;
		if(randWeight!=null)
			showRandomWeight = (Boolean) randWeight;
		
		if(showRandomWeight){
			UIUtil.getSession().put(SessionKeys.SHOW_RANDOM_WEIGHTS, false);
		}else{
			UIUtil.getSession().put(SessionKeys.SHOW_RANDOM_WEIGHTS, true);
		}
			
		UIUtil.redirect(page);
	}
}
