package com.cleanview.features;

import java.io.Serializable;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import com.cleanview.models.FeatureDetails;
import com.cleanview.persist.common.Status;

public class FeatureService implements Serializable{
	
	private static final long serialVersionUID = 1L;

	public static Status addFeature(FeatureDetails obj1){
				
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(FeatureServiceURLS.ADD_FEATURE);
		
		Response res = target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(obj1, MediaType.APPLICATION_XML));
		if(res.getStatus()==200)
			return Status.SUCCESS;
		return Status.FALIUER;
		
		
	}
	
	public static List<FeatureDetails> fetchAllFeatures(FeatureDetails feature){
		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(FeatureServiceURLS.FETCH_ALL_FEATURES);
		
		Response res = target.request(MediaType.APPLICATION_XML).
				post(Entity.entity(feature, MediaType.APPLICATION_XML));
		
		return res.readEntity(new GenericType<List<FeatureDetails>>(){});
	}
	
	public static Status updateFeatureDetails(FeatureDetails featureDetails,String OriginalFeatureId){
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(FeatureServiceURLS.UPDATE_FEATURE);
		
		Response res = target.queryParam("originalFeatureId", OriginalFeatureId).request(MediaType.APPLICATION_XML).
					post(Entity.entity(featureDetails, MediaType.APPLICATION_XML));
		if(res.getStatus()==200)
			return Status.SUCCESS;
		return Status.FALIUER;
		
	}
	
	public static Status deleteFeatureDetails(FeatureDetails featureDetails){
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(FeatureServiceURLS.DELETE_FEATURE);
		
		Response res = target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(featureDetails, MediaType.APPLICATION_XML));
		if(res.getStatus()==200)
			return Status.SUCCESS;
		return Status.FALIUER;
		
	}
}
