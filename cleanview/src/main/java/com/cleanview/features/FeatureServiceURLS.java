package com.cleanview.features;

import java.io.Serializable;

import com.cleanview.ui.constants.CommonUIConstants;
import com.cleanview.utilities.UIUtil;

public class FeatureServiceURLS implements Serializable{
	
	private static final long serialVersionUID = 1L;
	public static final String UPDATE_FEATURE = UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("UPDATE_FEATURE");
	public static final String ADD_FEATURE = UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("ADD_FEATURE");
	public static final String FETCH_ALL_FEATURES = UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("FETCH_ALL_FEATURES");		
	public static final String DELETE_FEATURE = UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("DELETE_FEATURE");
}
