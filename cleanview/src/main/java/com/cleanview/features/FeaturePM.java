package com.cleanview.features;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;






import com.cleanview.models.FeatureDetails;
import com.cleanview.ui.abstractImpls.AbstractChangeViewListner;
import com.cleanview.ui.constants.SessionKeys;
import com.cleanview.ui.models.CategoryDetails;
import com.cleanview.utilities.UIUtil;

@ViewScoped
@ManagedBean
public class FeaturePM extends AbstractChangeViewListner implements Serializable{

	private static final long serialVersionUID = 1L;
	private List<PMFeatureDetails> featureList = null;
	private Map<String,FeatureDetails> featureMap = null;
	private CategoryDetails selectedCategory = null;
	private FeatureDetails selectedFeature = null;
	private FeatureDetails newFeature = null;
	public FeaturePM(){
		super();
		newFeature = new FeatureDetails();		
		selectedFeature = new FeatureDetails();
		useSelectedCategory();
		
	}
	
	public void useSelectedCategory(){
		selectedCategory = (CategoryDetails) UIUtil.getSession().getValueAt(SessionKeys.SELECTED_CATEGORY);
		if(selectedCategory == null)
			return;
		featureMap = selectedCategory.getDetails().getFeatureDetails();
		featureList = mapToList(featureMap);
	}

	private List<PMFeatureDetails> mapToList(Map<String,FeatureDetails> map){
		List<PMFeatureDetails> list = new ArrayList<PMFeatureDetails>();
		
		for(String key: map.keySet()){
			list.add(new PMFeatureDetails(key,map.get(key)));
		}
		return list;
	}
	
	public List<PMFeatureDetails> getFeaturesList() {
		return featureList;
	}

	public void setFeaturesList(List<PMFeatureDetails> featureslst) {
		this.featureList = featureslst;
	}

	public CategoryDetails getSelectedCategory() {
		return selectedCategory;
	}

	public void setSelectedCategory(CategoryDetails selectedCategory) {
		this.selectedCategory = selectedCategory;
	}

	public FeatureDetails getSelectedFeature() {
		return selectedFeature;
	}

	public void setSelectedFeature(FeatureDetails selectedFeature) {
		this.selectedFeature = selectedFeature;
	}

	public FeatureDetails getNewFeature() {
		return newFeature;
	}

	public void setNewFeature(FeatureDetails newFtr) {
		this.newFeature = newFtr;
	}

	public Map<String, FeatureDetails> getFeaturesMap() {
		return featureMap;
	}

	public void setFeaturesMap(Map<String, FeatureDetails> ftrMap) {
		this.featureMap = ftrMap;
	}
	
	public class PMFeatureDetails implements Serializable{
	
		private static final long serialVersionUID = 1L;
		FeatureDetails featureDetails = null;
		String originalName = null;
		
		public PMFeatureDetails(String origName, FeatureDetails gd){
			originalName = origName;
			featureDetails = gd;
		}

		public FeatureDetails getFeatureDetails() {
			return featureDetails;
		}

		public void setFeatureDetails(FeatureDetails ftrDetails) {
			this.featureDetails = ftrDetails;
		}

		public String getOriginalName() {
			return originalName;
		}

		public void setOriginalName(String originalName) {
			this.originalName = originalName;
		}
	}
	
	public PMFeatureDetails newPMFeatureDetails(String featureId, FeatureDetails fd){
		return new PMFeatureDetails(featureId, fd);
	}
	
	@Override
	public void onChange(){
		useSelectedCategory();
	}
	
	@Override
	public String getId(){
		return FeaturePM.class.getName();
	}
}
