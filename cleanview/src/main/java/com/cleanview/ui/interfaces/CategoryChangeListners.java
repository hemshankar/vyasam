package com.cleanview.ui.interfaces;


public interface CategoryChangeListners {

	public abstract void onChange();
	public abstract String getId();
	
}
