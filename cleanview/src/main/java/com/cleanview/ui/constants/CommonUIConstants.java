package com.cleanview.ui.constants;

public class CommonUIConstants {
	
	//========================Connection Related
	public final static String SERVER_HOSTNAME = "SERVER_HOSTNAME";
	public final static String SERVER_PORT = "SERVER_PORT";
	public final static String APP_HOME="APP_HOME";
	public final static String PERSIST_SERVICES_HOME = "PERSIST_SERVICES_HOME";
}
