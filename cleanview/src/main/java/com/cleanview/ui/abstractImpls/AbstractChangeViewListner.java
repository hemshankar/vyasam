package com.cleanview.ui.abstractImpls;

import java.io.Serializable;

import com.cleanview.category.CategoryDetailsPM;
import com.cleanview.ui.interfaces.CategoryChangeListners;
import com.cleanview.utilities.UIUtil;

public abstract class AbstractChangeViewListner implements Serializable, CategoryChangeListners {


	private static final long serialVersionUID = 1L;
	public CategoryDetailsPM cpm = (CategoryDetailsPM) UIUtil.getPageModel("#{categoryDetailsPM}",CategoryDetailsPM.class);
	
	public AbstractChangeViewListner(){
		register();
	}
	
	public void register(){	
		cpm.getCategoryListners().put(getId(), this);
	}
	
	public abstract String getId();
}
