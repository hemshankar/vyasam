package com.cleanview.ui.models;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.cleanview.models.CategoryDetailsForPersistance;

public class CategoryDetails implements Serializable{
		
	private static final long serialVersionUID = 1L;
	
	private Integer categoryId;
	private String categoryName;
	
	private Map<Integer,CategoryDetails> children = new HashMap<Integer, CategoryDetails>();
	private CategoryDetails parent;
	
	private CategoryDetailsForPersistance details;

	private Integer selectedCategoryId = 0;
	
	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Map<Integer,CategoryDetails> getChildren() {
		return children;
	}

	public void setChildren(Map<Integer,CategoryDetails> children) {
		this.children = children;
	}

	public CategoryDetails getParent() {
		return parent;
	}

	public void setParent(CategoryDetails parent) {
		this.parent = parent;
	}

	public CategoryDetailsForPersistance getDetails() {
		return details;
	}

	public void setDetails(CategoryDetailsForPersistance details) {
		this.details = details;
	}

	public Integer getSelectedCategoryId() {
		return selectedCategoryId;
	}

	public void setSelectedCategoryId(Integer selectedCategorySNo) {
		this.selectedCategoryId = selectedCategorySNo;
	}		
}
