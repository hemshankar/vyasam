package com.cleanview.values;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.cleanview.category.CategoryDetailsService;
import com.cleanview.models.CategoryDetailsForPersistance;
import com.cleanview.models.FeatureDetails;
import com.cleanview.models.ValueDetails;
import com.cleanview.ui.abstractImpls.AbstractChangeViewListner;
import com.cleanview.ui.constants.SessionKeys;
import com.cleanview.ui.models.CategoryDetails;
import com.cleanview.utilities.UIUtil;
import com.mysql.jdbc.StringUtils;

@ViewScoped
@ManagedBean
public class ValuesPM extends AbstractChangeViewListner implements Serializable {

	private static final long serialVersionUID = 1L;

//	private List<String,List<PMValueDetails>> featureValueDetails = null;
	
	private List<PMValueDetails> valueList = null;
	//private Map<String,List<PMValueDetails>> groupedValueListByName = null;
	private List<ValueGroup> groupedValueListByName = null;
	private Map<String, ValueDetails> valueMap = null;
	private CategoryDetails selectedCategory = null;
	private FeatureDetails selectedFeature = null;
	private String selectedFeatureParam = "";
	private String selectedCategoryParam = "";
	private ValueDetails newValue = null;
	private ValueGroup newGroupValue = null;
	private Boolean init = false;
	
	public ValuesPM() {
		newValue = new ValueDetails();
	}
	
	public void init() {
		// useSelectedCategory();
		if(init)
			return;
		
		init = true;
		selectedCategory = (CategoryDetails) UIUtil.getSession().getValueAt(
				SessionKeys.SELECTED_CATEGORY);

		if (!StringUtils.isNullOrEmpty(selectedFeatureParam)
				&& !StringUtils.isNullOrEmpty(selectedCategoryParam)) {
			
			CategoryDetailsForPersistance cate = new CategoryDetailsForPersistance();
			cate.setCategoryId(Integer.parseInt(selectedCategoryParam));
			selectedCategory = UIUtil
					.getCategoryDetailsFromPersistantObject(CategoryDetailsService
							.fetchCategoryById(cate));
		}
		selectedFeature = selectedCategory.getDetails().getFeatureDetails()
				.get(selectedFeatureParam);
		UIUtil.getSession()
				.put(SessionKeys.SELECTED_CATEGORY, selectedCategory);
		UIUtil.getSession().put(SessionKeys.SELECTED_FEATURE, selectedFeature);
		useSelectedCategory();
	}

	public void useSelectedCategory() {
		// selectedCategory = (CategoryDetails)
		// UIUtil.getSession().getValueAt(SessionKeys.SELECTED_CATEGORY);
		// selectedFeature = (FeatureDetails)
		// UIUtil.getSession().getValueAt(SessionKeys.SELECTED_FEATURE);
		if (selectedCategory == null || selectedFeature == null)
			return;
		valueMap = selectedFeature.getValueDetails();
		valueList = mapToList(valueMap);
		groupedValueListByName = groupValuesBasedOnNames(valueList);
		newGroupValue = new ValueGroup();
	}

	public List<ValueGroup> groupValuesBasedOnNames(List<PMValueDetails> vList){		
		Map<String,List<PMValueDetails>> groupedMap = new HashMap<String, List<PMValueDetails>>();
		List<ValueGroup> groupList = new ArrayList<ValuesPM.ValueGroup>();
		for(PMValueDetails value: vList){
			String valueName = value.getvDetails().getValueName();
			List<PMValueDetails> lst = groupedMap.get(valueName);
			if(lst==null){
				lst = new ArrayList<PMValueDetails>();
				groupedMap.put(valueName, lst);
			}
			lst.add(value);
		}
		
		ValueGroup group = null;
		for(String valueName: groupedMap.keySet()){
			group = new ValueGroup();
			group.setValueName(valueName);
			group.setListOfValues(groupedMap.get(valueName));
			
			Integer weight = groupedMap.get(valueName).get(0).getvDetails().getWeight();
			if(weight==null)
				weight = 0;
			
			group.setGroupWeight(weight);
			group.setDesc(groupedMap.get(valueName).get(0).getvDetails().getDescription());
			groupList.add(group);
		}
		
		return groupList;
	}
	
	public List<PMValueDetails> mapToList(Map<String, ValueDetails> map) {
		List<PMValueDetails> vList = new ArrayList<PMValueDetails>();

		for (String key : map.keySet()) {
			vList.add(new PMValueDetails(map.get(key)));
		}
		return vList;
	}

	public void listToMap() {
		for (PMValueDetails vd : valueList) {
			valueMap.put(vd.getOriginalName(), vd.getvDetails());
		}
	}

	public class ValueGroup implements Serializable{
		String valueName = "";
		List<PMValueDetails> listOfValues = new ArrayList<ValuesPM.PMValueDetails>();
		Integer groupWeight = 0;
		String desc="";
		public String getValueName() {
			return valueName;
		}
		public void setValueName(String valueName) {
			this.valueName = valueName;
		}
		public List<PMValueDetails> getListOfValues() {
			return listOfValues;
		}
		public void setListOfValues(List<PMValueDetails> listOfValues) {
			this.listOfValues = listOfValues;
		}
		public Integer getGroupWeight() {
			return groupWeight;
		}
		public void setGroupWeight(Integer groupWeight) {
			this.groupWeight = groupWeight;
		}
		public String getDesc() {
			return desc;
		}
		public void setDesc(String desc) {
			this.desc = desc;
		}
	}
	
	public class PMValueDetails implements Serializable {
		ValueDetails vDetails = null;
		String originalName = "";

		public PMValueDetails(ValueDetails vD) {
			vDetails = vD;
			originalName = vDetails.getValue();
		}

		public ValueDetails getvDetails() {
			return vDetails;
		}

		public void setvDetails(ValueDetails vDetails) {
			this.vDetails = vDetails;
		}

		public String getOriginalName() {
			return originalName;
		}

		public void setOriginalName(String originalName) {
			this.originalName = originalName;
		}

	}

	public List<PMValueDetails> getValueList() {
		return valueList;
	}

	public void setValueList(List<PMValueDetails> valueList) {
		this.valueList = valueList;
	}

	public Map<String, ValueDetails> getValueMap() {
		return valueMap;
	}

	public void setValueMap(Map<String, ValueDetails> valueMap) {
		this.valueMap = valueMap;
	}

	public CategoryDetails getSelectedCategory() {
		return selectedCategory;
	}

	public void setSelectedCategory(CategoryDetails selectedCategory) {
		this.selectedCategory = selectedCategory;
	}

	public FeatureDetails getSelectedFeature() {
		return selectedFeature;
	}

	public void setSelectedFeature(FeatureDetails selectedFeature) {
		this.selectedFeature = selectedFeature;
	}

	public ValueDetails getNewValue() {
		return newValue;
	}

	public void setNewValue(ValueDetails newValue) {
		this.newValue = newValue;
	}

	public String getSelectedFeatureParam() {
		return selectedFeatureParam;
	}

	public void setSelectedFeatureParam(String selectedFeatureParam) {
		this.selectedFeatureParam = selectedFeatureParam;
	}

	public String getSelectedCategoryParam() {
		return selectedCategoryParam;
	}

	public void setSelectedCategoryParam(String selectedCategoryParam) {
		this.selectedCategoryParam = selectedCategoryParam;
	}

	@Override
	public void onChange() {
		useSelectedCategory();
	}

	@Override
	public String getId() {
		return ValuesPM.class.getName();
	}

	public List<ValueGroup> getGroupedValueListByName() {
		return groupedValueListByName;
	}

	public void setGroupedValueListByName(List<ValueGroup> groupedValueListByName) {
		this.groupedValueListByName = groupedValueListByName;
	}
	
	public ValueGroup getNewGroupValue() {
		return newGroupValue;
	}

	public void setNewGroupValue(ValueGroup newGroupValue) {
		this.newGroupValue = newGroupValue;
	}
}
