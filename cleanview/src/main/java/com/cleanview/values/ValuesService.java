package com.cleanview.values;

import java.io.Serializable;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import com.cleanview.models.ValueDetails;
import com.cleanview.persist.common.Status;

public class ValuesService implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	//?value=" + originalValue;
	
	public static Status addValue(ValueDetails obj1){
				
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(ValuesServiceURLS.ADD_VALUE);
		
		Response res = target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(obj1, MediaType.APPLICATION_XML));
		
		if(res.getStatus()==200){
			return Status.SUCCESS;
		}
		return Status.FALIUER;
	}
		
	public static List<ValueDetails> fetchAllValues(ValueDetails value){
		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(ValuesServiceURLS.FETCH_ALL_VALUES);
		
		Response res = target.request(MediaType.APPLICATION_XML).
				post(Entity.entity(value, MediaType.APPLICATION_XML));
		
		return res.readEntity(new GenericType<List<ValueDetails>>(){});
	}
	
	public static Status updateValueDetails(ValueDetails valueDetails,String originalValue){
		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(ValuesServiceURLS.UPDATE_VALUE);
		
		Response res = target.queryParam("value", originalValue).request(MediaType.APPLICATION_XML).
					post(Entity.entity(valueDetails, MediaType.APPLICATION_XML));
		
		if(res.getStatus()==200){
			return Status.SUCCESS;
		}
		return Status.FALIUER;
	}
	
	public static Status deleteValueDetails(ValueDetails valueDetails){
		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(ValuesServiceURLS.DELETE_VALUE);
		
		Response res = target.request(MediaType.APPLICATION_XML).
					post(Entity.entity(valueDetails, MediaType.APPLICATION_XML));
		
		if(res.getStatus()==200){
			return Status.SUCCESS;
		}
		return Status.FALIUER;
	}
}
