package com.cleanview.values;

import java.io.Serializable;

import com.cleanview.ui.constants.CommonUIConstants;
import com.cleanview.utilities.UIUtil;

public class ValuesServiceURLS implements Serializable{
	
	private static final long serialVersionUID = 1L;
	public static String ADD_VALUE = UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("ADD_VALUE");	
	public static String FETCH_ALL_VALUES = UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("FETCH_ALL_VALUES");
	public static String UPDATE_VALUE = UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("UPDATE_VALUE");
	public static String DELETE_VALUE = UIUtil.getConfig(CommonUIConstants.PERSIST_SERVICES_HOME) + UIUtil.getConfig("DELETE_VALUE");
}
