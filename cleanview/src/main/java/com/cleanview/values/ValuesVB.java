package com.cleanview.values;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.cleanview.features.FeaturePM;
import com.cleanview.models.FeatureDetails;
import com.cleanview.models.ValueDetails;
import com.cleanview.persist.common.Status;
import com.cleanview.session.CleanViewSession;
import com.cleanview.utilities.UIUtil;
import com.cleanview.values.ValuesPM.PMValueDetails;
import com.cleanview.values.ValuesPM.ValueGroup;


@ManagedBean
@RequestScoped
public class ValuesVB implements Serializable{
	
	private static final long serialVersionUID = 1L;
	FeaturePM fpm = null;
	CleanViewSession session = null;
	ValuesPM pm = null;
	public ValuesVB(){
        fpm = (FeaturePM) UIUtil.getPageModel("#{featurePM}",FeaturePM.class);    
        pm = (ValuesPM) UIUtil.getPageModel("#{valuesPM}",ValuesPM.class);        
        session = UIUtil.getSession();
    }
	
	public void addValue(){				
		ValueDetails newValue = pm.getNewValue();
		newValue.setCategoryId(pm.getSelectedCategory().getCategoryId());
		newValue.setFeatureId(pm.getSelectedFeature().getFeatureId());
		
		try{
			if(ValuesService.addValue(newValue)==Status.SUCCESS){
				
				pm.getValueMap().put(newValue.getValue(), newValue);
				pm.getValueList().add(pm.new PMValueDetails(newValue));
				pm.setNewValue(new ValueDetails());
				pm.setGroupedValueListByName(pm.groupValuesBasedOnNames(pm.getValueList()));
				System.out.println("===================Added value successfully");
			}else{
				System.out.println("===================Error while adding the  value");
			}
			
		} catch(Exception e) {
			System.out.println("===================Error while adding the  value" + e.getMessage());
		}
	}
	
	public void updateValue(Integer index1,Integer index2 ){
		try{
			ValueGroup valueGroup = pm.getGroupedValueListByName().get(index1);
			
			PMValueDetails value = valueGroup.getListOfValues().get(index2);	//pm.getValueList().get(index);
			ValueGroup assignedGroup =  getValueGroup(value.getvDetails().getValueName());
			if(assignedGroup!=null){
				value.getvDetails().setDescription(assignedGroup.getDesc());
				value.getvDetails().setWeight(assignedGroup.getGroupWeight());
				pm.useSelectedCategory();
			}
			updateValue(value);
		} catch(Exception e) {
			System.out.println("===================Error while updating the value" + e.getMessage());
		}
	}
	
	public void updateNameAndWeight(Integer index){
		ValueGroup valueGroup = pm.getGroupedValueListByName().get(index);
		if(valueGroup==null)
			return;
		for(PMValueDetails vd: valueGroup.getListOfValues()){
			try{
				vd.getvDetails().setWeight(valueGroup.getGroupWeight());
				vd.getvDetails().setValueName(valueGroup.getValueName());
				vd.getvDetails().setDescription(valueGroup.getDesc());
				updateValue(vd);
				pm.useSelectedCategory();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	public ValueGroup getValueGroup(String valueName){
		List<ValueGroup> valueGroupList = pm.getGroupedValueListByName();
		
		for(ValueGroup vg:  valueGroupList){
			if(vg.getValueName().equals(valueName)){
				return vg;
			}
		}
		return null;
	}
	
	public void updateValue(PMValueDetails value){
		FeatureDetails fd = pm.getSelectedFeature();
		if(ValuesService.updateValueDetails(value.getvDetails(),value.getOriginalName())==Status.SUCCESS){
			
			if(!value.getvDetails().getValue().equals(value.getOriginalName())){
				fd.getValueDetails().remove(value.getOriginalName());
			}
			value.setOriginalName(value.getvDetails().getValue());
			fd.getValueDetails().put(value.getOriginalName(), value.getvDetails());				
		
			System.out.println("===================Update success");
		}else{
			System.out.println("===================Error while updating the value");
		}
	}
	
	public void deleteValue(Integer index1,Integer index2){
		try{
			ValueGroup valueGroup = pm.getGroupedValueListByName().get(index1);
			
			PMValueDetails value = valueGroup.getListOfValues().get(index2);	//pm.getValueList().get(index);
			if(ValuesService.deleteValueDetails(value.getvDetails())==Status.SUCCESS){
				valueGroup.getListOfValues().remove(value);
				pm.getValueList().remove(value);
				pm.setGroupedValueListByName(pm.groupValuesBasedOnNames(pm.getValueList()));
				System.out.println("===================Value deleted successfully");
			}else{
				System.out.println("===================Error while deleting the value");
			}
		} catch(Exception e) {
			System.out.println("===================Error while deleting the value" + e.getMessage());
		}
	}
	
	public void createNewGroup(){
		List<ValueGroup> valueGroupList = pm.getGroupedValueListByName();
		valueGroupList.add(pm.getNewGroupValue());
		pm.setNewGroupValue(pm.new ValueGroup());
	}
}
