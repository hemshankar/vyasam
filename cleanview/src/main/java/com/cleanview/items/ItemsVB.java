package com.cleanview.items;

import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.cleanview.category.CategoryDetailsPM;
import com.cleanview.items.ItemsPM.PMItemDetails;
import com.cleanview.models.ItemDetails;
import com.cleanview.persist.common.Status;
import com.cleanview.session.CleanViewSession;
import com.cleanview.utilities.UIUtil;


@ManagedBean
@RequestScoped
public class ItemsVB implements Serializable{
	
	private static final long serialVersionUID = 1L;
	ItemsPM pm = null;
	CategoryDetailsPM cpm=null;
	CleanViewSession session = null;
	public ItemsVB(){
        pm = (ItemsPM) UIUtil.getPageModel("#{itemsPM}",ItemsPM.class);  
        cpm = (CategoryDetailsPM) UIUtil.getPageModel("#{categoryDetailsPM}",CategoryDetailsPM.class);     
        session = UIUtil.getSession();
    }
	
	public void addItem(){				
		PMItemDetails newItem = pm.getPMNewItem();
		//set category Id
		newItem.getiDetails().setCategoryId(pm.getSelectedCategory().getCategoryId());
		
		//set feature and values
		newItem.updateFeaturesFromList();
		
		//set primary images location1
		String folderName = "images" + cpm.getClassHirarchy();
		newItem.getiDetails().setPrimaryImages(folderName);
		
		try{
			newItem.setiDetails(ItemsService.addItemDetails(newItem.getiDetails()));
			//newItem.getiDetails().setPrimaryImages(folderName);
			pm.getPMItemsList().add(newItem);
			PMItemDetails tmp = pm.newPMItemDetails(new ItemDetails());
			pm.setPMNewItem(tmp);
		} catch(Exception e) {
			System.out.println("===================Error while adding the item" + e.getMessage());
		}
	}
	
	public void updateItem(Integer index){
		try{
			PMItemDetails pmids = pm.getPMItemsList().get(index);
			pmids.updateFeaturesFromList();
			ItemDetails item = pm.getPMItemsList().get(index).iDetails;
			if(ItemsService.updateItemDetails(item)==Status.SUCCESS){				
				System.out.println("===================Item update Success");
				String URL = "itemsUpdate.xhtml?category=" + pm.getSelectedCategory().getCategoryId();
				UIUtil.redirect(URL);
			}else{
				System.out.println("===================Error while updating the item");
			}
		} catch(Exception e) {
			System.out.println("===================Error while updating the item" + e.getMessage());
		}
	}
	
	public void deleteItem(Integer index){
		try{
			PMItemDetails pmids = pm.getPMItemsList().get(index);
			pmids.updateFeaturesFromList();
			ItemDetails item = pm.getPMItemsList().get(index).iDetails;
			if(ItemsService.deleteItemDetails(item)==Status.SUCCESS){
				pm.getPMItemsList().remove(index);
				System.out.println("===================Item delete Success");
				String URL = "itemsUpdate.xhtml?category=" + pm.getSelectedCategory().getCategoryId();
				UIUtil.redirect(URL);
			}else{
				System.out.println("===================Error while deleting the item");
			}
		} catch(Exception e) {
			System.out.println("===================Error while deleting the item" + e.getMessage());
		}
	}
	
	public void upload(PMItemDetails pmItemDetails) {
	    try {
	      
		  InputStream input = pmItemDetails.getFile().getInputStream();
		  ItemDetails iDetails = pmItemDetails.getiDetails();
		  
		  //form directory path
		  String folderName = pmItemDetails.getiDetails().getPrimaryImages() 
				  + File.separator + iDetails.getItemId();
		  String imageName = "img" + iDetails.getItemId() + "_" + pmItemDetails.getImages().size() + ".jpg";
		  
		  //create directory
		  Path path = Paths.get(UIUtil.getRootPath() + "/resources/" + folderName);		  
		  java.nio.file.Files.createDirectories(path);
		  		  
		  //save file in the directory
		  File imageFile = new File(UIUtil.getRootPath() + "/resources/" + folderName ,imageName);
		  Files.copy(input, imageFile.toPath(),StandardCopyOption.REPLACE_EXISTING);
		  
		  pmItemDetails.getImages().add(imageName);
		  
	    } catch (Exception e) {
	      System.out.println(e.getLocalizedMessage());
	      e.printStackTrace();
	    }
	  }
	
	public void doNothing(){
		pm.useSelectedCategory();
	}
}
