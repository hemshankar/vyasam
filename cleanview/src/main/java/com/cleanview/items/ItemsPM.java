package com.cleanview.items;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.servlet.http.Part;

import com.cleanview.models.FeatureDetails;
import com.cleanview.models.ItemDetails;
import com.cleanview.ui.abstractImpls.AbstractChangeViewListner;
import com.cleanview.ui.constants.SessionKeys;
import com.cleanview.ui.models.CategoryDetails;
import com.cleanview.utilities.UIUtil;

@ViewScoped
@ManagedBean
public class ItemsPM extends AbstractChangeViewListner implements Serializable{

	private static final long serialVersionUID = 1L;	
	private CategoryDetails selectedCategory = null;
	private List<String> featuresList = new ArrayList<String>();
	private Map<String,FeatureDetails> featuresDetails = null;
	private ItemDetails selectedItem = null;
	private PMItemDetails newPMItem = null;
	private List<PMItemDetails> pmItemList = new ArrayList<PMItemDetails>();
	private String categoryIdInput = null;
	
	
	public ItemsPM(){
		super();
		useSelectedCategory();
	}
	
	public void init(){
		
	}
	
	public void useSelectedCategory(){
		selectedCategory = (CategoryDetails) UIUtil.getSession().getValueAt(SessionKeys.SELECTED_CATEGORY);
		featuresList.clear();
		pmItemList.clear();
		
		if(selectedCategory == null){
//			newPMItem = new PMItemDetails(new ItemDetails());		
//			selectedItem = new ItemDetails();
			return;	
		}	
		ItemDetails item = new ItemDetails();
		item.setCategoryId(selectedCategory.getCategoryId());
		List<ItemDetails> itemsList = ItemsService.featchAllItemDetails(item, 0, 1000);
		featuresDetails = selectedCategory.getDetails().getFeatureDetails();
		featuresList.addAll(featuresDetails.keySet());
		for(ItemDetails ids: itemsList){
			pmItemList.add(new PMItemDetails(ids));
		}
		
		newPMItem = new PMItemDetails(new ItemDetails());		
		selectedItem = new ItemDetails();
	}
	
	public List<PMItemDetails> getPMItemsList() {
		return pmItemList;
	}
	public void setPMItemsList(List<PMItemDetails> iList) {
		this.pmItemList = iList;
	}
	public CategoryDetails getSelectedCategory() {
		return selectedCategory;
	}
	public void setSelectedCategory(CategoryDetails selectedCategory) {
		this.selectedCategory = selectedCategory;
	}
	public PMItemDetails getPMNewItem() {
		return newPMItem;
	}
	public void setPMNewItem(PMItemDetails newItem) {
		this.newPMItem = newItem;
	}

	public ItemDetails getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(ItemDetails selectedItem) {
		this.selectedItem = selectedItem;
	}

	public class PMItemDetails implements Serializable{
		ItemDetails iDetails = null;
		//List<String> featureNames = new ArrayList<String>();
		List<Value> featureValues = new ArrayList<Value>();
		//List<String> featureUnit = new ArrayList<String>();
		List<String> images = new ArrayList<String>();
		private transient Part file;
		
		public PMItemDetails(ItemDetails idetails){			
			iDetails= idetails;
			//String folderName = iDetails.getPrimaryImages() + "/" + iDetails.getItemId();
			//idetails.setPrimaryImages(folderName);
			images = UIUtil.getImages(idetails);
			for(String key: featuresDetails.keySet()){
				if(!featuresDetails.get(key).getHide()){
					featureValues.add(new Value(
											featuresDetails.get(key).getFeatureId(),
											iDetails.getFeatures().get(key),
											featuresDetails.get(key).getUnit()
							));
				}
			}
		}
		public void updateFeaturesFromList(){
			//iDetails.setModelName(featureValues.get(0).getValue());
			//iDetails.setBrandId(Integer.getInteger(featureValues.get(1)));
			
			for(int i=0;i<featureValues.size();i++){
				iDetails.getFeatures().put(featureValues.get(i).getFeature(), featureValues.get(i).getValue());
			}
		}
		public ItemDetails getiDetails() {
			return iDetails;
		}
		public void setiDetails(ItemDetails iDetails) {
			this.iDetails = iDetails;
		}
		public List<Value> getFeatureValues() {
			return featureValues;
		}
		public void setFeatureValues(List<Value> featureValues) {
			this.featureValues = featureValues;
		}
		public List<String> getImages() {
			return images;
		}
		public void setImages(List<String> images) {
			this.images = images;
		}
		public Part getFile() {
			return file;
		}
		public void setFile(Part file) {
			this.file = file;
		}
		public class Value implements Serializable{
			private String value = null;
			private String feature = null;
			private String unit = null;
			public Value(String fture, String val,String unt){
				feature = fture;
				value = val;
				unit = unt;
			}
			
			public String getValue() {
				return value;
			}

			public void setValue(String value) {
				this.value = value;
			}

			public String getFeature() {
				return feature;
			}

			public void setFeature(String feature) {
				this.feature = feature;
			}

			public String getUnit() {
				return unit;
			}

			public void setUnit(String unit) {
				this.unit = unit;
			}
		}
	}
	
	public PMItemDetails newPMItemDetails(ItemDetails id){
		return new PMItemDetails(new ItemDetails());
	}

	public String getCategoryIdInput() {
		return categoryIdInput;
	}

	public void setCategoryIdInput(String categoryIdInput) {
		this.categoryIdInput = categoryIdInput;
	}

	@Override
	public void onChange() {
		useSelectedCategory();
		String URL = "itemsUpdate.xhtml?category=" + selectedCategory.getCategoryId();
		UIUtil.redirect(URL);
	}

	@Override
	public String getId() {
		return ItemsPM.class.getName();
	}
}
