package com.cleanview.models;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ItemDetails implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Integer itemId;	
	private String modelName = "";
	private Map<String,String> features = new LinkedHashMap<String, String>();
	private Boolean verified;
	private Integer categoryId;
	private Integer brandId;
	private String primaryImages = "images";
	private String secondaryImages = "images";
	
	public Integer getItemId() {
		return itemId;
	}
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}	
	public Map<String, String> getFeatures() {
		return features;
	}
	public void setFeatures(Map<String, String> features) {
		this.features = features;
	}
	public Boolean getVerified() {
		return verified;
	}
	public void setVerified(Boolean verified) {
		this.verified = verified;
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public Integer getBrandId() {
		return brandId;
	}
	public void setBrandId(Integer brandId) {
		this.brandId = brandId;
	}
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public String getPrimaryImages() {
		return primaryImages;
	}
	public void setPrimaryImages(String primaryImages) {
		this.primaryImages = primaryImages;
	}
	public String getSecondaryImages() {
		return secondaryImages;
	}
	public void setSecondaryImages(String secondaryImages) {
		this.secondaryImages = secondaryImages;
	}
	@Override
	public String toString(){
		String str = "ItemDetails";
		str = "ItemDetails<--->";
		str = str + "<-->modelName<->" + modelName;
		str = str + "<-->brandId<->" + brandId;
		str = str + "<-->categoryId<->" + categoryId;
		str = str + "<-->itemId<->" + itemId;
		str = str + "<-->primaryImages<->" + primaryImages;
		str = str + "<-->secondaryImages<->" + secondaryImages;
		str = str + "<-->verified<->" + verified;
		for(String key: features.keySet()){
			str = str + "<-->" + key + "<->" + features.get(key);
		}
		return str;
	}
	
	public static ItemDetails populateFromString(String str){
		ItemDetails id = new ItemDetails();
		String [] strArr = str.split("<--->");
		strArr = strArr[1].split("<-->");
		//Map <String,String> members = new HashMap<String, String>();
				
		for(String s: strArr){
			String [] mem = s.split("<->");
			if(mem.length<2 || mem[1].equals("null"))
				continue;
			
			if(mem[0].equals("modelName")){
				id.setModelName(mem[1]);
			}else
			if(mem[0].equals("brandId")){
				id.setBrandId(Integer.parseInt(mem[1]));
			}else
			if(mem[0].equals("categoryId")){
				id.setCategoryId(Integer.parseInt(mem[1]));
			}else
			if(mem[0].equals("itemId")){
				id.setItemId(Integer.parseInt(mem[1]));
			}else
			if(mem[0].equals("primaryImages")){
				id.setPrimaryImages(mem[1]);
			}else
			if(mem[0].equals("secondaryImages")){
				id.setSecondaryImages(mem[1]);
			}else			
			if(mem[0].equals("verified")){
				if(mem[1].equalsIgnoreCase("true"))
					id.setVerified(true);
				else
					id.setVerified(false);
			}else{
				id.getFeatures().put(mem[0], mem[1]);
			}
		}
		return id;
	}
	
	/*public String toString() {
		return "ItemDetails\n [itemId=" + itemId + ",\n modelName="
				+ modelName + ",\n features=" + features + ",\n verified="
				+ verified + ",\n categoryId=" + categoryId + ",\n brandId="
				+ brandId + ",\n primaryImages=" + primaryImages
				+ ",\n secondaryImages=" + secondaryImages + "]";
	}*/
}
