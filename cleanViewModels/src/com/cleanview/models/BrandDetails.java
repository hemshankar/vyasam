package com.cleanview.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BrandDetails implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Integer brandId;
	private String brandName;
	private String description;
	private Integer categoryId=1;
	
	public Integer getBrandId() {
		return brandId;
	}
	public void setBrandId(Integer brandId) {
		this.brandId = brandId;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String destciption) {
		this.description = destciption;
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer category_id) {
		this.categoryId = category_id;
	}
	@Override
	public String toString() {
		return "BrandDetails\n [brandId=" + brandId + ",\n brandName="
				+ brandName + ",\n description=" + description
				+ ",\n categoryId=" + categoryId + "]";
	}
	
	
}
