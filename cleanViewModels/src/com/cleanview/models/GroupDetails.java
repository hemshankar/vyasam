package com.cleanview.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class GroupDetails implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String groupId;
	private String description;
	private Integer categoryId=1;
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer category_id) {
		this.categoryId = category_id;
	}
	@Override
	public String toString() {
		return "GroupDetails [groupId=" + groupId + ", description="
				+ description + "]";
	}
	
}
