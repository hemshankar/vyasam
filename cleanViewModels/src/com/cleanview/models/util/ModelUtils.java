package com.cleanview.models.util;

public class ModelUtils {

	public static String removeWhiteSpaces(String str){    	
    	str =  str.replaceAll("\\W+", "_");
    	return str;
    }
	
}
