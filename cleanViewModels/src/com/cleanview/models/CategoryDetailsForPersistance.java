package com.cleanview.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class CategoryDetailsForPersistance implements Serializable{
		
	private static final long serialVersionUID = 1L;
	
	private Integer categoryId;
	private String categoryName = "";
	private Boolean isValid = true;
	
	private Integer parentId = 1;
	private String description;
	
	private Map<String, FeatureDetails> featureDetails = new LinkedHashMap<String, FeatureDetails>();
	private Map<String, GroupDetails> groupDetails = new LinkedHashMap<String, GroupDetails>();
	private List<BrandDetails> brands = new ArrayList<BrandDetails>();
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parengId) {
		this.parentId = parengId;
	}	
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}	
	public Boolean getIsValid() {
		return isValid;
	}
	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}	
	public Map<String, FeatureDetails> getFeatureDetails() {
		return featureDetails;
	}
	public void setFeatureDetails(Map<String, FeatureDetails> featureDetails) {
		this.featureDetails = featureDetails;
	}
	public Map<String, GroupDetails> getGroupDetails() {
		return groupDetails;
	}
	public void setGroupDetails(Map<String, GroupDetails> groupDetails) {
		this.groupDetails = groupDetails;
	}
	
	public List<BrandDetails> getBrands() {
		return brands;
	}
	public void setBrands(List<BrandDetails> brands) {
		this.brands = brands;
	}
	@Override
	public String toString() {
		return "CategoryDetailsForPersistance\n [categoryId=" + categoryId
				+ ",\n categoryName=" + categoryName + ",\n isValid="
				+ isValid + ",\n parentId=" + parentId + ",\n description="
				+ description + ",\n featureDetails=" + featureDetails
				+ ",\n groupDetails=" + groupDetails + ",\n brands=" + brands
				+ "]";
	}
}
