package com.cleanview.models;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import com.cleanview.models.util.ModelUtils;

@XmlRootElement
public class FeatureDetails implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String featureId = "";
	private String featureName = "";
	private Integer priority = 0;
	private Integer maxValue = 0;
	private Integer minValue = 0;
	private String description = "";
	private String unit = "";
	private String groupId = "";
	private Map<String,ValueDetails> valueDetails= new LinkedHashMap<String, ValueDetails>();
	private Integer categoryId = 1;
	private Boolean hide = false; 
	public String getFeatureId() {
		return featureId;
	}
	public void setFeatureId(String featureId) {
		this.featureId = featureId;
	}
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	public Integer getMaxValue() {
		return maxValue;
	}
	public void setMaxValue(Integer maxValue) {
		this.maxValue = maxValue;
	}
	public Integer getMinValue() {
		return minValue;
	}
	public void setMinValue(Integer minValue) {
		this.minValue = minValue;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public Map<String, ValueDetails> getValueDetails() {
		return valueDetails;
	}
	public void setValueDetails(Map<String, ValueDetails> valueDetails) {
		this.valueDetails = valueDetails;
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer category_id) {
		this.categoryId = category_id;
	}
	@Override
	public String toString() {
		return "FeatureDetails [featureId=" + featureId + "\npriority="
				+ priority + "\nmaxValue=" + maxValue + "\nminValue="
				+ minValue + "\ndescription=" + description + "\nunit=" + unit
				+ "\ngroupId=" + groupId + "\nvalueDetails=" + valueDetails
				+ "]";
	}
	public String getFeatureName() {
		return featureName;
	}
	public void setFeatureName(String featureName) {
		this.featureName = featureName;
	}
	public Boolean getHide() {
		return hide;
	}
	public void setHide(Boolean hide) {
		this.hide = hide;
	}
	
}
